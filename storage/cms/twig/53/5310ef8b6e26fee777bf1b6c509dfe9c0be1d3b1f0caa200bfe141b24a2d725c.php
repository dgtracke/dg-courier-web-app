<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\laragon\www\cargo/themes/spotlayer/pages/dashboard/shipment.htm */
class __TwigTemplate_bc83762f76d0bda2ab8379d32408c0d3d79d5271fe03cfec911af95f235f1bfb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"kt-portlet \">
    <div class=\"kt-portlet__body\">
        <div class=\"kt-widget kt-widget--user-profile-3\">
            <div class=\"kt-widget__top\">
                <div class=\"kt-widget__content\">
                    <div class=\"kt-widget__head\">
                        <a href=\"javascript:;\" class=\"kt-widget__username\">
                            ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "tracking", [], "any", false, false, false, 8), "prefix_order", [], "any", false, false, false, 8), "html", null, true);
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "number", [], "any", false, false, false, 8), "html", null, true);
        echo "
                        </a>
                        <div class=\"kt-widget__action\">
                \t\t\t<a href=\"javascript:;\" class=\"btn btn-label-brand btn-bold btn-sm dropdown-toggle\" data-toggle=\"dropdown\">
                \t\t\t\t";
        // line 12
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Order Actions"]);
        echo "
                \t\t\t</a>
                \t\t\t<div class=\"dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md\">
                                <!--begin::Nav-->
                                <ul class=\"kt-nav\">
                                    ";
        // line 17
        if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 17) != 4) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 17) != 8))) {
            // line 18
            echo "                                        ";
            if ((((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 18) == 0) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 18) == 1)) || (twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "requested", [], "any", false, false, false, 18) == 100))) {
                // line 19
                echo "                                            ";
                if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 19) == 1) && (twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "role_id", [], "any", false, false, false, 19) == 5))) {
                    // line 20
                    echo "                                            ";
                } else {
                    // line 21
                    echo "                                                ";
                    if (twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "hasUserPermission", [0 => "order", 1 => "u"], "method", false, false, false, 21)) {
                        // line 22
                        echo "                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"";
                        // line 23
                        echo $this->extensions['Cms\Twig\Extension']->pageFilter("dashboard/order-edit", ["id" => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "id", [], "any", false, false, false, 23)]);
                        echo "\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-edit-1\"></i>
                                                            <span class=\"kt-nav__link-text\">";
                        // line 25
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Edit"]);
                        echo "</span>
                                                        </a>
                                                    </li>
                                                ";
                    }
                    // line 29
                    echo "                                                ";
                    if (twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "hasUserPermission", [0 => "order", 1 => "d"], "method", false, false, false, 29)) {
                        // line 30
                        echo "                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"javascript:;\" id=\"delete\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-cancel\"></i>
                                                            <span class=\"kt-nav__link-text\">";
                        // line 33
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Delete"]);
                        echo "</span>
                                                        </a>
                                                    </li>
                                                ";
                    }
                    // line 37
                    echo "                                            ";
                }
                // line 38
                echo "                                        ";
            }
            // line 39
            echo "

                                        ";
            // line 41
            if ((twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "role_id", [], "any", false, false, false, 41) != 5)) {
                // line 42
                echo "                                            ";
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 42) == 0)) {
                    // line 43
                    echo "                                                <li class=\"kt-nav__item\">
                                                    <a href=\"javascript:;\" id=\"accept\" class=\"kt-nav__link\">
                                                        <i class=\"kt-nav__link-icon flaticon2-check-mark\"></i>
                                                        <span class=\"kt-nav__link-text\">";
                    // line 46
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Accept"]);
                    echo "</span>
                                                    </a>
                                                </li>
                                                <li class=\"kt-nav__item\">
                                                    <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#refuse\" class=\"kt-nav__link\">
                                                        <i class=\"kt-nav__link-icon flaticon-signs-2\"></i>
                                                        <span class=\"kt-nav__link-text\">";
                    // line 52
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Refuse"]);
                    echo "</span>
                                                    </a>
                                                </li>
                                            ";
                } elseif ((twig_get_attribute($this->env, $this->source,                 // line 55
($context["order"] ?? null), "requested", [], "any", false, false, false, 55) != 2)) {
                    // line 56
                    echo "                                                ";
                    if (((((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 56) == 1) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 56) == 5)) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 56) == 7)) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 56) == 10))) {
                        // line 57
                        echo "                                                    ";
                        if (twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "manifest_id", [], "any", false, false, false, 57))) {
                            // line 58
                            echo "                                                        ";
                            if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "assigned_id", [], "any", false, false, false, 58)) {
                                // line 59
                                echo "                                                            <li class=\"kt-nav__item\">
                                                                <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#assign_employee\" class=\"kt-nav__link\">
                                                                    <i class=\"kt-nav__link-icon flaticon-users\"></i>
                                                                    <span class=\"kt-nav__link-text\">";
                                // line 62
                                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Change Assigned Employee"]);
                                echo "</span>
                                                                </a>
                                                            </li>
                                                        ";
                            } else {
                                // line 66
                                echo "                                                            <li class=\"kt-nav__item\">
                                                                <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#assign_employee\" class=\"kt-nav__link\">
                                                                    <i class=\"kt-nav__link-icon flaticon-users\"></i>
                                                                    <span class=\"kt-nav__link-text\">";
                                // line 69
                                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Assign Employee"]);
                                echo "</span>
                                                                </a>
                                                            </li>
                                                        ";
                            }
                            // line 73
                            echo "                                                    ";
                        }
                        // line 74
                        echo "                                                    ";
                        if (twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "assigned_id", [], "any", false, false, false, 74))) {
                            // line 75
                            echo "                                                        ";
                            if (twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "hasUserPermission", [0 => "manifest", 1 => "c"], "method", false, false, false, 75)) {
                                // line 76
                                echo "                                                            ";
                                if (twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "manifest_id", [], "any", false, false, false, 76))) {
                                    // line 77
                                    echo "                                                                <li class=\"kt-nav__item\">
                                                                    <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#assign_manifest\" class=\"kt-nav__link\">
                                                                        <i class=\"kt-nav__link-icon flaticon-truck\"></i>
                                                                        <span class=\"kt-nav__link-text\">";
                                    // line 80
                                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Assign Manifest"]);
                                    echo "</span>
                                                                    </a>
                                                                </li>
                                                            ";
                                } else {
                                    // line 84
                                    echo "                                                                ";
                                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "manifest", [], "any", false, false, false, 84), "status", [], "any", false, false, false, 84) == 1)) {
                                        // line 85
                                        echo "                                                                    <li class=\"kt-nav__item\">
                                                                        <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#assign_manifest\" class=\"kt-nav__link\">
                                                                            <i class=\"kt-nav__link-icon flaticon-truck\"></i>
                                                                            <span class=\"kt-nav__link-text\">";
                                        // line 88
                                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Change Manifest"]);
                                        echo "</span>
                                                                        </a>
                                                                    </li>
                                                                ";
                                    }
                                    // line 92
                                    echo "                                                            ";
                                }
                                // line 93
                                echo "                                                        ";
                            }
                            // line 94
                            echo "                                                    ";
                        }
                        // line 95
                        echo "                                                    ";
                        if (( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "assigned_id", [], "any", false, false, false, 95)) ||  !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "manifest_id", [], "any", false, false, false, 95)))) {
                            // line 96
                            echo "                                                        <li class=\"kt-nav__item\">
                                                            ";
                            // line 97
                            if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "type", [], "any", false, false, false, 97) == 1)) {
                                // line 98
                                echo "                                                                <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#received\" class=\"kt-nav__link\">
                                                                    <i class=\"kt-nav__link-icon flaticon2-check-mark\"></i>
                                                                    <span class=\"kt-nav__link-text\">";
                                // line 100
                                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Received"]);
                                echo "</span>
                                                                </a>
                                                            ";
                            } else {
                                // line 103
                                echo "                                                                <a href=\"javascript:;\" id=\"delivered_driver\" class=\"kt-nav__link\">
                                                                    <i class=\"kt-nav__link-icon flaticon2-check-mark\"></i>
                                                                    <span class=\"kt-nav__link-text\">";
                                // line 105
                                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Delivered to driver"]);
                                echo "</span>
                                                                </a>
                                                            ";
                            }
                            // line 108
                            echo "                                                        </li>
                                                    ";
                        }
                        // line 110
                        echo "                                                    ";
                        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 110) != 10)) {
                            // line 111
                            echo "                                                        <li class=\"kt-nav__item\">
                                                            <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#transfer_office\" class=\"kt-nav__link\">
                                                                <i class=\"kt-nav__link-icon flaticon-logout\"></i>
                                                                <span class=\"kt-nav__link-text\">";
                            // line 114
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Transfer To Branch"]);
                            echo "</span>
                                                            </a>
                                                        </li>
                                                    ";
                        }
                        // line 118
                        echo "                                                ";
                    }
                    // line 119
                    echo "                                                ";
                    if (((((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 119) != 7) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 119) != 10)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 119) != 11)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 119) != 12))) {
                        // line 120
                        echo "                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"javascript:;\" id=\"stock\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-user-ok\"></i>
                                                            <span class=\"kt-nav__link-text\">";
                        // line 123
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Save in stock"]);
                        echo "</span>
                                                        </a>
                                                    </li>
                                                ";
                    }
                    // line 127
                    echo "                                                ";
                    if ((((((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 127) != 7) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 127) != 9)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 127) != 10)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 127) != 11)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 127) != 12))) {
                        // line 128
                        echo "                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#postpone\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-refresh\"></i>
                                                            <span class=\"kt-nav__link-text\">";
                        // line 131
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Postpone"]);
                        echo "</span>
                                                        </a>
                                                    </li>
                                                ";
                    }
                    // line 135
                    echo "                                                ";
                    if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 135) == 3) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 135) == 9))) {
                        // line 136
                        echo "                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#discards\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-refresh\"></i>
                                                            <span class=\"kt-nav__link-text\">";
                        // line 139
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Transfer as discards"]);
                        echo "</span>
                                                        </a>
                                                    </li>
                                                ";
                    }
                    // line 143
                    echo "                                                ";
                    if ((((((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 143) == 3) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 143) == 5)) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 143) == 6)) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 143) == 7)) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 143) == 9))) {
                        // line 144
                        echo "                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"";
                        // line 145
                        echo $this->extensions['Cms\Twig\Extension']->pageFilter("dashboard/order-deliver", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 145), "id", [], "any", false, false, false, 145)]);
                        echo "\" id=\"deliveried_bak\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-user-ok\"></i>
                                                            <span class=\"kt-nav__link-text\">";
                        // line 147
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Delieveried"]);
                        echo "</span>
                                                        </a>
                                                    </li>
                                                ";
                    }
                    // line 151
                    echo "                                            ";
                }
                // line 152
                echo "                                        ";
            }
            // line 153
            echo "                                    ";
        }
        // line 154
        echo "

                                    ";
        // line 156
        if ((twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "role_id", [], "any", false, false, false, 156) != 5)) {
            // line 157
            echo "                                        ";
            if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 157) == 1) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 157) == 4))) {
                // line 158
                echo "                                            <li class=\"kt-nav__item\">
                                                <a href=\"javascript:;\" id=\"return_discards\" class=\"kt-nav__link\">
                                                    <i class=\"kt-nav__link-icon flaticon-refresh\"></i>
                                                    <span class=\"kt-nav__link-text\">";
                // line 161
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Return discards"]);
                echo "</span>
                                                </a>
                                            </li>
                                        ";
            }
            // line 165
            echo "                                        ";
            if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 165) == 11)) {
                // line 166
                echo "                                            <li class=\"kt-nav__item\">
                                                <a href=\"javascript:;\" id=\"return_discards\" class=\"kt-nav__link\">
                                                    <i class=\"kt-nav__link-icon flaticon2-check-mark\"></i>
                                                    <span class=\"kt-nav__link-text\">";
                // line 169
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Deliver returned discards"]);
                echo "</span>
                                                </a>
                                            </li>
                                        ";
            }
            // line 173
            echo "                                        ";
            if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "type", [], "any", false, false, false, 173) == 2) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 173) != 0))) {
                // line 174
                echo "                                            <li class=\"kt-nav__item\">
                                                <a href=\"";
                // line 175
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("dashboard/order-print", ["id" => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "id", [], "any", false, false, false, 175), "type" => "shipment"]);
                echo "\" target=\"_blank\" class=\"kt-nav__link\">
                                                    <i class=\"kt-nav__link-icon flaticon2-print\"></i>
                                                    <span class=\"kt-nav__link-text\">";
                // line 177
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Print Shipment"]);
                echo "</span>
                                                </a>
                                            </li>
                                        ";
            }
            // line 181
            echo "                                        ";
            if (((((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 181) != 0) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 181) != 10)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 181) != 11)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 181) != 12))) {
                // line 182
                echo "                                            <li class=\"kt-nav__item\">
                                                <a href=\"";
                // line 183
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("dashboard/order-print", ["id" => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "id", [], "any", false, false, false, 183), "type" => "label"]);
                echo "\" target=\"_blank\" class=\"kt-nav__link\">
                                                    <i class=\"kt-nav__link-icon flaticon2-print\"></i>
                                                    <span class=\"kt-nav__link-text\">";
                // line 185
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Print Label"]);
                echo "</span>
                                                </a>
                                            </li>
                                        ";
            }
            // line 189
            echo "                                    ";
        }
        // line 190
        echo "                                </ul>
                                <!--end::Nav-->
                            </div>
                        </div>
                    </div>

                    <div class=\"kt-widget__subhead\">
                        ";
        // line 197
        echo call_user_func_array($this->env->getFunction('barcodeHTML')->getCallable(), [["data" => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "barcode", [], "any", false, false, false, 197), "type" => "CODABAR"]]);
        echo "
                    </div>

                    <div class=\"kt-widget__info\">

                        <div class=\"kt-widget__stats d-flex align-items-center flex-fill\">
                            <div class=\"kt-widget__item\">
                                <span class=\"kt-widget__date\">
                                    ";
        // line 205
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Notes"]);
        echo "
                                </span>
                                <div class=\"kt-widget__label notes_scroll\">
                                    <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">";
        // line 208
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "notes", [], "any", false, false, false, 208), "count", [], "any", false, false, false, 208), "html", null, true);
        echo "</span>
                                </div>
                            </div>
                            <div class=\"kt-widget__item\">
                                <span class=\"kt-widget__date\">
                                    ";
        // line 213
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Package Receive Date"]);
        echo "
                                </span>
                                <div class=\"kt-widget__label\">
                                    ";
        // line 216
        if ((($context["addShipmentForm"] ?? null) == "add_form_normal")) {
            // line 217
            echo "                                    <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "created_at", [], "any", false, false, false, 217), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "dateformat", [], "any", false, false, false, 217)), "html", null, true);
            echo "</span>
                                    ";
        } else {
            // line 219
            echo "                                    <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "ship_date", [], "any", false, false, false, 219), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "dateformat", [], "any", false, false, false, 219)), "html", null, true);
            echo "</span>
                                    ";
        }
        // line 221
        echo "                                </div>
                            </div>
                            ";
        // line 223
        if ((($context["addShipmentForm"] ?? null) != "add_form_simple")) {
            // line 224
            echo "                            <div class=\"kt-widget__item\">
                                <span class=\"kt-widget__date\">
                                    ";
            // line 226
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Received Date"]);
            echo "
                                </span>
                                <div class=\"kt-widget__label\">
                                    <span class=\"btn btn-label-";
            // line 229
            echo twig_escape_filter($this->env, ($context["progress_status"] ?? null), "html", null, true);
            echo " btn-sm btn-bold btn-upper\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "releasedNote_received", [], "any", false, false, false, 229), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "dateformat", [], "any", false, false, false, 229)), "html", null, true);
            echo "</span>
                                </div>
                            </div>
                            ";
        } elseif ((        // line 232
($context["addShipmentForm"] ?? null) != "add_form_simple")) {
            // line 233
            echo "                                ";
            if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receive_date", [], "any", false, false, false, 233)) {
                // line 234
                echo "                                    <div class=\"kt-widget__item\">
                                        <span class=\"kt-widget__date\">
                                            ";
                // line 236
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Received Date"]);
                echo "
                                        </span>
                                        <div class=\"kt-widget__label\">
                                            <span class=\"btn btn-label-";
                // line 239
                echo twig_escape_filter($this->env, ($context["progress_status"] ?? null), "html", null, true);
                echo " btn-sm btn-bold btn-upper\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receive_date", [], "any", false, false, false, 239), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "dateformat", [], "any", false, false, false, 239)), "html", null, true);
                echo "</span>
                                        </div>
                                    </div>
                                ";
            } else {
                // line 243
                echo "                                    <div class=\"kt-widget__item\">
                                        <span class=\"kt-widget__date\">
                                            ";
                // line 245
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Package Expected Delievery Date"]);
                echo "
                                        </span>
                                        <div class=\"kt-widget__label\">
                                            ";
                // line 248
                if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "delivery_date", [], "any", false, false, false, 248)) {
                    // line 249
                    echo "                                                <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "delivery_date", [], "any", false, false, false, 249), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "dateformat", [], "any", false, false, false, 249)), "html", null, true);
                    echo "</span>";
                    if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "postponed", [], "any", false, false, false, 249) == 1) && twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 249), [0 => 1, 1 => 3, 2 => 5, 3 => 6, 4 => 7, 5 => 10, 6 => 11]))) {
                        echo " | <span class=\"btn btn-label-info btn-sm btn-bold btn-upper\">";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["POSTPONED"]);
                        echo "</span>";
                    }
                    // line 250
                    echo "                                            ";
                } else {
                    // line 251
                    echo "                                                <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">
                                                ";
                    // line 252
                    if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "deliverytime", [], "any", false, false, false, 252)) {
                        // line 253
                        echo "                                                    ";
                        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_modify_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "ship_date", [], "any", false, false, false, 253), (("+" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "deliverytime", [], "any", false, false, false, 253), "count", [], "any", false, false, false, 253)) . " hours")), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "dateformat", [], "any", false, false, false, 253)), "html", null, true);
                        echo "
                                                ";
                    } else {
                        // line 255
                        echo "                                                    ";
                        echo null;
                        echo "
                                                ";
                    }
                    // line 257
                    echo "                                                </span>
                                            ";
                }
                // line 259
                echo "                                        </div>
                                    </div>
                                ";
            }
            // line 262
            echo "                            ";
        }
        // line 263
        echo "
                            <div class=\"kt-widget__item flex-fill\">
                                <span class=\"kt-widget__subtitel\">";
        // line 265
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Progress"]);
        echo "</span>
                                <div class=\"kt-widget__progress d-flex  align-items-center\">
                                    <div class=\"progress\" style=\"height: 5px;width: 100%;\">
                                        <div class=\"progress-bar kt-bg-";
        // line 268
        echo twig_escape_filter($this->env, ($context["progress_status"] ?? null), "html", null, true);
        echo "\" role=\"progressbar\" style=\"width: ";
        echo twig_escape_filter($this->env, ($context["progress"] ?? null), "html", null, true);
        echo "%;\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                                    </div>
                                    <span class=\"kt-widget__stat\">
                                        ";
        // line 271
        echo twig_escape_filter($this->env, ($context["progress"] ?? null), "html", null, true);
        echo "%
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end:: Portlet-->


<div class=\"row\">
    <div class=\"col-xl-6\">
        <!--begin:: Widgets/Order Statistics-->
        <div class=\"kt-portlet kt-portlet--height-fluid\">
        \t<div class=\"kt-portlet__head\">
        \t\t<div class=\"kt-portlet__head-label\">
        \t\t\t<h3 class=\"kt-portlet__head-title\">
        \t\t\t\t";
        // line 292
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Order Details"]);
        echo "
        \t\t\t</h3>
        \t\t</div>
        \t</div>
        \t<div class=\"kt-portlet__body kt-portlet__body--fluid\">
        \t\t<div class=\"kt-widget12\">
        \t\t\t<div class=\"kt-widget12__content\">
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
        // line 301
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Shipment Type"]);
        echo "</span>
                                <span class=\"kt-widget12__value\">
                                    ";
        // line 303
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "type", [], "any", false, false, false, 303) == 1)) {
            // line 304
            echo "                                        ";
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receive"]);
            echo "
                                    ";
        } else {
            // line 306
            echo "                                        ";
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Send"]);
            echo "
                                    ";
        }
        // line 308
        echo "                                </span>
                            </div>
                            ";
        // line 310
        if ((($context["addShipmentForm"] ?? null) == "add_form_advance")) {
            // line 311
            echo "                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 312
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Package Type"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 313
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "packaging", [], "any", false, false, false, 313), "name", [], "any", false, false, false, 313), "html", null, true);
            echo "</span>
                            </div>
                            ";
        }
        // line 316
        echo "                        </div>
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
        // line 319
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Status"]);
        echo "</span>
                                <span class=\"kt-widget12__value\">
                                    <span class=\"btn btn-label-";
        // line 321
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "status", [], "any", false, false, false, 321), "color", [], "any", false, false, false, 321), "html", null, true);
        echo " btn-sm btn-bold btn-upper\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "status", [], "any", false, false, false, 321), "name", [], "any", false, false, false, 321), "html", null, true);
        echo "</span>
                                    ";
        // line 322
        if ((($context["progress_status"] ?? null) == "danger")) {
            echo " | <span class=\"btn btn-label-";
            echo twig_escape_filter($this->env, ($context["progress_status"] ?? null), "html", null, true);
            echo " btn-sm btn-bold btn-upper\">";
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Delayed"]);
            echo "</span>";
        }
        if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 322) == 10) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 322) == 11))) {
            echo " | <span class=\"btn btn-label-warning btn-sm btn-bold btn-upper\">";
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["RETURNING DISCARDS"]);
            echo "</span>";
        }
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "postponed", [], "any", false, false, false, 322) == 1)) {
            echo " | <span class=\"btn btn-label-info btn-sm btn-bold btn-upper\">";
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["POSTPONED"]);
            echo "</span>";
        }
        if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 322) == 12) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["row"] ?? null), "status", [], "any", false, false, false, 322), "equal", [], "any", false, false, false, 322) != 12))) {
            echo " | <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">";
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Supplied"]);
            echo "</span>";
        }
        // line 323
        echo "                                </span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
        // line 326
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Channel"]);
        echo "</span>
                                <span class=\"kt-widget12__value\">";
        // line 327
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "channel", [], "any", false, false, false, 327)]);
        echo "</span>
                            </div>
                        </div>
                        ";
        // line 330
        if ((($context["addShipmentForm"] ?? null) == "add_form_normal")) {
            // line 331
            echo "                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 333
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Label"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 334
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "label", [], "any", false, false, false, 334), "name", [], "any", false, false, false, 334), "html", null, true);
            echo "</span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 337
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Ground Handler"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 338
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "handler", [], "any", false, false, false, 338), "name", [], "any", false, false, false, 338), "html", null, true);
            echo "</span>
                            </div>
                        </div>
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 343
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Breakdown"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 344
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "breakdown", [], "any", false, false, false, 344), "name", [], "any", false, false, false, 344), "html", null, true);
            echo "</span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 347
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Tranfer to Jost"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 348
            echo ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "transfer_jost", [], "any", false, false, false, 348)) ? ("Yes") : ("No"));
            echo "</span>
                            </div>
                        </div>
                        ";
        } elseif ((        // line 351
($context["addShipmentForm"] ?? null) == "add_form_advance")) {
            // line 352
            echo "                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 354
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Payment Type"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 355
            if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payment_type", [], "any", false, false, false, 355) == 1)) {
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Postpaid"]);
            } elseif ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payment_type", [], "any", false, false, false, 355) == 2)) {
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Prepaid"]);
            }
            echo "</span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 358
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Currency"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 359
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "currency", [], "any", false, false, false, 359), "name", [], "any", false, false, false, 359), "html", null, true);
            echo "</span>
                            </div>
                        </div>
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 364
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Office"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 365
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "office", [], "any", false, false, false, 365), "name", [], "any", false, false, false, 365), "html", null, true);
            echo "</span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 368
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Delivery Time"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">";
            // line 369
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "deliverytime", [], "any", false, false, false, 369), "name", [], "any", false, false, false, 369), "html", null, true);
            echo "</span>
                            </div>
                        </div>
                        ";
        }
        // line 373
        echo "                        ";
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "assigned_id", [], "any", false, false, false, 373) || twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "manifest_id", [], "any", false, false, false, 373))) {
            // line 374
            echo "                            <div class=\"kt-widget12__item\">
                                ";
            // line 375
            if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "manifest_id", [], "any", false, false, false, 375)) {
                // line 376
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 377
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Manifest"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">#";
                // line 378
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "currency", [], "any", false, false, false, 378), "id", [], "any", false, false, false, 378), "html", null, true);
                echo "</span>
                                        ";
                // line 379
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "manifest", [], "any", false, false, false, 379), "driver", [], "any", false, false, false, 379)) {
                    // line 380
                    echo "                                            <span class=\"kt-widget12__value kt-font-sm\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "manifest", [], "any", false, false, false, 380), "driver", [], "any", false, false, false, 380), "name", [], "any", false, false, false, 380), "html", null, true);
                    echo "</span>
                                        ";
                }
                // line 382
                echo "                                    </div>
                                ";
            }
            // line 384
            echo "                                ";
            if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "assigned_id", [], "any", false, false, false, 384)) {
                // line 385
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 386
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Assigned To"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">";
                // line 387
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "responsiable", [], "any", false, false, false, 387), "name", [], "any", false, false, false, 387), "html", null, true);
                echo "</span>
                                        ";
                // line 388
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "responsiable", [], "any", false, false, false, 388), "mobile", [], "any", false, false, false, 388)) {
                    // line 389
                    echo "                                            <span class=\"kt-widget12__value kt-font-sm\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "responsiable", [], "any", false, false, false, 389), "mobile", [], "any", false, false, false, 389), "html", null, true);
                    echo "</span>
                                        ";
                }
                // line 391
                echo "                                    </div>
                                ";
            }
            // line 393
            echo "                            </div>
                        ";
        }
        // line 395
        echo "                        ";
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "courier_id", [], "any", false, false, false, 395) || twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "mode_id", [], "any", false, false, false, 395))) {
            // line 396
            echo "                            <div class=\"kt-widget12__item\">
                                ";
            // line 397
            if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "courier_id", [], "any", false, false, false, 397)) {
                // line 398
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 399
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Courier Company"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">";
                // line 400
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "courier", [], "any", false, false, false, 400), "name", [], "any", false, false, false, 400), "html", null, true);
                echo "</span>
                                    </div>
                                ";
            }
            // line 403
            echo "                                ";
            if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "mode_id", [], "any", false, false, false, 403)) {
                // line 404
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 405
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Shipping Mode"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">";
                // line 406
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "mode", [], "any", false, false, false, 406), "name", [], "any", false, false, false, 406), "html", null, true);
                echo "</span>
                                    </div>
                                ";
            }
            // line 409
            echo "                            </div>
                        ";
        }
        // line 411
        echo "
                        ";
        // line 412
        if (twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 412), [0 => 4, 1 => 5, 2 => 6, 3 => 10, 4 => 11, 5 => 12])) {
            // line 413
            echo "                            ";
            if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "delivered_by", [], "any", false, false, false, 413) != twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "delivered_responsiable", [], "any", false, false, false, 413))) {
                // line 414
                echo "                                <div class=\"kt-widget12__item\">
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 416
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Delivered Responsiabality"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">";
                // line 417
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "delivered_responsiabality", [], "any", false, false, false, 417), "name", [], "any", false, false, false, 417), "html", null, true);
                echo "</span>
                                    </div>
                                </div>
                            ";
            }
            // line 421
            echo "                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
            // line 423
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Delivered By"]);
            echo "</span>
                                    <span class=\"kt-widget12__value\">";
            // line 424
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "delivered_employee", [], "any", false, false, false, 424), "name", [], "any", false, false, false, 424), "html", null, true);
            echo "</span>
                                </div>
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
            // line 427
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Received By"]);
            echo "</span>
                                    <span class=\"kt-widget12__value\">";
            // line 428
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "received_by", [], "any", false, false, false, 428), "html", null, true);
            echo "</span>
                                </div>
                            </div>
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
            // line 433
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver ID Copy"]);
            echo "</span>
                                    <span class=\"kt-widget12__value\">
                                        <img src=\"";
            // line 435
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "id_copy", [], "any", false, false, false, 435), "path", [], "any", false, false, false, 435), "html", null, true);
            echo "\" style=\"max-height:350px;\" alt=\"\" />
                                    </span>
                                </div>
                            </div>
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
            // line 441
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver Signature"]);
            echo "</span>
                                    <span class=\"kt-widget12__value\">
                                        <img src=\"";
            // line 443
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "esign", [], "any", false, false, false, 443), "html", null, true);
            echo "\" style=\"max-height:350px;\" alt=\"\" />
                                    </span>
                                </div>
                            </div>
                        ";
        }
        // line 448
        echo "        \t\t\t</div>
        \t\t</div>
        \t</div>
        </div>
        <!--end:: Widgets/Order Statistics-->
    </div>
    <div class=\"col-xl-6\">
        <!--begin:: Widgets/Order Statistics-->
        <div class=\"kt-portlet kt-portlet--height-fluid\">
        \t<div class=\"kt-portlet__head\">
        \t\t<div class=\"kt-portlet__head-label\">
        \t\t\t<h3 class=\"kt-portlet__head-title\">
        \t\t\t\t";
        // line 460
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Payment&Items Details"]);
        echo "
        \t\t\t</h3>
        \t\t</div>
        \t</div>
        \t<div class=\"kt-portlet__body kt-portlet__body--fluid\">
        \t\t<div class=\"kt-widget12\">
        \t\t\t<div class=\"kt-widget12__content\">
                        ";
        // line 467
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "items", [], "any", false, false, false, 467))) {
            // line 468
            echo "                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                            \t\t<div class=\"table-responsive\">
                                        ";
            // line 471
            if ((($context["addShipmentForm"] ?? null) == "add_form_normal")) {
                // line 472
                echo "                                        <table class=\"table table-dark\">
                                            <thead>
                                                <tr>
                                                    <th>";
                // line 475
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["AirwayBill"]);
                echo "</th>
                                                    <th>";
                // line 476
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Info"]);
                echo "</th>
                                                    <th>";
                // line 477
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Weight"]);
                echo "</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                ";
                // line 481
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "items", [], "any", false, false, false, 481));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 482
                    echo "                                                    <tr>
                                                        <th scope=\"row\">";
                    // line 483
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "airWayBill", [], "any", false, false, false, 483), "html", null, true);
                    echo "</th>
                                                        <td>
                                                            ";
                    // line 485
                    if (twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 485)) {
                        // line 486
                        echo "                                                                ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 486), "html", null, true);
                        echo "<br />
                                                            ";
                    }
                    // line 488
                    echo "                                                            <b>";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Number of boxes"]);
                    echo ":</b> ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "num_boxes", [], "any", false, false, false, 488), "html", null, true);
                    echo "<br />
                                                            <b>";
                    // line 489
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Number of boxes"]);
                    echo ":</b> ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "num_pallets", [], "any", false, false, false, 489), "html", null, true);
                    echo "<br />
                                                        </td>
                                                        <td>";
                    // line 491
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "weight", [], "any", false, false, false, 491), "html", null, true);
                    echo " ";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Kg"]);
                    echo "</td>
                                                    </tr>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 494
                echo "                                            </tbody>
                                        </table>
                                        ";
            } else {
                // line 497
                echo "                                        <table class=\"table table-dark\">
                    \t\t\t\t\t  \t<thead>
                    \t\t\t\t\t    \t<tr>
                    \t\t\t\t\t      \t\t<th>";
                // line 500
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Category"]);
                echo "</th>
                    \t\t\t\t\t      \t\t<th>";
                // line 501
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Info"]);
                echo "</th>
                    \t\t\t\t\t      \t\t<th>";
                // line 502
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Weight"]);
                echo "</th>
                    \t\t\t\t\t    \t</tr>
                    \t\t\t\t\t  \t</thead>
                    \t\t\t\t\t  \t<tbody>
                                                ";
                // line 506
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "items", [], "any", false, false, false, 506));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 507
                    echo "                        \t\t\t\t\t    \t<tr>
                        \t\t\t\t\t\t      \t<th scope=\"row\">";
                    // line 508
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "category", [], "any", false, false, false, 508), "name", [], "any", false, false, false, 508), "html", null, true);
                    echo "</th>
                        \t\t\t\t\t\t      \t<td>
                                                            ";
                    // line 510
                    if (twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 510)) {
                        // line 511
                        echo "                                                                ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "description", [], "any", false, false, false, 511), "html", null, true);
                        echo "<br />
                                                            ";
                    }
                    // line 513
                    echo "                                                            <b>";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Quantity"]);
                    echo ":</b> ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 513), "html", null, true);
                    echo "<br />
                                                            ";
                    // line 514
                    if ((($context["addShipmentForm"] ?? null) != "add_form_simple")) {
                        // line 515
                        echo "                                                            <b>";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Dimensions"]);
                        echo ":</b> ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "length", [], "any", false, false, false, 515), "html", null, true);
                        echo " ";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["cm"]);
                        echo " x ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "width", [], "any", false, false, false, 515), "html", null, true);
                        echo " ";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["cm"]);
                        echo " x ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "height", [], "any", false, false, false, 515), "html", null, true);
                        echo " ";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["cm"]);
                        echo "
                                                            ";
                    }
                    // line 517
                    echo "                                                        </td>
                        \t\t\t\t\t\t      \t<td>";
                    // line 518
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "weight", [], "any", false, false, false, 518), "html", null, true);
                    echo " ";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Kg"]);
                    echo "</td>
                        \t\t\t\t\t    \t</tr>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 521
                echo "                    \t\t\t\t\t  \t</tbody>
                    \t\t\t\t\t</table>
                                        ";
            }
            // line 524
            echo "                                    </div>
                                </div>
                            </div>
                        ";
        }
        // line 528
        echo "                        
                        ";
        // line 529
        if ((($context["addShipmentForm"] ?? null) == "add_form_normal")) {
            // line 530
            echo "                        ";
            if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "custom_clearance", [], "any", false, false, false, 530) != 0) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "fiscal_representaion", [], "any", false, false, false, 530) != 0))) {
                // line 531
                echo "                            <div class=\"kt-widget12__item\">
                                ";
                // line 532
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "custom_clearance", [], "any", false, false, false, 532) != 0)) {
                    // line 533
                    echo "                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                    // line 534
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Custom Clearance"]);
                    echo "</span>
                                    <span class=\"kt-widget12__value\">";
                    // line 535
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "custom_clearance", [], "any", false, false, false, 535)]);
                    echo "</span>
                                </div>
                                ";
                }
                // line 538
                echo "                                ";
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "fiscal_representation", [], "any", false, false, false, 538) != 0)) {
                    // line 539
                    echo "                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                    // line 540
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Fisacl Representaion"]);
                    echo "</span>
                                    <span class=\"kt-widget12__value\">";
                    // line 541
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "fiscal_representation", [], "any", false, false, false, 541)]);
                    echo "</span>
                                </div>
                                ";
                }
                // line 544
                echo "                            </div>
                        ";
            }
            // line 546
            echo "                        ";
            if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payment_term", [], "any", false, false, false, 546) != 0) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "price_kg", [], "any", false, false, false, 546) != 0))) {
                // line 547
                echo "                            <div class=\"kt-widget12__item\">
                                ";
                // line 548
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payment_term", [], "any", false, false, false, 548) != 0)) {
                    // line 549
                    echo "                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                    // line 550
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Invoice Payment Term"]);
                    echo "</span>
                                    <span class=\"kt-widget12__value\">";
                    // line 551
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payment_term", [], "any", false, false, false, 551)]);
                    echo "</span>
                                </div>
                                ";
                }
                // line 554
                echo "                                ";
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "price_kg", [], "any", false, false, false, 554) != 0)) {
                    // line 555
                    echo "                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                    // line 556
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Handling Price per Kg "]);
                    echo "</span>
                                    <span class=\"kt-widget12__value\">";
                    // line 557
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "price_kg", [], "any", false, false, false, 557)]);
                    echo "</span>
                                </div>
                                ";
                }
                // line 560
                echo "                            </div>
                        ";
            }
            // line 562
            echo "                        ";
            if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "storage_fee", [], "any", false, false, false, 562) == 1)) {
                // line 563
                echo "                            <div class=\"kt-widget12__item\">
                                ";
                // line 564
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "cost_24", [], "any", false, false, false, 564) != 0)) {
                    // line 565
                    echo "                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                    // line 566
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Storage Costs after 24 Hours"]);
                    echo "</span>
                                    <span class=\"kt-widget12__value\">";
                    // line 567
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "cost_24", [], "any", false, false, false, 567)]);
                    echo "</span>
                                </div>
                                ";
                }
                // line 570
                echo "                                ";
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "cost_48", [], "any", false, false, false, 570) != 0)) {
                    // line 571
                    echo "                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                    // line 572
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Storage Costs after 48 Hours "]);
                    echo "</span>
                                    <span class=\"kt-widget12__value\">";
                    // line 573
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "cost_48", [], "any", false, false, false, 573)]);
                    echo "</span>
                                </div>
                                ";
                }
                // line 576
                echo "                            </div>
                        ";
            }
            // line 578
            echo "                       
                        <div class=\"kt-widget12__item\">
                            ";
            // line 580
            $context["sub_total"] = (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "price_kg", [], "any", false, false, false, 580) * twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "details", [], "any", false, false, false, 580), "weight", [], "any", false, false, false, 580));
            // line 581
            echo "                            ";
            $context["store_fee"] = (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payment_term", [], "any", false, false, false, 581) * twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "cost_24", [], "any", false, false, false, 581));
            // line 582
            echo "                            ";
            $context["total"] = (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "custom_clearance", [], "any", false, false, false, 582) + twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "fiscal_representation", [], "any", false, false, false, 582)) + ($context["sub_total"] ?? null)) + ($context["store_fee"] ?? null));
            // line 583
            echo "                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 584
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Sub Total"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">
                                        ";
            // line 586
            echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [($context["sub_total"] ?? null)]);
            echo "
                                </span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
            // line 590
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Total"]);
            echo "</span>
                                <span class=\"kt-widget12__value\">
                                        ";
            // line 592
            echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [($context["total"] ?? null)]);
            echo "
                                </span>
                            </div>
                        </div>
                        ";
        } else {
            // line 597
            echo "                            ";
            if (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payments", [], "any", false, false, false, 597), "where", [0 => "item_id", 1 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 597), "id", [], "any", false, false, false, 597)], "method", false, false, false, 597), "where", [0 => "for_id", 1 => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 597)], "method", false, false, false, 597), "first", [], "any", false, false, false, 597), "status", [], "any", false, false, false, 597) == 3) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 597) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 597)))) {
                // line 598
                echo "
                            ";
            } elseif (((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,             // line 599
($context["order"] ?? null), "payments", [], "any", false, false, false, 599), "where", [0 => "item_id", 1 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 599), "id", [], "any", false, false, false, 599)], "method", false, false, false, 599), "where", [0 => "for_id", 1 => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_id", [], "any", false, false, false, 599)], "method", false, false, false, 599), "first", [], "any", false, false, false, 599), "status", [], "any", false, false, false, 599) == 3) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_id", [], "any", false, false, false, 599) == twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 599)))) {
                // line 600
                echo "
                            ";
            } else {
                // line 602
                echo "
                                ";
                // line 603
                if (((((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "payment", [], "any", false, false, false, 603), "enable", [], "any", false, false, false, 603) == 1) && twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 603)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 603) != 0)) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 603) != 2))) {
                    // line 604
                    echo "                                    ";
                    if (twig_in_filter("paystack", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "payment", [], "any", false, false, false, 604), "gateways", [], "any", false, false, false, 604))) {
                        // line 605
                        echo "                                        ";
                        if (twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 605), [0 => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_id", [], "any", false, false, false, 605), 1 => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 605)])) {
                            // line 606
                            echo "                                            <div class=\"kt-widget12__item\">
                                                <div class=\"kt-widget12__info\">
                                                    <form class=\"kt-widget12__desc\">
                                                        <script src=\"https://js.paystack.co/v1/inline.js\"></script>
                                                        <button type=\"button\" class=\"btn btn-wide btn-success btn-bold btn-upper btn-elevate btn-elevate-air\" onclick=\"payWithPaystack()\"> <i class=\"fa fa-dollar-sign\"></i> ";
                            // line 610
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["PAY ONLINE"]);
                            echo " </button>
                                                    </form>
                                                    <span class=\"kt-widget12__value\">
                                                        <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"";
                            // line 613
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Paystack only uses GHS currency so that is why we change the money for it"]);
                            echo "\" data-original-title=\"";
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Paystack only uses GHS currency so that is why we change the money for it"]);
                            echo "\" data-skin=\"dark\">";
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The amount will be changed for Ghanaian cedi for using the paystack payment gateway, the changed price will equal the same as you should pay"]);
                            echo "</span>
                                                        <span class=\"btn btn-label-warning btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"";
                            // line 614
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["If you are sender or receiver you will have to pay the amount if its prepaid or postpaid just if you are the resposnsiable of this payment"]);
                            echo "\" data-original-title=\"";
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["If you are sender or receiver you will have to pay the amount if its prepaid or postpaid just if you are the resposnsiable of this payment"]);
                            echo "\" data-skin=\"dark\">";
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The amount you will pay online is what is requested from you only"]);
                            echo "</span>
                                                    </span>
                                                </div>
                                            </div>
                                        ";
                        } else {
                            // line 619
                            echo "                                            <div class=\"alert alert-info\" role=\"alert\">
                                                <div class=\"alert-icon\"><i class=\"flaticon-info\"></i></div>
                                                <div class=\"alert-text\">
                                                    ";
                            // line 622
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Sender and receiver can see the online payment option if they have to pay something they can pay it online"]);
                            echo ".
                                                </div>
                                                <div class=\"alert-close\">
                                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                                        <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        ";
                        }
                        // line 631
                        echo "                                    ";
                    }
                    // line 632
                    echo "                                ";
                }
                // line 633
                echo "
                            ";
            }
            // line 635
            echo "                            ";
            if ((((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "tax", [], "any", false, false, false, 635) != 0) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "insurance", [], "any", false, false, false, 635) != 0)) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "customs_value", [], "any", false, false, false, 635) != 0))) {
                // line 636
                echo "                                <div class=\"kt-widget12__item\">
                                    ";
                // line 637
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "tax", [], "any", false, false, false, 637) != 0)) {
                    // line 638
                    echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                    // line 639
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Tax"]);
                    echo "</span>
                                        <span class=\"kt-widget12__value\">";
                    // line 640
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "tax", [], "any", false, false, false, 640), "html", null, true);
                    echo "%</span>
                                    </div>
                                    ";
                }
                // line 643
                echo "                                    ";
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "insurance", [], "any", false, false, false, 643) != 0)) {
                    // line 644
                    echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                    // line 645
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Insurance"]);
                    echo "</span>
                                        <span class=\"kt-widget12__value\">";
                    // line 646
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "insurance", [], "any", false, false, false, 646), "html", null, true);
                    echo "%</span>
                                    </div>
                                    ";
                }
                // line 649
                echo "                                    ";
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "customs_value", [], "any", false, false, false, 649) != 0)) {
                    // line 650
                    echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                    // line 651
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Customs Value"]);
                    echo "</span>
                                        <span class=\"kt-widget12__value\">";
                    // line 652
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "customs_value", [], "any", false, false, false, 652), "html", null, true);
                    echo "</span>
                                    </div>
                                    ";
                }
                // line 655
                echo "                                </div>
                            ";
            }
            // line 657
            echo "                            ";
            if ((twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "role_id", [], "any", false, false, false, 657), [0 => 1, 1 => 2, 2 => 3, 3 => 6]) || twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "is_superuser", [], "any", false, false, false, 657))) {
                // line 658
                echo "                                <div class=\"kt-widget12__item\">
                                    ";
                // line 659
                if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "type", [], "any", false, false, false, 659) == 1) && (null === twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 659)))) {
                    // line 660
                    echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                    // line 661
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Pickup Cost"]);
                    echo "</span>
                                            <span class=\"kt-widget12__value\">
                                                ";
                    // line 663
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "courier_fee", [], "any", false, false, false, 663)]);
                    echo "
                                                ";
                    // line 664
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payments", [], "any", false, false, false, 664), "where", [0 => "payment_type", 1 => "courier_fee"], "method", false, false, false, 664), "first", [], "any", false, false, false, 664), "status", [], "any", false, false, false, 664) == 3)) {
                        // line 665
                        echo "                                                    <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Paid"]);
                        echo "</span>
                                                ";
                    }
                    // line 667
                    echo "                                            </span>
                                        </div>
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                    // line 670
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Shipping Cost"]);
                    echo "</span>
                                            <span class=\"kt-widget12__value\">
                                                ";
                    // line 672
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "fees", [], "any", false, false, false, 672), "delivery_cost", [], "any", false, false, false, 672)]);
                    echo " <!--";
                    if (twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 672), [0 => 100, 1 => 0, 2 => 1])) {
                        echo "<span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                        echo "\" data-original-title=\"";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                        echo "\" data-skin=\"dark\">";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Not confirmed yet"]);
                        echo "</span>";
                    }
                    echo "-->
                                            </span>
                                        </div>
                                    ";
                } else {
                    // line 676
                    echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                    // line 677
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Shipping Cost"]);
                    echo "</span>
                                            <span class=\"kt-widget12__value\">
                                                ";
                    // line 679
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "courier_fee", [], "any", false, false, false, 679)]);
                    echo " <!--";
                    if (twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 679), [0 => 100, 1 => 0, 2 => 1])) {
                        echo "<span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                        echo "\" data-original-title=\"";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                        echo "\" data-skin=\"dark\">";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Not confirmed yet"]);
                        echo "</span>";
                    }
                    echo "-->
                                            </span>
                                        </div>
                                    ";
                }
                // line 683
                echo "                                    ";
                if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 683) == 1)) {
                    // line 684
                    echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                    // line 685
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Package Cost"]);
                    echo "</span>
                                            <span class=\"kt-widget12__value\">
                                                ";
                    // line 687
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "package_fee", [], "any", false, false, false, 687)]);
                    echo "
                                                ";
                    // line 688
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payments", [], "any", false, false, false, 688), "where", [0 => "payment_type", 1 => "package_fee"], "method", false, false, false, 688), "first", [], "any", false, false, false, 688), "status", [], "any", false, false, false, 688) == 2)) {
                        // line 689
                        echo "                                                    <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\">";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Canceled"]);
                        echo "</span>
                                                ";
                    } elseif ((twig_get_attribute($this->env, $this->source,                     // line 690
($context["order"] ?? null), "requested", [], "any", false, false, false, 690) == 12)) {
                        // line 691
                        echo "                                                    <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Paid"]);
                        echo "</span>
                                                ";
                    }
                    // line 693
                    echo "                                            </span>
                                        </div>
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                    // line 696
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Return Courier Cost"]);
                    echo "</span>
                                            <span class=\"kt-widget12__value\">";
                    // line 697
                    echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_courier_fee", [], "any", false, false, false, 697)]);
                    echo "</span> <!--";
                    if (twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 697), [0 => 100, 1 => 0, 2 => 1])) {
                        echo "<span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                        echo "\" data-original-title=\"";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                        echo "\" data-skin=\"dark\">";
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Not confirmed yet"]);
                        echo "</span>";
                    }
                    echo "-->
                                        </div>
                                    ";
                } else {
                    // line 700
                    echo "                                        ";
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payments", [], "any", false, false, false, 700), "where", [0 => "payment_type", 1 => "return_package_fee"], "method", false, false, false, 700), "first", [], "any", false, false, false, 700)) {
                        // line 701
                        echo "                                            <div class=\"kt-widget12__info\">
                                                <span class=\"kt-widget12__desc\">";
                        // line 702
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Return Courier Cost"]);
                        echo "</span>
                                                <span class=\"kt-widget12__value\">
                                                    ";
                        // line 704
                        echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_courier_fee", [], "any", false, false, false, 704)]);
                        echo "
                                                    ";
                        // line 705
                        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payments", [], "any", false, false, false, 705), "where", [0 => "payment_type", 1 => "return_package_fee"], "method", false, false, false, 705), "first", [], "any", false, false, false, 705), "status", [], "any", false, false, false, 705) == 2)) {
                            // line 706
                            echo "                                                        <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\">";
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Canceled"]);
                            echo "</span>
                                                    ";
                        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,                         // line 707
($context["order"] ?? null), "payments", [], "any", false, false, false, 707), "where", [0 => "payment_type", 1 => "return_package_fee"], "method", false, false, false, 707), "first", [], "any", false, false, false, 707), "status", [], "any", false, false, false, 707) == 3)) {
                            // line 708
                            echo "                                                        <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">";
                            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Paid"]);
                            echo "</span>
                                                    ";
                        }
                        // line 710
                        echo "                                                </span>
                                            </div>
                                        ";
                    }
                    // line 713
                    echo "                                    ";
                }
                // line 714
                echo "                                </div>
                            ";
            }
            // line 716
            echo "                            ";
            if ((($context["addShipmentForm"] ?? null) != "add_form_simple")) {
                // line 717
                echo "                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    ";
                // line 719
                if ((twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 719) == twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 719))) {
                    // line 720
                    echo "                                        <span class=\"kt-widget12__desc\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Total Requested From You"]);
                    echo "</span>
                                    ";
                } else {
                    // line 722
                    echo "                                        <span class=\"kt-widget12__desc\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Total Requested From The Receiver"]);
                    echo "</span>
                                    ";
                }
                // line 724
                echo "                                    <span class=\"kt-widget12__value\">
                                        ";
                // line 725
                echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [($context["receiver_fees"] ?? null)]);
                echo "<!--";
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 725), [0 => 100, 1 => 0, 2 => 1])) {
                    echo " <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                    echo "\" data-original-title=\"";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                    echo "\" data-skin=\"dark\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Not confirmed yet"]);
                    echo "</span>";
                }
                echo "-->
                                        ";
                // line 726
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payments", [], "any", false, false, false, 726), "where", [0 => "item_id", 1 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 726), "id", [], "any", false, false, false, 726)], "method", false, false, false, 726), "where", [0 => "for_id", 1 => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 726)], "method", false, false, false, 726), "first", [], "any", false, false, false, 726), "status", [], "any", false, false, false, 726) == 3)) {
                    // line 727
                    echo "                                            <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Paid"]);
                    echo "</span>
                                        ";
                }
                // line 729
                echo "                                    </span>
                                    <!--
                                        ";
                // line 731
                if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "type", [], "any", false, false, false, 731) == 1) && (null === twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 731)))) {
                    // line 732
                    echo "                                            <span class=\"muted-text kt-font-danger\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The correct fees will be applied after selecting the receiver address"]);
                    echo "</span>
                                        ";
                }
                // line 733
                echo "</span>
                                    -->
                                </div>
                            </div>
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    ";
                // line 739
                if ((twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 739) == twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_id", [], "any", false, false, false, 739))) {
                    // line 740
                    echo "                                        <span class=\"kt-widget12__desc\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Total Requested From You"]);
                    echo "</span>
                                    ";
                } else {
                    // line 742
                    echo "                                        <span class=\"kt-widget12__desc\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Total Requested From The Sender"]);
                    echo "</span>
                                    ";
                }
                // line 744
                echo "                                    <span class=\"kt-widget12__value\">
                                        ";
                // line 745
                echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [($context["sender_fees"] ?? null)]);
                echo "<!--";
                if (twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 745), [0 => 100, 1 => 0, 2 => 1])) {
                    echo " <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                    echo "\" data-original-title=\"";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                    echo "\" data-skin=\"dark\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Not confirmed yet"]);
                    echo "</span>";
                }
                echo "-->
                                        ";
                // line 746
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payments", [], "any", false, false, false, 746), "where", [0 => "item_id", 1 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 746), "id", [], "any", false, false, false, 746)], "method", false, false, false, 746), "where", [0 => "for_id", 1 => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_id", [], "any", false, false, false, 746)], "method", false, false, false, 746), "first", [], "any", false, false, false, 746), "status", [], "any", false, false, false, 746) == 3)) {
                    // line 747
                    echo "                                            <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Paid"]);
                    echo "</span>
                                        ";
                }
                // line 749
                echo "                                    </span>
                                    <!--
                                        ";
                // line 751
                if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "type", [], "any", false, false, false, 751) == 1) && (null === twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 751)))) {
                    // line 752
                    echo "                                            <span class=\"muted-text kt-font-danger\">";
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The correct fees will be applied after selecting the receiver address"]);
                    echo "</span>
                                        ";
                }
                // line 754
                echo "                                    -->
                                </div>
                            </div>
                            ";
            }
            // line 758
            echo "                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
            // line 760
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Total received"]);
            echo "</span>
                                    <span class=\"kt-widget12__value\">";
            // line 761
            echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [($context["total_received"] ?? null)]);
            echo "</span>
                                </div>
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
            // line 764
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Total remaining"]);
            echo "</span>
                                    <span class=\"kt-widget12__value\">
                                        ";
            // line 766
            if ((((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 766) == 1) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_package_fee", [], "any", false, false, false, 766) == 2)) && twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 766), [0 => 4, 1 => 5, 2 => 6, 3 => 8, 4 => 9, 5 => 10, 6 => 11, 7 => 12]))) {
                // line 767
                echo "                                            ";
                echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [($context["total_remaining"] ?? null)]);
                echo " <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\">";
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The requested amount of the sender was deducted from the return"]);
                echo "</span>
                                        ";
            } else {
                // line 769
                echo "                                            ";
                echo call_user_func_array($this->env->getFilter('currency')->getCallable(), [($context["total_remaining"] ?? null)]);
                echo "
                                        ";
            }
            // line 770
            echo " <!--";
            if (twig_in_filter(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "requested", [], "any", false, false, false, 770), [0 => 100, 1 => 0, 2 => 1])) {
                echo "<span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"";
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                echo "\" data-original-title=\"";
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The actual cost will be calculated when we receive the package"]);
                echo "\" data-skin=\"dark\">";
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Not confirmed yet"]);
                echo "</span>";
            }
            echo "-->
                                    </span>
                                </div>
                            </div>
                        ";
        }
        // line 775
        echo "                    </div>
                </div>
        \t</div>
        </div>
        <!--end:: Widgets/Order Statistics-->
    </div>
</div>
<div class=\"row\">
    <div class=\"col-xl-";
        // line 783
        if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "type", [], "any", false, false, false, 783) == 1) && (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 783) == null))) {
            echo "12";
        } else {
            echo "6";
        }
        echo "\">
        <!--begin:: Widgets/Order Statistics-->
        <div class=\"kt-portlet kt-portlet--height-fluid\">
        \t<div class=\"kt-portlet__head\">
        \t\t<div class=\"kt-portlet__head-label\">
        \t\t\t<h3 class=\"kt-portlet__head-title\">
        \t\t\t\t";
        // line 789
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Sender Details"]);
        echo "
        \t\t\t</h3>
        \t\t</div>
        \t</div>
        \t<div class=\"kt-portlet__body kt-portlet__body--fluid\">
        \t\t<div class=\"kt-widget12\">
        \t\t\t<div class=\"kt-widget12__content\">
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
        // line 798
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Name"]);
        echo "</span>
                                <span class=\"kt-widget12__value\">
                                    ";
        // line 800
        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_name", [], "any", false, false, false, 800)) {
            // line 801
            echo "                                        ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_name", [], "any", false, false, false, 801)), "html", null, true);
            echo "
                                    ";
        } else {
            // line 803
            echo "                                        ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 803), "name", [], "any", false, false, false, 803)), "html", null, true);
            echo "
                                    ";
        }
        // line 805
        echo "                                    ";
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 805), "userverify_dateverified", [], "any", false, false, false, 805)) {
            // line 806
            echo "                                        <i class=\"flaticon2-correct kt-font-info\"></i>
                                    ";
        }
        // line 808
        echo "                                </span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
        // line 811
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Mobile"]);
        echo "</span>
                                ";
        // line 812
        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_mobile", [], "any", false, false, false, 812)) {
            // line 813
            echo "                                <span class=\"kt-widget12__value\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_mobile", [], "any", false, false, false, 813), "html", null, true);
            echo "</span>
                                ";
        } else {
            // line 815
            echo "                                <span class=\"kt-widget12__value\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 815), "mobile", [], "any", false, false, false, 815), "html", null, true);
            echo "</span>
                                ";
        }
        // line 817
        echo "                            </div>
                        </div>
                        ";
        // line 819
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 819), "email", [], "any", false, false, false, 819) || twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 819), "gender", [], "any", false, false, false, 819))) {
            // line 820
            echo "                            <div class=\"kt-widget12__item\">
                                ";
            // line 821
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 821), "email", [], "any", false, false, false, 821)) {
                // line 822
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 823
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Email"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">";
                // line 824
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 824), "email", [], "any", false, false, false, 824), "html", null, true);
                echo "</span>
                                    </div>
                                ";
            }
            // line 827
            echo "                                ";
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 827), "gender", [], "any", false, false, false, 827)) {
                // line 828
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 829
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Gender"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">";
                // line 830
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender", [], "any", false, false, false, 830), "gender", [], "any", false, false, false, 830), "html", null, true);
                echo "</span>
                                    </div>
                                ";
            }
            // line 833
            echo "                            </div>
                        ";
        }
        // line 835
        echo "                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
        // line 837
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Sender Address"]);
        echo "</span>
                                <span class=\"kt-widget12__value\">
                                    ";
        // line 839
        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_addr", [], "any", false, false, false, 839)) {
            // line 840
            echo "                                        ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_addr", [], "any", false, false, false, 840)), "html", null, true);
            echo "
                                    ";
        } else {
            // line 842
            echo "                                        ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 842), "name", [], "any", false, false, false, 842)), "html", null, true);
            echo "
                                    ";
        }
        // line 844
        echo "                                </span>
                            </div>
                        </div>
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">";
        // line 849
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Street"]);
        echo "</span>
                                <span class=\"kt-widget12__value\">
                                    ";
        // line 851
        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_addr", [], "any", false, false, false, 851)) {
            // line 852
            echo "                                        ";
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_addr", [], "any", false, false, false, 852)), "html", null, true);
            echo "
                                    ";
        } else {
            // line 854
            echo "                                        ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 854), "street", [], "any", false, false, false, 854), "html", null, true);
            echo "
                                    ";
        }
        // line 856
        echo "                                </span>
                            </div>
                        </div>
                        ";
        // line 859
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 859), "area", [], "any", false, false, false, 859) || twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 859), "thecity", [], "any", false, false, false, 859))) {
            // line 860
            echo "                            <div class=\"kt-widget12__item\">
                                ";
            // line 861
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 861), "area", [], "any", false, false, false, 861)) {
                // line 862
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 863
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["County"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">
                                            ";
                // line 865
                if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_area_id", [], "any", false, false, false, 865)) {
                    // line 866
                    echo "                                                ";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_area_id", [], "any", false, false, false, 866)), "html", null, true);
                    echo "
                                            ";
                } else {
                    // line 868
                    echo "                                                ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 868), "area", [], "any", false, false, false, 868), "name", [], "any", false, false, false, 868), "html", null, true);
                    echo "
                                            ";
                }
                // line 870
                echo "                                        </span>
                                    </div>
                                ";
            }
            // line 873
            echo "                                ";
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 873), "thecity", [], "any", false, false, false, 873)) {
                // line 874
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 875
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["City"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">
                                            ";
                // line 877
                if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_city_id", [], "any", false, false, false, 877)) {
                    // line 878
                    echo "                                                ";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_city_id", [], "any", false, false, false, 878)), "html", null, true);
                    echo "
                                            ";
                } else {
                    // line 880
                    echo "                                                ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 880), "thecity", [], "any", false, false, false, 880), "name", [], "any", false, false, false, 880), "html", null, true);
                    echo "
                                            ";
                }
                // line 882
                echo "                                        </span>
                                    </div>
                                ";
            }
            // line 885
            echo "                            </div>
                        ";
        }
        // line 887
        echo "                        ";
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 887), "thestate", [], "any", false, false, false, 887) || twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 887), "thecountry", [], "any", false, false, false, 887))) {
            // line 888
            echo "                            <div class=\"kt-widget12__item\">
                                ";
            // line 889
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 889), "thestate", [], "any", false, false, false, 889)) {
                // line 890
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 891
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["State"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">
                                            ";
                // line 893
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 893), "thestate", [], "any", false, false, false, 893), "name", [], "any", false, false, false, 893), "html", null, true);
                echo "
                                        </span>
                                    </div>
                                ";
            }
            // line 897
            echo "                                ";
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 897), "thecountry", [], "any", false, false, false, 897)) {
                // line 898
                echo "                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">";
                // line 899
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Country"]);
                echo "</span>
                                        <span class=\"kt-widget12__value\">
                                            ";
                // line 901
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 901), "thecountry", [], "any", false, false, false, 901), "name", [], "any", false, false, false, 901), "html", null, true);
                echo "
                                        </span>
                                    </div>
                                ";
            }
            // line 905
            echo "                            </div>
                        ";
        }
        // line 907
        echo "                        ";
        if ((($context["addShipmentForm"] ?? null) == "add_form_advance")) {
            // line 908
            echo "                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                        \t\t<div class=\"kt-widget15\">
                        \t\t\t<div class=\"kt-widget15__map\">
                        \t\t\t\t<div id=\"kt_map_sender\" style=\"height:320px;\"></div>
                        \t\t\t</div>
                                </div>
                            </div>
                        </div>
                        ";
        }
        // line 918
        echo "        \t\t\t</div>
        \t\t</div>
        \t</div>
        </div>
        <!--end:: Widgets/Order Statistics-->
    </div>
    ";
        // line 924
        if ((($context["addShipmentForm"] ?? null) != "add_form_normal")) {
            // line 925
            echo "    ";
            if (((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "type", [], "any", false, false, false, 925) == 2) || (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 925) != null))) {
                // line 926
                echo "        <div class=\"col-xl-6\">
            <!--begin:: Widgets/Order Statistics-->
            <div class=\"kt-portlet kt-portlet--height-fluid\">
            \t<div class=\"kt-portlet__head\">
            \t\t<div class=\"kt-portlet__head-label\">
            \t\t\t<h3 class=\"kt-portlet__head-title\">
            \t\t\t\t";
                // line 932
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver Details"]);
                echo "
            \t\t\t</h3>
            \t\t</div>
            \t</div>
            \t<div class=\"kt-portlet__body kt-portlet__body--fluid\">
            \t\t<div class=\"kt-widget12\">
            \t\t\t<div class=\"kt-widget12__content\">
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                // line 941
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Name"]);
                echo "</span>
                                    <span class=\"kt-widget12__value\">
                                        ";
                // line 943
                if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_name", [], "any", false, false, false, 943)) {
                    // line 944
                    echo "                                            ";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_name", [], "any", false, false, false, 944)), "html", null, true);
                    echo "
                                        ";
                } else {
                    // line 946
                    echo "                                            ";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 946), "name", [], "any", false, false, false, 946)), "html", null, true);
                    echo "
                                        ";
                }
                // line 948
                echo "                                        ";
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 948), "userverify_dateverified", [], "any", false, false, false, 948)) {
                    // line 949
                    echo "                                            <i class=\"flaticon2-correct kt-font-info\"></i>
                                        ";
                }
                // line 951
                echo "                                    </span>
                                </div>
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                // line 954
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Mobile"]);
                echo "</span>
                                    ";
                // line 955
                if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_mobile", [], "any", false, false, false, 955)) {
                    // line 956
                    echo "                                    <span class=\"kt-widget12__value\">";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_mobile", [], "any", false, false, false, 956)), "html", null, true);
                    echo "</span>
                                    ";
                } else {
                    // line 958
                    echo "                                    <span class=\"kt-widget12__value\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 958), "mobile", [], "any", false, false, false, 958), "html", null, true);
                    echo "</span>
                                    ";
                }
                // line 960
                echo "                                </div>
                            </div>
                            ";
                // line 962
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 962), "email", [], "any", false, false, false, 962) || twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 962), "gender", [], "any", false, false, false, 962))) {
                    // line 963
                    echo "                                <div class=\"kt-widget12__item\">
                                    ";
                    // line 964
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 964), "email", [], "any", false, false, false, 964)) {
                        // line 965
                        echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                        // line 966
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Email"]);
                        echo "</span>
                                            <span class=\"kt-widget12__value\">";
                        // line 967
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 967), "email", [], "any", false, false, false, 967), "html", null, true);
                        echo "</span>
                                        </div>
                                    ";
                    }
                    // line 970
                    echo "                                    ";
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 970), "gender", [], "any", false, false, false, 970)) {
                        // line 971
                        echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                        // line 972
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Gender"]);
                        echo "</span>
                                            <span class=\"kt-widget12__value\">";
                        // line 973
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 973), "gender", [], "any", false, false, false, 973), "html", null, true);
                        echo "</span>
                                        </div>
                                    ";
                    }
                    // line 976
                    echo "                                </div>
                            ";
                }
                // line 978
                echo "                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                // line 980
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver Address"]);
                echo "</span>
                                    <span class=\"kt-widget12__value\">
                                        ";
                // line 982
                if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_addr", [], "any", false, false, false, 982)) {
                    // line 983
                    echo "                                            ";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_addr", [], "any", false, false, false, 983)), "html", null, true);
                    echo "
                                        ";
                } else {
                    // line 985
                    echo "                                            ";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 985), "name", [], "any", false, false, false, 985)), "html", null, true);
                    echo "
                                        ";
                }
                // line 987
                echo "                                    </span>
                                </div>
                            </div>
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">";
                // line 992
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Street"]);
                echo "</span>
                                    <span class=\"kt-widget12__value\">
                                        ";
                // line 994
                if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_addr", [], "any", false, false, false, 994)) {
                    // line 995
                    echo "                                            ";
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_addr", [], "any", false, false, false, 995)), "html", null, true);
                    echo "
                                        ";
                } else {
                    // line 997
                    echo "                                            ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 997), "street", [], "any", false, false, false, 997), "html", null, true);
                    echo "
                                        ";
                }
                // line 999
                echo "                                    </span>
                                </div>
                            </div>
                            ";
                // line 1002
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1002), "area", [], "any", false, false, false, 1002) || twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1002), "thecity", [], "any", false, false, false, 1002))) {
                    // line 1003
                    echo "                                <div class=\"kt-widget12__item\">
                                    ";
                    // line 1004
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1004), "area", [], "any", false, false, false, 1004)) {
                        // line 1005
                        echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                        // line 1006
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["County"]);
                        echo "</span>
                                            <span class=\"kt-widget12__value\">
                                                ";
                        // line 1008
                        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_area_id", [], "any", false, false, false, 1008)) {
                            // line 1009
                            echo "                                                    ";
                            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_area_id", [], "any", false, false, false, 1009)), "html", null, true);
                            echo "
                                                ";
                        } else {
                            // line 1011
                            echo "                                                    ";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1011), "area", [], "any", false, false, false, 1011), "name", [], "any", false, false, false, 1011), "html", null, true);
                            echo "
                                                ";
                        }
                        // line 1013
                        echo "                                            </span>
                                        </div>
                                    ";
                    }
                    // line 1016
                    echo "                                    ";
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1016), "thecity", [], "any", false, false, false, 1016)) {
                        // line 1017
                        echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                        // line 1018
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["City"]);
                        echo "</span>
                                            <span class=\"kt-widget12__value\">
                                                ";
                        // line 1020
                        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_city_id", [], "any", false, false, false, 1020)) {
                            // line 1021
                            echo "                                                    ";
                            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_city_id", [], "any", false, false, false, 1021)), "html", null, true);
                            echo "
                                                ";
                        } else {
                            // line 1023
                            echo "                                                    ";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1023), "thecity", [], "any", false, false, false, 1023), "name", [], "any", false, false, false, 1023), "html", null, true);
                            echo "
                                                ";
                        }
                        // line 1025
                        echo "                                            </span>
                                        </div>
                                    ";
                    }
                    // line 1028
                    echo "                                </div>
                            ";
                }
                // line 1030
                echo "                            ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1030), "thestate", [], "any", false, false, false, 1030) || twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1030), "thecountry", [], "any", false, false, false, 1030))) {
                    // line 1031
                    echo "                                <div class=\"kt-widget12__item\">
                                    ";
                    // line 1032
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1032), "thestate", [], "any", false, false, false, 1032)) {
                        // line 1033
                        echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                        // line 1034
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["State"]);
                        echo "</span>
                                            <span class=\"kt-widget12__value\">
                                                ";
                        // line 1036
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1036), "thestate", [], "any", false, false, false, 1036), "name", [], "any", false, false, false, 1036), "html", null, true);
                        echo "
                                            </span>
                                        </div>
                                    ";
                    }
                    // line 1040
                    echo "                                    ";
                    if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1040), "thecountry", [], "any", false, false, false, 1040)) {
                        // line 1041
                        echo "                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">";
                        // line 1042
                        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Country"]);
                        echo "</span>
                                            <span class=\"kt-widget12__value\">
                                                ";
                        // line 1044
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1044), "thecountry", [], "any", false, false, false, 1044), "name", [], "any", false, false, false, 1044), "html", null, true);
                        echo "
                                            </span>
                                        </div>
                                    ";
                    }
                    // line 1048
                    echo "                                </div>
                            ";
                }
                // line 1050
                echo "                            ";
                if ((($context["addShipmentForm"] ?? null) != "add_form_simple")) {
                    // line 1051
                    echo "                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                            \t\t<div class=\"kt-widget15\">
                            \t\t\t<div class=\"kt-widget15__map\">
                            \t\t\t\t<div id=\"kt_map_receiver\" style=\"height:320px;\"></div>
                            \t\t\t</div>
                                    </div>
                                </div>
                            </div>
                            ";
                }
                // line 1061
                echo "            \t\t\t</div>
            \t\t</div>
            \t</div>
            </div>
            <!--end:: Widgets/Order Statistics-->
        </div>
    ";
            }
            // line 1068
            echo "    ";
        }
        // line 1069
        echo "</div>
<div class=\"row\">
    <div class=\"col-xl-6\">
        <!--begin:: Widgets/Last Updates-->
        <div class=\"kt-portlet kt-portlet--height-fluid\">
            <div class=\"kt-portlet__head\">
                <div class=\"kt-portlet__head-label\">
                    <h3 class=\"kt-portlet__head-title\">";
        // line 1076
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Latest Updates"]);
        echo "</h3>
                </div>
            </div>
            <div class=\"kt-portlet__body\">
                <div class=\"kt-scroll\" data-scroll=\"true\" style=\"height: 300px\">
                    ";
        // line 1081
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "activities", [], "any", false, false, false, 1081));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 1082
            echo "                        <!--begin::widget 12-->
                        <div class=\"kt-widget4\">
                            <div class=\"kt-widget4__item\">
                                <span class=\"kt-widget4__icon\"><i class=\"flaticon-pie-chart-1 kt-font-info\"></i></span>
                                <div class=\"kt-widget4__info\">
        \t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-widget4__username\">
                                        ";
            // line 1088
            if (twig_get_attribute($this->env, $this->source, $context["activity"], "other", [], "any", false, false, false, 1088)) {
                // line 1089
                echo "                                            ";
                $context["text"] = ("activity_" . twig_get_attribute($this->env, $this->source, $context["activity"], "description", [], "any", false, false, false, 1089));
                // line 1090
                echo "                                            ";
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), [($context["text"] ?? null)]);
                echo ": ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["activity"], "other", [], "any", false, false, false, 1090), "html", null, true);
                echo "
                                        ";
            } else {
                // line 1092
                echo "                                            ";
                $context["text"] = ("activity_" . twig_get_attribute($this->env, $this->source, $context["activity"], "description", [], "any", false, false, false, 1092));
                // line 1093
                echo "            \t\t\t\t\t\t\t\t";
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), [($context["text"] ?? null)]);
                echo "
                                        ";
            }
            // line 1095
            echo "        \t\t\t\t\t\t\t</a>
        \t\t\t\t\t\t\t<p class=\"kt-widget4__text\">
        \t\t\t\t\t\t\t\t";
            // line 1097
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["activity"], "created_at", [], "any", false, false, false, 1097), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "dateformat", [], "any", false, false, false, 1097)), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["activity"], "created_at", [], "any", false, false, false, 1097), "h:i"), "html", null, true);
            echo " ";
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), [twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["activity"], "created_at", [], "any", false, false, false, 1097), "a")]);
            echo "
        \t\t\t\t\t\t\t</p>
        \t\t\t\t\t\t</div>
                                <span class=\"kt-widget4__number kt-font-info\">";
            // line 1100
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["activity"], "user", [], "any", false, false, false, 1100), "name", [], "any", false, false, false, 1100), "html", null, true);
            echo "</span>
                            </div>
                        </div>
                        <!--end::Widget 12-->
                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 1105
            echo "                        <div class=\"kt-notification\">
                            <div class=\"kt-notification__item\">
                                <div class=\"kt-notification__item-icon\">
                                    <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\" class=\"kt-svg-icon kt-svg-icon--brand\">
                                        <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">
                                            <rect x=\"0\" y=\"0\" width=\"24\" height=\"24\"/>
                                            <path d=\"M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z\" fill=\"#000000\"/>
                                            <rect fill=\"#000000\" opacity=\"0.3\" transform=\"translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) \" x=\"16.3255682\" y=\"2.94551858\" width=\"3\" height=\"18\" rx=\"1\"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class=\"kt-notification__item-details\">
                                    <div class=\"kt-notification__item-title\">
                                        ";
            // line 1118
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["No activities found"]);
            echo ".
                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1124
        echo "                </div>
            </div>
        </div>
        <!--end:: Widgets/Last Updates-->
    </div>
    <div class=\"col-xl-6\" id=\"notes_continer\">

        <div class=\"kt-portlet kt-portlet--height-fluid\">
            <div class=\"kt-portlet__head\">
                <div class=\"kt-portlet__head-label\">
                    <h3 class=\"kt-portlet__head-title\">";
        // line 1134
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Notes"]);
        echo "</h3>
                </div>
            </div>
            <div class=\"kt-portlet__body\">
                ";
        // line 1138
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onNote", ["success" => "flashRequest(data.type,data.message);\$('#note_message').val('');", "class" => "kt_form notes"]]);
        echo "
                    <div class=\"form-group form-group-last kt-hide\">
                        <div class=\"alert alert-danger kt_form_msg_notes\" role=\"alert\">
                            <div class=\"alert-icon\"><i class=\"flaticon-warning\"></i></div>
                            <div class=\"alert-text\">
                                ";
        // line 1143
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Oh snap! Change a few things up and try submitting again"]);
        echo ".
                            </div>
                            <div class=\"alert-close\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                    <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <textarea class=\"form-control\" id=\"note_message\" rows=\"3\" name=\"note\" placeholder=\"";
        // line 1153
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Write your notes"]);
        echo "\" required></textarea>
                    </div>
                    <div class=\"row\">
                        <div class=\"col\">
                            <button type=\"submit\" class=\"btn btn-label-brand btn-bold\">";
        // line 1157
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Add note"]);
        echo "</button>
                            <button type=\"reset\" class=\"btn btn-clean btn-bold\">";
        // line 1158
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Cancel"]);
        echo "</button>
                        </div>
                    </div>
                ";
        // line 1161
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "

                <div class=\"kt-separator kt-separator--space-lg kt-separator--border-dashed\"></div>
                <div id=\"notes\">
                    ";
        // line 1165
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['item'] = ($context["order"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("notes"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 1166
        echo "                </div>
            </div>
        </div>
    </div>
    <!--
        <div class=\"col-xl-6 kt-todo\">
            <div class=\"kt-grid__item kt-grid__item--fluid  kt-portlet kt-portlet--height-fluid kt-todo__list\" id=\"kt_todo_list\">
                <div class=\"kt-portlet__body kt-portlet__body--fit-x\">
                    <div class=\"kt-todo__head\">
                        <div class=\"kt-todo__toolbar\">
                            <div class=\"kt-todo__info\">
                                <span class=\"kt-todo__name\">
                                    ";
        // line 1178
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Order Tasks"]);
        echo "
                                </span>
                            </div>
                            <div class=\"kt-todo__details\">
                                <a href=\"javascript:;\" class=\"btn btn-label-success btn-upper btn-sm btn-bold\">";
        // line 1182
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["New Task"]);
        echo "</a>
                            </div>
                        </div>
                    </div>

                    <div class=\"kt-todo__body\">
                        ";
        // line 1188
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tasks"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
            // line 1189
            echo "                            <div class=\"kt-todo__items\" data-type=\"task\">
                                <div class=\"kt-todo__item kt-todo__item--unread\" data-id=\"1\" data-type=\"task\">
                                    <div class=\"kt-todo__info\">
                                        <div class=\"kt-todo__actions\">
                                            <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand\">
                                                <input type=\"checkbox\">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=\"kt-todo__details\" data-toggle=\"view\">
                                        <div class=\"kt-todo__message\">
                                            <span class=\"kt-todo__subject\">";
            // line 1201
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "title", [], "any", false, false, false, 1201), "html", null, true);
            echo "</span>
                                        </div>
                                        <div class=\"kt-todo__labels\">
                                            <span class=\"kt-todo__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline\">";
            // line 1204
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "comment", [], "any", false, false, false, 1204), "html", null, true);
            echo "</span>
                                        </div>
                                    </div>
                                    <div class=\"kt-todo__datetime\" data-toggle=\"view\">
                                        8:30 PM
                                    </div>
                                    <div class=\"kt-todo__sender\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"";
            // line 1210
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["task"], "user", [], "any", false, false, false, 1210), "name", [], "any", false, false, false, 1210), "html", null, true);
            echo "\">
                                        ";
            // line 1211
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["task"], "user", [], "any", false, false, false, 1211), "avatar", [], "any", false, false, false, 1211)) {
                // line 1212
                echo "                                             <span class=\"kt-media kt-media--sm kt-media--danger kt-hidden\" style=\"background-image: url('";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["task"], "user", [], "any", false, false, false, 1212), "avatar", [], "any", false, false, false, 1212), "thumb", [0 => 100, 1 => 100, 2 => "crop"], "method", false, false, false, 1212), "html", null, true);
                echo "')\">
                                                 <span></span>
                                             </span>
                                        ";
            } else {
                // line 1216
                echo "                                            <div class=\"kt-todo__sender\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["task"], "user", [], "any", false, false, false, 1216), "name", [], "any", false, false, false, 1216), "html", null, true);
                echo "\">
                                                <span class=\"kt-media kt-media--sm kt-media--brand\">
                                                    <span>";
                // line 1218
                echo twig_escape_filter($this->env, twig_slice($this->env, twig_capitalize_string_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["task"], "user", [], "any", false, false, false, 1218), "name", [], "any", false, false, false, 1218)), 0, 2), "html", null, true);
                echo "</span>
                                                </span>
                                            </div>
                                       ";
            }
            // line 1222
            echo "                                    </div>
                                </div>
                            </div>
                        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 1226
            echo "                            <div class=\"kt-todo__items\" data-type=\"task\">
                                <div class=\"kt-todo__item kt-todo__item--unread\" data-type=\"task\">
                                    <div class=\"kt-todo__details\" data-toggle=\"view\">
                                        <div class=\"kt-todo__message\">
                                            <span class=\"kt-todo__subject\">";
            // line 1230
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["No tasks found"]);
            echo ".</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1236
        echo "                    </div>
                    ";
        // line 1237
        if (($context["tasks"] ?? null)) {
            // line 1238
            echo "                        <div class=\"kt-todo__foot\">
                            <div class=\"kt-todo__toolbar\">
                                <div class=\"kt-todo__controls\">
                                     ";
            // line 1241
            echo twig_get_attribute($this->env, $this->source, ($context["tasks"] ?? null), "render", [], "any", false, false, false, 1241);
            echo "
                                     <button class=\"kt-todo__icon\" data-toggle=\"kt-tooltip\" title=\"Previose page\">
                                         <i class=\"flaticon2-left-arrow\"></i>
                                     </button>

                                     <button class=\"kt-todo__icon\" data-toggle=\"kt-tooltip\" title=\"Next page\">
                                         <i class=\"flaticon2-right-arrow\"></i>
                                     </button>
                                </div>
                            </div>
                        </div>
                    ";
        }
        // line 1253
        echo "                </div>
            </div>
        </div>
    -->
</div>

<div class=\"modal fade\" id=\"assign_employee\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >";
        // line 1263
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Assign Employee"]);
        echo "</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>
            ";
        // line 1267
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onAssign", ["success" => "created successfully!", "flash" => true, "class" => "kt_form"]]);
        echo "
                <div class=\"modal-body\">
                    ";
        // line 1269
        if (($context["no_employees"] ?? null)) {
            // line 1270
            echo "                        <div class=\"alert alert-warning\" role=\"alert\">
                            <div class=\"alert-icon\"><i class=\"flaticon-warning\"></i></div>
                            <div class=\"alert-text\">
                                ";
            // line 1273
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["No employees cover that shipment area, so here are all the employees to choose from"]);
            echo ".
                            </div>
                            <div class=\"alert-close\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                    <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>
                                </button>
                            </div>
                        </div>
                    ";
        }
        // line 1282
        echo "                    <div class=\"row\">
                        <div class=\"form-group col-lg-12\">
                            <label>";
        // line 1284
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["To who?"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control \" name=\"assigned_id\" data-live-search=\"true\">
                                <option data-hidden=\"true\"></option>
                                ";
        // line 1287
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["employees"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["employee"]) {
            // line 1288
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["employee"], "id", [], "any", false, false, false, 1288), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["employee"], "name", [], "any", false, false, false, 1288), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['employee'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1290
        echo "                            </select>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 1295
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Close"]);
        echo "</button>
                    <button type=\"submit\" class=\"btn btn-primary\">";
        // line 1296
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Assign"]);
        echo "</button>
                </div>
            ";
        // line 1298
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"assign_manifest\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >";
        // line 1306
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Assign Manifest"]);
        echo "</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>
            ";
        // line 1310
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onManifest", ["success" => "created successfully!", "flash" => true, "class" => "kt_form"]]);
        echo "
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"form-group col-lg-12\">
                            <label>";
        // line 1314
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["To which manifest?"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control \" name=\"manifest_id\" data-live-search=\"true\">
                                <option data-hidden=\"true\"></option>
                                ";
        // line 1317
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["manifestes"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["manifest"]) {
            // line 1318
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["manifest"], "id", [], "any", false, false, false, 1318), "html", null, true);
            echo "\">#";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["manifest"], "id", [], "any", false, false, false, 1318), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["manifest"], "driver", [], "any", false, false, false, 1318), "name", [], "any", false, false, false, 1318), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manifest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1320
        echo "                            </select>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 1325
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Close"]);
        echo "</button>
                    <button type=\"submit\" class=\"btn btn-primary\">";
        // line 1326
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Assign"]);
        echo "</button>
                </div>
            ";
        // line 1328
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"transfer_office\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >";
        // line 1336
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Transfer to office"]);
        echo "</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>
            ";
        // line 1340
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onTransfer", ["success" => "created successfully!", "flash" => true, "class" => "kt_form"]]);
        echo "
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"form-group col-lg-12\">
                            <label>";
        // line 1344
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["To which office?"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control \" name=\"office_id\" data-live-search=\"true\">
                                <option data-hidden=\"true\"></option>
                                ";
        // line 1347
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["offices"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["office"]) {
            // line 1348
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["office"], "id", [], "any", false, false, false, 1348), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["office"], "name", [], "any", false, false, false, 1348), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['office'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1350
        echo "                            </select>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 1355
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Close"]);
        echo "</button>
                    <button type=\"submit\" class=\"btn btn-primary\">";
        // line 1356
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Transfer"]);
        echo "</button>
                </div>
            ";
        // line 1358
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"postpone\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >";
        // line 1366
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Postpone"]);
        echo "</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>
            ";
        // line 1370
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onPostpone", ["success" => "created successfully!", "flash" => true, "class" => "kt_form"]]);
        echo "
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"form-group col-lg-12\">
                        <label>";
        // line 1374
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["To Date"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                        <div class=\"input-group date\">
                            <input type=\"text\" class=\"form-control date\" readonly=\"\" name=\"ship_date\" value=\"";
        // line 1376
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_date_modify_filter($this->env, ($context["now"] ?? null), "+1 day"), twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "dateformat", [], "any", false, false, false, 1376)), "html", null, true);
        echo "\" required>
                            <div class=\"input-group-append\">
                                <span class=\"input-group-text\">
                                    <i class=\"la la-calendar\"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"form-group col-lg-12\">
                        <label>";
        // line 1387
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Reason"]);
        echo "</label>
                        <textarea class=\"form-control\" name=\"message\"></textarea>
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 1393
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Close"]);
        echo "</button>
                <button type=\"submit\" class=\"btn btn-primary\">";
        // line 1394
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Postpone"]);
        echo "</button>
            </div>
            ";
        // line 1396
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"refuse\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >";
        // line 1404
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Refuse"]);
        echo "</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>

            ";
        // line 1409
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onRefuse", ["success" => "created successfully!", "flash" => true, "class" => "kt_form"]]);
        echo "
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"form-group col-lg-12\">
                        <label>";
        // line 1413
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Reason"]);
        echo "</label>
                        <textarea class=\"form-control\" name=\"message\"></textarea>
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 1419
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Close"]);
        echo "</button>
                <button type=\"submit\" class=\"btn btn-primary\">";
        // line 1420
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Refuse"]);
        echo "</button>
            </div>
            ";
        // line 1422
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"discards\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >";
        // line 1430
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Transfer as discards"]);
        echo "</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>

            ";
        // line 1435
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onDiscards", ["success" => "created successfully!", "flash" => true, "class" => "kt_form"]]);
        echo "
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"form-group col-lg-12\">
                        <label>";
        // line 1439
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Reason"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                        <select class=\"form-control \" id=\"discard_reason\" name=\"message\" data-live-search=\"true\" required>
                            <option data-hidden=\"true\"></option>
                            <option value=\"1\">";
        // line 1442
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver request"]);
        echo "</option>
                            <option value=\"2\">";
        // line 1443
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver evade"]);
        echo "</option>
                            <option value=\"3\">";
        // line 1444
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Sender request"]);
        echo "</option>
                        </select>
                    </div>
                </div>
                <div class=\"discard_reason_receiver kt-hidden\">
                    <div class=\"row\">
                        <div class=\"form-group form-group-last kt-hide\">
                            <div class=\"alert alert-danger kt_form_msg\" role=\"alert\">
                                <div class=\"alert-icon\"><i class=\"flaticon-warning\"></i></div>
                                <div class=\"alert-text\">
                                    ";
        // line 1454
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Oh snap! Change a few things up and try submitting again"]);
        echo ".
                                </div>
                                <div class=\"alert-close\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                        <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class=\"form-group col-lg-12\">
                            <label>";
        // line 1464
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Return package fees responsiable"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"col-lg-9 col-xl-6\">
                                <div class=\"kt-radio-inline\">
                                    <label class=\"kt-radio\">
                                        <input type=\"radio\" name=\"return_package_fee\" class=\"return_package_fee\" value=\"1\" ";
        // line 1468
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_package_fee", [], "any", false, false, false, 1468) == 1)) {
            echo "checked";
        }
        echo " required> ";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver"]);
        echo "
                                        <span></span>
                                    </label>
                                    <label class=\"kt-radio\">
                                        <input type=\"radio\" name=\"return_package_fee\" class=\"return_package_fee\" value=\"2\" ";
        // line 1472
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_package_fee", [], "any", false, false, false, 1472) == 2)) {
            echo "checked";
        }
        echo " required> ";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Sender"]);
        echo "
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group row\">
                        ";
        // line 1480
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "payments", [], "any", false, false, false, 1480), "where", [0 => "for_id", 1 => twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_id", [], "any", false, false, false, 1480)], "method", false, false, false, 1480));
        foreach ($context['_seq'] as $context["_key"] => $context["payment"]) {
            // line 1481
            echo "                            <label class=\"col-lg-9 col-form-label kt-align-left\" for=\"payment_";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["payment"], "id", [], "any", false, false, false, 1481), "html", null, true);
            echo "\">
                                ";
            // line 1482
            if (twig_get_attribute($this->env, $this->source, $context["payment"], "payment_type", [], "any", false, false, false, 1482)) {
                echo call_user_func_array($this->env->getFilter('__')->getCallable(), [twig_get_attribute($this->env, $this->source, $context["payment"], "payment_type", [], "any", false, false, false, 1482)]);
                echo ": ";
            }
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Is it paid ?"]);
            echo "
                            </label>
                            <div class=\"col-lg-3\">
                                <div class=\"kt-checkbox-single\">
                                    <label class=\"kt-checkbox\">
                                        <input type=\"checkbox\" name=\"payments[]\" id=\"payment_";
            // line 1487
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["payment"], "id", [], "any", false, false, false, 1487), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["payment"], "id", [], "any", false, false, false, 1487), "html", null, true);
            echo "\">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1493
        echo "                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 1497
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Close"]);
        echo "</button>
                <button type=\"submit\" class=\"btn btn-primary\">";
        // line 1498
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Discard"]);
        echo "</button>
            </div>
            ";
        // line 1500
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"received\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >";
        // line 1508
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver information"]);
        echo "</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>

            ";
        // line 1513
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onDeliver", ["success" => "created successfully!", "flash" => true, "class" => "kt_form", "novalidate" => true]]);
        echo "
                <div class=\"modal-body\">
                    <div class=\"row\">

                        <div class=\"form-group col-lg-12\">
                            <label>";
        // line 1518
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver"]);
        echo "/";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Client"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control clients col-lg-12\" name=\"receiver_id\" id=\"receiver_id\" required>
                                <option data-hidden=\"true\"></option>
                                ";
        // line 1521
        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 1521)) {
            // line 1522
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 1522), "id", [], "any", false, false, false, 1522), "html", null, true);
            echo "\" selected>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 1522), "name", [], "any", false, false, false, 1522), "html", null, true);
            echo "</option>
                                ";
        }
        // line 1524
        echo "                                ";
        if (twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "hasUserPermission", [0 => "client", 1 => "c"], "method", false, false, false, 1524)) {
            // line 1525
            echo "                                    <option value=\"new\" data-icon=\"flaticon2-add\">";
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Add New"]);
            echo "</option>
                                ";
        }
        // line 1527
        echo "                            </select>
                        </div>
                        <div class=\"form-group col-lg-12\">
                            <label>";
        // line 1530
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver Address"]);
        echo "/";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Client Address"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control receiver_address_id\" name=\"receiver_address_id\" id=\"receiver_address_id\" data-live-search=\"true\" title=\"";
        // line 1531
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please select receiver first"]);
        echo "\" required>
                                <option data-hidden=\"true\"></option>
                                ";
        // line 1533
        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver", [], "any", false, false, false, 1533)) {
            // line 1534
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1534), "id", [], "any", false, false, false, 1534), "html", null, true);
            echo "\" selected>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 1534), "name", [], "any", false, false, false, 1534), "html", null, true);
            echo "</option>
                                ";
        }
        // line 1536
        echo "                            </select>
                        </div>
                        <div class=\"col-lg-12 kt-hidden\" id=\"addnewreceiver\">
                            <div class=\"kt-portlet kt-portlet--bordered kt-portlet--head--noborder kt-margin-b-0\">
                                <div class=\"kt-portlet__head\">
                                    <div class=\"kt-portlet__head-label\">
                                        <h3 class=\"kt-portlet__head-title\">
                                            ";
        // line 1543
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Add a new client"]);
        echo "
                                        </h3>
                                    </div>
                                </div>
                                <div class=\"kt-portlet__body\">
                                    <div class=\"row\">
                                        <div class=\"form-group col-lg-12\">
                                            <label>";
        // line 1550
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Client Name"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                            <input type=\"text\" class=\"form-control name\" name=\"receiver[name]\" required />
                                        </div>
                                        <div class=\"form-group col-lg-6\">
                                            <label>";
        // line 1554
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Mobile"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                            <input type=\"text\" class=\"form-control mobile\" name=\"receiver[mobile]\" required />
                                        </div>
                                        <div class=\"form-group col-lg-6\">
                                            <label>";
        // line 1558
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Gender"]);
        echo "</label>
                                            <div class=\"kt-radio-inline\">
                                                <label class=\"kt-radio\">
                                                    <input type=\"radio\" name=\"receiver[gender]\" class=\"gender\" value=\"male\"> ";
        // line 1561
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Male"]);
        echo "
                                                    <span></span>
                                                </label>
                                                <label class=\"kt-radio\">
                                                    <input type=\"radio\" name=\"receiver[gender]\" class=\"gender\" value=\"female\"> ";
        // line 1565
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Female"]);
        echo "
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"location-receiver\">
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-12\">
                                                <label>";
        // line 1574
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Address"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                <input type=\"text\" class=\"form-control address street_addr\" data-receiver=\"route\" name=\"receiver[street_address]\"  rel=\"receiver\" required/>
                                                <input type=\"hidden\" class=\"form-control lat\" data-receiver=\"lat\" name=\"receiver[lat]\" />
                                                <input type=\"hidden\" class=\"form-control lng\" data-receiver=\"lng\" name=\"receiver[lng]\" />
                                                <input type=\"hidden\" class=\"form-control url\" data-receiver=\"url\" name=\"receiver[url]\" />
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>";
        // line 1581
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Country"]);
        echo "</label>
                                                <select class=\"form-control country_id\" data-receiver=\"country\" data-live-search=\"true\" name=\"receiver[country]\">
                                                    <option data-hidden=\"true\"></option>
                                                    ";
        // line 1584
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 1585
            echo "                                                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["country"], "id", [], "any", false, false, false, 1585), "html", null, true);
            echo "\" ";
            if ((($context["currentLang"] ?? null) != "en")) {
                echo "data-subtext=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["country"], "lang", [0 => ($context["currentLang"] ?? null)], "method", false, false, false, 1585), "name", [], "any", false, false, false, 1585), "html", null, true);
                echo "\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["country"], "lang", [0 => "en"], "method", false, false, false, 1585), "name", [], "any", false, false, false, 1585), "html", null, true);
            echo "</option>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1587
        echo "                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>";
        // line 1590
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Postal Code"]);
        echo "</label>
                                                <input class=\"form-control postal_code\" type=\"text\" data-receiver=\"postal_code\" name=\"receiver[postal_code]\" >
                                            </div>
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-6\">
                                                <label>";
        // line 1596
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["State / Region"]);
        echo "</label>
                                                <select class=\"form-control state_id\" data-receiver=\"administrative_area_level_1\" title=\"";
        // line 1597
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please select country first"]);
        echo "\" name=\"receiver[state]\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    ";
        // line 1599
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["states"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["state"]) {
            // line 1600
            echo "                                                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["state"], "id", [], "any", false, false, false, 1600), "html", null, true);
            echo "\" ";
            if ((($context["currentLang"] ?? null) != "en")) {
                echo "data-subtext=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["state"], "lang", [0 => ($context["currentLang"] ?? null)], "method", false, false, false, 1600), "name", [], "any", false, false, false, 1600), "html", null, true);
                echo "\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["state"], "lang", [0 => "en"], "method", false, false, false, 1600), "name", [], "any", false, false, false, 1600), "html", null, true);
            echo "</option>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['state'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1602
        echo "                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>";
        // line 1605
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["City"]);
        echo "</label>
                                                <select class=\"form-control city_id\" data-receiver=\"locality\" name=\"receiver[city]\" title=\"";
        // line 1606
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please select state first"]);
        echo "\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    ";
        // line 1608
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
            // line 1609
            echo "                                                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["city"], "id", [], "any", false, false, false, 1609), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["city"], "name", [], "any", false, false, false, 1609), "html", null, true);
            echo "</option>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1611
        echo "                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-12\">
                                                <label>";
        // line 1614
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["County"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                <select class=\"form-control area_id\" data-receiver=\"sublocality\" name=\"receiver[county]\" title=\"";
        // line 1615
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please select city first"]);
        echo "\" data-live-search=\"true\" required>
                                                    <option data-hidden=\"true\"></option>
                                                    ";
        // line 1617
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["areas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["county"]) {
            // line 1618
            echo "                                                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["county"], "id", [], "any", false, false, false, 1618), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["county"], "name", [], "any", false, false, false, 1618), "html", null, true);
            echo "</option>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['county'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1620
        echo "                                                </select>
                                            </div>
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-12\">
                                                <label>";
        // line 1625
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Google Map"]);
        echo "</label>
                                                <div class=\"col-sm-12 map_canvas map_receiver\"></div>
                                                <span class=\"form-text text-muted\">";
        // line 1627
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Change the pin to select the right location"]);
        echo "</span>
                                            </div>
                                        </div>
                                        <div class=\"form-group row\">
                                            <div class=\"col-lg-12\">
                                                <div class=\"kt-checkbox-single\">
                                                    <label class=\"kt-checkbox\">
                                                        <input type=\"checkbox\" name=\"connect\" class=\"connect\" value=\"receiver\"> ";
        // line 1634
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Connect client"]);
        echo ": ";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["create an account for the client"]);
        echo "
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"row align-items-center kt-hidden\" id=\"connect_receiver\">
                                            <div class=\"col-md-12\">
                                                <div class=\"form-group kt-form__group--inline\">
                                                    <div class=\"kt-form__label\">
                                                        <label class=\"col-form-label\">";
        // line 1644
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Email"]);
        echo ":&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                    </div>
                                                    <div class=\"kt-form__control\">
                                                        <input type=\"text\" class=\"form-control email\" name=\"receiver[email]\" required/>
                                                    </div>
                                                </div>
                                                <div class=\"d-md-none kt-margin-b-10\"></div>
                                            </div>
                                            <div class=\"col-md-12\">
                                                <div class=\"form-group kt-form__group--inline\">
                                                    <div class=\"kt-form__label\">
                                                        <label class=\"col-form-label\">";
        // line 1655
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Username"]);
        echo ":&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                    </div>
                                                    <div class=\"kt-form__control\">
                                                        <input type=\"text\" class=\"form-control username\" name=\"receiver[username]\" required>
                                                    </div>
                                                </div>
                                                <div class=\"d-md-none kt-margin-b-10\"></div>
                                            </div>
                                            <div class=\"col-md-12\">
                                                <div class=\"form-group kt-form__group--inline\">
                                                    <div class=\"kt-form__label\">
                                                        <label class=\"col-form-label\">";
        // line 1666
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Password"]);
        echo ":&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                    </div>
                                                    <div class=\"kt-form__control\">
                                                        <input type=\"password\" class=\"form-control password\" name=\"receiver[password]\" required>
                                                    </div>
                                                </div>
                                                <div class=\"d-md-none kt-margin-b-10\"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"kt-portlet__foot\">
                                    <div class=\"row align-items-center\">
                                        <div class=\"col-lg-12\">
                                            <button type=\"button\" class=\"btn btn-success save\">";
        // line 1680
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Save"]);
        echo "</button>
                                            <button type=\"button\" class=\"btn btn-secondary cancel\">";
        // line 1681
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Cancel"]);
        echo "</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"col-lg-12 kt-hidden\" id=\"addnewreceiveraddress\">
                            <div class=\"kt-portlet kt-portlet--bordered kt-portlet--head--noborder kt-margin-b-0\">
                                <div class=\"kt-portlet__head\">
                                    <div class=\"kt-portlet__head-label\">
                                        <h3 class=\"kt-portlet__head-title\">
                                            ";
        // line 1692
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Add a new client address"]);
        echo "
                                        </h3>
                                    </div>
                                </div>
                                <div class=\"kt-portlet__body\">
                                    <div class=\"location-receiveraddress\">
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-12\">
                                                <label>";
        // line 1700
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Address"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                <input type=\"text\" class=\"form-control address street_addr\" data-receiveraddress=\"route\" name=\"receiveraddress[street_address]\"  rel=\"receiveraddress\" required/>
                                                <input type=\"hidden\" class=\"form-control lat\" data-receiveraddress=\"lat\" name=\"receiveraddress[lat]\" />
                                                <input type=\"hidden\" class=\"form-control lng\" data-receiveraddress=\"lng\" name=\"receiveraddress[lng]\" />
                                                <input type=\"hidden\" class=\"form-control url\" data-receiveraddress=\"url\" name=\"receiveraddress[url]\" />
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>";
        // line 1707
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Country"]);
        echo "</label>
                                                <select class=\"form-control country_id\" data-receiveraddress=\"country\" data-live-search=\"true\" name=\"receiveraddress[country]\">
                                                    <option data-hidden=\"true\"></option>
                                                    ";
        // line 1710
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["countries"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 1711
            echo "                                                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["country"], "id", [], "any", false, false, false, 1711), "html", null, true);
            echo "\" ";
            if ((($context["currentLang"] ?? null) != "en")) {
                echo "data-subtext=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["country"], "lang", [0 => ($context["currentLang"] ?? null)], "method", false, false, false, 1711), "name", [], "any", false, false, false, 1711), "html", null, true);
                echo "\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["country"], "lang", [0 => "en"], "method", false, false, false, 1711), "name", [], "any", false, false, false, 1711), "html", null, true);
            echo "</option>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1713
        echo "                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>";
        // line 1716
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Postal Code"]);
        echo "</label>
                                                <input class=\"form-control postal_code\" type=\"text\" data-sendreceiveraddresser=\"postal_code\" name=\"receiveraddress[postal_code]\" >
                                            </div>
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-6\">
                                                <label>";
        // line 1722
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["State / Region"]);
        echo "</label>
                                                <select class=\"form-control state_id\" data-receiveraddress=\"administrative_area_level_1\" title=\"";
        // line 1723
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please select country first"]);
        echo "\" name=\"receiveraddress[state]\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    ";
        // line 1725
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["states"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["state"]) {
            // line 1726
            echo "                                                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["state"], "id", [], "any", false, false, false, 1726), "html", null, true);
            echo "\" ";
            if ((($context["currentLang"] ?? null) != "en")) {
                echo "data-subtext=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["state"], "lang", [0 => ($context["currentLang"] ?? null)], "method", false, false, false, 1726), "name", [], "any", false, false, false, 1726), "html", null, true);
                echo "\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["state"], "lang", [0 => "en"], "method", false, false, false, 1726), "name", [], "any", false, false, false, 1726), "html", null, true);
            echo "</option>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['state'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1728
        echo "                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>";
        // line 1731
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["City"]);
        echo "</label>
                                                <select class=\"form-control city_id\" data-receiveraddress=\"locality\" name=\"receiveraddress[city]\" title=\"";
        // line 1732
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please select state first"]);
        echo "\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    ";
        // line 1734
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
            // line 1735
            echo "                                                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["city"], "id", [], "any", false, false, false, 1735), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["city"], "name", [], "any", false, false, false, 1735), "html", null, true);
            echo "</option>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1737
        echo "                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-12\">
                                                <label>";
        // line 1740
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["County"]);
        echo "</label>
                                                <select class=\"form-control area_id\" data-receiveraddress=\"sublocality\" name=\"receiveraddress[county]\" title=\"";
        // line 1741
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please select city first"]);
        echo "\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    ";
        // line 1743
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["areas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["county"]) {
            // line 1744
            echo "                                                        <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["county"], "id", [], "any", false, false, false, 1744), "html", null, true);
            echo "\" ";
            if ((($context["currentLang"] ?? null) != "en")) {
                echo "data-subtext=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["county"], "lang", [0 => ($context["currentLang"] ?? null)], "method", false, false, false, 1744), "name", [], "any", false, false, false, 1744), "html", null, true);
                echo "\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["county"], "lang", [0 => "en"], "method", false, false, false, 1744), "name", [], "any", false, false, false, 1744), "html", null, true);
            echo "</option>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['county'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1746
        echo "                                                </select>
                                            </div>
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-12\">
                                                <label>";
        // line 1751
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Google Map"]);
        echo "</label>
                                                <div class=\"col-sm-12 map_canvas map_receiveraddress\"></div>
                                                <span class=\"form-text text-muted\">";
        // line 1753
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Change the pin to select the right location"]);
        echo "</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"kt-portlet__foot\">
                                    <div class=\"row align-items-center\">
                                        <div class=\"col-lg-12\">
                                            <button type=\"button\" class=\"btn btn-success save\">";
        // line 1761
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Save"]);
        echo "</button>
                                            <button type=\"button\" class=\"btn btn-secondary cancel\">";
        // line 1762
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Cancel"]);
        echo "</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit\"></div>
                        <div class=\"form-group col-lg-12 ";
        // line 1769
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 1769) != 1)) {
            echo "kt-hidden";
        }
        echo " package_fee\">
                            <label>";
        // line 1770
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Courier Cost"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"input-group\">
                                ";
        // line 1772
        if ((twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "place_symbol_before", [], "any", false, false, false, 1772) == 1)) {
            // line 1773
            echo "                                    <div class=\"input-group-prepend\">
                                        <span class=\"input-group-text\">
                                            ";
            // line 1775
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "currency_symbol", [], "any", false, false, false, 1775), "html", null, true);
            echo "
                                        </span>
                                    </div>
                                ";
        }
        // line 1779
        echo "                                    <input type=\"text\" class=\"form-control decimal\" data-type='currency' id=\"courier_fee\" name=\"courier_fee\" value=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "courier_fee", [], "any", false, false, false, 1779), "html", null, true);
        echo "\" required />
                                ";
        // line 1780
        if ((twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "place_symbol_before", [], "any", false, false, false, 1780) == 0)) {
            // line 1781
            echo "                                    <div class=\"input-group-append\">
                                        <span class=\"input-group-text\">
                                            ";
            // line 1783
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "currency_symbol", [], "any", false, false, false, 1783), "html", null, true);
            echo "
                                        </span>
                                    </div>
                                ";
        }
        // line 1787
        echo "                            </div>
                        </div>
                        <div class=\"form-group col-lg-12\">
                            <label>";
        // line 1790
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Return package cost"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"kt-radio-inline\">
                                <label class=\"kt-radio\">
                                    <input type=\"radio\" name=\"return_defray\" class=\"return_defray\" value=\"1\" ";
        // line 1793
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 1793) == 1)) {
            echo "checked";
        }
        echo " required> ";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Yes"]);
        echo "
                                    <span></span>
                                </label>
                                <label class=\"kt-radio\">
                                    <input type=\"radio\" name=\"return_defray\" class=\"return_defray\" value=\"2\" ";
        // line 1797
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 1797) == 2)) {
            echo "checked";
        }
        echo "  required> ";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["No"]);
        echo "
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class=\"form-group col-lg-12 ";
        // line 1802
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 1802) != 1)) {
            echo "kt-hidden";
        }
        echo " package_fee\">
                            <label>";
        // line 1803
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Package Cost"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"input-group\">
                                ";
        // line 1805
        if ((twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "place_symbol_before", [], "any", false, false, false, 1805) == 1)) {
            // line 1806
            echo "                                    <div class=\"input-group-prepend\">
                                        <span class=\"input-group-text\">
                                            ";
            // line 1808
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "currency_symbol", [], "any", false, false, false, 1808), "html", null, true);
            echo "
                                        </span>
                                    </div>
                                ";
        }
        // line 1812
        echo "                                    <input type=\"text\" class=\"form-control decimal\" data-type='currency' id=\"package_fee\" name=\"package_fee\" value=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "package_fee", [], "any", false, false, false, 1812), "html", null, true);
        echo "\" required />
                                ";
        // line 1813
        if ((twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "place_symbol_before", [], "any", false, false, false, 1813) == 0)) {
            // line 1814
            echo "                                    <div class=\"input-group-append\">
                                        <span class=\"input-group-text\">
                                            ";
            // line 1816
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "currency_symbol", [], "any", false, false, false, 1816), "html", null, true);
            echo "
                                        </span>
                                    </div>
                                ";
        }
        // line 1820
        echo "                            </div>
                        </div>
                        <div class=\"form-group col-lg-12 ";
        // line 1822
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 1822) != 1)) {
            echo "kt-hidden";
        }
        echo " package_fee\">
                            <label>";
        // line 1823
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Return Shipment Cost"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"input-group\">
                                ";
        // line 1825
        if ((twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "place_symbol_before", [], "any", false, false, false, 1825) == 1)) {
            // line 1826
            echo "                                    <div class=\"input-group-prepend\">
                                        <span class=\"input-group-text\">
                                            ";
            // line 1828
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "currency_symbol", [], "any", false, false, false, 1828), "html", null, true);
            echo "
                                        </span>
                                    </div>
                                ";
        }
        // line 1832
        echo "                                    <input type=\"text\" class=\"form-control decimal\" data-type='currency' name=\"return_courier_fee\" id=\"return_courier_fee\" value=\"";
        if (twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "package_fee", [], "any", false, false, false, 1832)) {
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_courier_fee", [], "any", false, false, false, 1832), "html", null, true);
        } else {
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "fees", [], "any", false, false, false, 1832), "delivery_cost_back_receiver", [], "any", false, false, false, 1832), "html", null, true);
        }
        echo "\" required />
                                ";
        // line 1833
        if ((twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "place_symbol_before", [], "any", false, false, false, 1833) == 0)) {
            // line 1834
            echo "                                    <div class=\"input-group-append\">
                                        <span class=\"input-group-text\">
                                            ";
            // line 1836
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "currency_symbol", [], "any", false, false, false, 1836), "html", null, true);
            echo "
                                        </span>
                                    </div>
                                ";
        }
        // line 1840
        echo "                            </div>
                        </div>
                        <div class=\"form-group col-lg-12 ";
        // line 1842
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_defray", [], "any", false, false, false, 1842) != 1)) {
            echo "kt-hidden";
        }
        echo " package_fee\">
                            <label>";
        // line 1843
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Return package fees responsiable"]);
        echo "&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"kt-radio-inline\">
                                <label class=\"kt-radio\">
                                    <input type=\"radio\" name=\"return_package_fee\" class=\"return_package_fee\" value=\"1\" ";
        // line 1846
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_package_fee", [], "any", false, false, false, 1846) != 2)) {
            echo "checked";
        }
        echo " required> ";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Receiver"]);
        echo "
                                    <span></span>
                                </label>
                                <label class=\"kt-radio\">
                                    <input type=\"radio\" name=\"return_package_fee\" class=\"return_package_fee\" value=\"2\" required ";
        // line 1850
        if ((twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "return_package_fee", [], "any", false, false, false, 1850) == 2)) {
            echo "checked";
        }
        echo "> ";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Sender"]);
        echo "
                                    <span></span>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">";
        // line 1859
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Close"]);
        echo "</button>
                    <button type=\"button\" class=\"btn btn-info save\">";
        // line 1860
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Confirm"]);
        echo "</button>
                    <button type=\"submit\" class=\"btn btn-primary kt-hidden\">";
        // line 1861
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Deliver"]);
        echo "</button>
                </div>
            ";
        // line 1863
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        </div>
    </div>
</div>
<!--
";
        // line 1868
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('styles'        );
        // line 1869
        echo "    <style>
        .kt-todo .kt-todo__content {
          width: 100%; }

        .kt-todo .kt-todo__aside {
          padding: 20px;
          width: 275px; }
          @media (min-width: 1025px) {
            .kt-todo .kt-todo__aside {
              margin-right: 25px; } }
          .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item {
            margin-bottom: 0.5rem; }
            .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link-title {
              padding-left: 1.9rem;
              font-weight: 600;
              color: #48465b;
              font-size: 1.1rem;
              padding-left: 1.9rem;
              font-weight: 600;
              color: #48465b;
              font-size: 1.1rem; }
            .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link {
              padding: 0.6rem 20px;
              border-radius: 4px; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link i {
                color: #8e96b8; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease;
                fill: #8e96b8; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link:hover g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-icon {
                text-align: center;
                margin: 0 0.7rem 0 -0.23rem; }
                .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-icon.kt-nav__link-icon--size {
                  font-size: 0.9rem;
                  color: #93a2dd; }
                .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-icon svg {
                  width: 20px;
                  height: 20px; }
                .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-icon g [fill] {
                  fill: #93a2dd; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-text {
                font-weight: 500;
                color: #595d6e; }
            .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item:last-child {
              margin-bottom: 0; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item:last-child .kt-nav__link .kt-nav__link-icon {
                font-size: 0.9rem; }
            .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link {
              background-color: #f7f8fa;
              border-radius: 4px; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link i, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link i, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link i {
                color: #5d78ff; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link g [fill], .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link g [fill], .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease;
                fill: #5d78ff; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link:hover g [fill], .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link:hover g [fill], .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link:hover g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link .kt-nav__link-text, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link .kt-nav__link-text, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link .kt-nav__link-text {
                color: #5d78ff; }

        .kt-todo .kt-todo__header {
          padding: 13px 25px;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -ms-flex-wrap: wrap;
          flex-wrap: wrap;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          width: 100%; }
          .kt-todo .kt-todo__header .kt-todo__title {
            margin: 0;
            font-size: 1.4rem;
            padding-right: 2rem;
            font-weight: 600;
            color: #595d6e; }
          .kt-todo .kt-todo__header .kt-todo__nav {
            padding: 0;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            margin-top: 0.2rem; }
            .kt-todo .kt-todo__header .kt-todo__nav .btn {
              margin-right: 1rem; }
            .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link {
              padding: 0.5rem 1.2rem;
              font-weight: 500;
              color: #74788d;
              border-radius: 4px; }
              .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link:not(:first-child):not(:last-child) {
                margin: 0 0.2rem; }
              .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link:hover, .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link.kt-todo__link--selected, .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link.kt-todo__link--active {
                background-color: #f7f8fa;
                color: #5d78ff;
                border-radius: 4px; }
          .kt-todo .kt-todo__header .kt-todo__users {
            line-height: 0;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center; }
            .kt-todo .kt-todo__header .kt-todo__users .kt-media {
              position: relative;
              z-index: 0; }
              .kt-todo .kt-todo__header .kt-todo__users .kt-media:not(:first-child):not(:last-child) {
                margin-left: -0.1rem; }
          @media (max-width: 1024px) {
            .kt-todo .kt-todo__header {
              padding: 10px 15px; } }

        .kt-todo .kt-todo__icon {
          border: 0;
          background: none;
          outline: none !important;
          -webkit-box-shadow: none;
          box-shadow: none;
          outline: none !important;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          height: 35px;
          width: 35px;
          background-color: #f7f8fa;
          -webkit-transition: all 0.3s ease;
          transition: all 0.3s ease;
          cursor: pointer;
          margin: 0;
          border-radius: 0;
          border-radius: 4px; }
          .kt-todo .kt-todo__icon i {
            font-size: 1.1rem; }
          .kt-todo .kt-todo__icon.kt-todo__icon--sm {
            height: 26px;
            width: 26px; }
            .kt-todo .kt-todo__icon.kt-todo__icon--sm i {
              font-size: 0.8rem; }
          .kt-todo .kt-todo__icon.kt-todo__icon--md {
            height: 30px;
            width: 30px; }
            .kt-todo .kt-todo__icon.kt-todo__icon--md i {
              font-size: 1rem; }
          .kt-todo .kt-todo__icon.kt-todo__icon--light {
            background-color: transparent; }
          .kt-todo .kt-todo__icon i {
            color: #8e96b8; }
          .kt-todo .kt-todo__icon g [fill] {
            -webkit-transition: fill 0.3s ease;
            transition: fill 0.3s ease;
            fill: #8e96b8; }
          .kt-todo .kt-todo__icon:hover g [fill] {
            -webkit-transition: fill 0.3s ease;
            transition: fill 0.3s ease; }
          .kt-todo .kt-todo__icon.kt-todo__icon--active, .kt-todo .kt-todo__icon:hover {
            -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
            background-color: #ebedf2; }
            .kt-todo .kt-todo__icon.kt-todo__icon--active.kt-todo__icon--light, .kt-todo .kt-todo__icon:hover.kt-todo__icon--light {
              background-color: transparent; }
            .kt-todo .kt-todo__icon.kt-todo__icon--active i, .kt-todo .kt-todo__icon:hover i {
              color: #5d78ff; }
            .kt-todo .kt-todo__icon.kt-todo__icon--active g [fill], .kt-todo .kt-todo__icon:hover g [fill] {
              -webkit-transition: fill 0.3s ease;
              transition: fill 0.3s ease;
              fill: #5d78ff; }
            .kt-todo .kt-todo__icon.kt-todo__icon--active:hover g [fill], .kt-todo .kt-todo__icon:hover:hover g [fill] {
              -webkit-transition: fill 0.3s ease;
              transition: fill 0.3s ease; }
          .kt-todo .kt-todo__icon.kt-todo__icon--back {
            background-color: transparent; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back i {
              color: #8e96b8;
              font-size: 1.5rem; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back g [fill] {
              -webkit-transition: fill 0.3s ease;
              transition: fill 0.3s ease;
              fill: #8e96b8; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back:hover g [fill] {
              -webkit-transition: fill 0.3s ease;
              transition: fill 0.3s ease; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back svg {
              height: 32px;
              width: 32px; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back:hover {
              background-color: transparent; }
              .kt-todo .kt-todo__icon.kt-todo__icon--back:hover i {
                color: #5d78ff; }
              .kt-todo .kt-todo__icon.kt-todo__icon--back:hover g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease;
                fill: #5d78ff; }
              .kt-todo .kt-todo__icon.kt-todo__icon--back:hover:hover g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease; }

        .kt-todo .kt-todo__list {
          display: -webkit-box !important;
          display: -ms-flexbox !important;
          display: flex !important;
          padding: 0; }
          .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar {
            position: relative;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding: 0 25px; }
            .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -webkit-box-align: center;
              -ms-flex-align: center;
              align-items: center;
              margin-right: 1rem; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__check {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__check .kt-checkbox {
                  margin: 0;
                  padding-left: 0; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__check .kt-todo__icon {
                  margin-left: 0; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__panel {
                display: none;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .btn {
                padding: 0.6rem 1rem; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions.kt-todo__actions--expanded .kt-todo__panel {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__icon {
                margin-right: 0.5rem; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__icon.kt-todo__icon--back {
                  margin-right: 2.5rem; }
            .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -webkit-box-align: center;
              -ms-flex-align: center;
              align-items: center;
              margin: 0.4rem 0; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls .kt-todo__icon {
                margin-left: 0.5rem; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls .kt-todo__sort {
                margin-left: 0.5rem; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls .kt-todo__pages .kt-todo__perpage {
                color: #74788d;
                font-size: 1rem;
                font-weight: 500;
                margin-right: 1rem;
                cursor: pointer;
                -webkit-transition: all 0.3s ease;
                transition: all 0.3s ease;
                padding: 0.5rem 0; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls .kt-todo__pages .kt-todo__perpage:hover {
                  -webkit-transition: all 0.3s ease;
                  transition: all 0.3s ease;
                  color: #5d78ff; }
            .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__sep {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              height: 1rem;
              width: 1rem; }
          .kt-todo .kt-todo__list .kt-todo__body {
            padding: 20px 0 0 0; }
            .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items {
              padding: 0;
              margin-bottom: 15px; }
              .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: start;
                -ms-flex-align: start;
                align-items: flex-start;
                padding: 12px 25px;
                -webkit-transition: all 0.3s ease;
                transition: all 0.3s ease;
                cursor: pointer; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex; }
                  @media screen\\0 {
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info {
                      padding: 8px 0; } }
                  @media screen\\0 {
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info {
                      min-width: 210px; } }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    margin: 0.3rem 10px 0 0;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center; }
                    @media screen\\0 {
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions {
                        min-width: 70px; } }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-checkbox {
                      margin: 0;
                      padding: 0;
                      margin-right: 10px; }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-todo__icon {
                      height: 22px;
                      width: 22px; }
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-todo__icon i {
                        font-size: 1rem;
                        color: #ebedf2; }
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-todo__icon:hover i {
                        color: rgba(255, 184, 34, 0.5) !important; }
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-todo__icon.kt-todo__icon--on i {
                        color: #ffb822 !important; }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center;
                    margin-right: 30px;
                    width: 175px; }
                    @media screen\\0 {
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender {
                        min-width: 175px; } }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-media {
                      margin-right: 10px;
                      min-width: 30px !important; }
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-media span {
                        min-width: 30px !important; }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-todo__author {
                      font-size: 1rem;
                      color: #595d6e;
                      font-weight: 500; }
                    @media (max-width: 1400px) {
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender {
                        display: block;
                        width: 70px;
                        margin-right: 10px; }
                        .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-todo__author {
                          display: block; }
                        .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-media {
                          margin-bottom: 5px; } }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details {
                  -webkit-box-flex: 1;
                  -ms-flex-positive: 1;
                  flex-grow: 1;
                  margin-top: 5px; }
                  @media screen\\0 {
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details {
                      width: 0;
                      height: 0; } }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details .kt-todo__message .kt-todo__subject {
                    font-size: 1rem;
                    color: #595d6e;
                    font-weight: 500; }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details .kt-todo__message .kt-todo__summary {
                    font-size: 1rem;
                    color: #a2a5b9;
                    font-weight: 400;
                    text-overflow: ellipsis; }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details .kt-todo__labels {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    margin-top: 5px; }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details .kt-todo__labels .kt-todo__label {
                      margin-right: 5px; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__datetime {
                  font-size: 1rem;
                  color: #a2a5b9;
                  font-weight: 400;
                  margin-right: 1rem;
                  margin-top: 5px;
                  width: 85px;
                  text-align: right; }
                  @media (max-width: 1400px) {
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__datetime {
                      width: 70px; } }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item:hover, .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--selected {
                  -webkit-transition: all 0.3s ease;
                  transition: all 0.3s ease;
                  background-color: #f7f8fa; }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item:hover .kt-todo__info .kt-todo__actions .kt-todo__icon i, .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--selected .kt-todo__info .kt-todo__actions .kt-todo__icon i {
                    font-size: 1rem;
                    color: #e2e5ec; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--unread .kt-todo__sender .kt-todo__author {
                  color: #48465b;
                  font-weight: 600; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--unread .kt-todo__details .kt-todo__message .kt-todo__subject {
                  color: #595d6e;
                  font-weight: 600; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--unread .kt-todo__datetime {
                  color: #48465b;
                  font-weight: 600; }
          .kt-todo .kt-todo__list .kt-todo__foot {
            margin-top: 10px;
            padding: 0 25px; }
            .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar {
              float: right; }
              .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                margin-left: 1rem; }
                .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__icon {
                  margin-left: 0.5rem; }
                .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__sort {
                  margin-left: 0.5rem; }
                .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__pages {
                  margin-left: 0.5rem; }
                  .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__pages .kt-todo__perpage {
                    color: #74788d;
                    font-size: 1rem;
                    font-weight: 500;
                    margin-right: 1rem;
                    cursor: pointer;
                    -webkit-transition: all 0.3s ease;
                    transition: all 0.3s ease;
                    padding: 0.5rem 0; }
                    .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__pages .kt-todo__perpage:hover {
                      -webkit-transition: all 0.3s ease;
                      transition: all 0.3s ease;
                      color: #5d78ff; }

        .kt-todo .kt-todo__view {
          padding: 0;
          display: -webkit-box !important;
          display: -ms-flexbox !important;
          display: flex !important; }
          .kt-todo  {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 100%;
            padding-bottom: 25px; }
            @media (max-width: 1024px) {
              .kt-todo  {
                padding-bottom: 15px; } }
            .kt-todo  .kt-todo__head {
              width: 100%; }
              .kt-todo  .kt-todo__head .kt-todo__toolbar {
                cursor: pointer;
                padding-top: 0.9rem;
                width: 100%;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap; }
                .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__icon.kt-todo__icon--back {
                  display: none; }
                .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__info {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  -webkit-box-flex: 1;
                  -ms-flex-positive: 1;
                  flex-grow: 1; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__info .kt-media {
                    margin-right: 0.7rem; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__info .kt-todo__name {
                    color: #48465b;
                    font-weight: 600;
                    font-size: 1.1rem;
                    padding-right: 0.5rem; }
                    .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__info .kt-todo__name:hover {
                      color: #5d78ff;
                      -webkit-transition: all 0.3s ease;
                      transition: all 0.3s ease; }
                .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  -ms-flex-wrap: wrap;
                  flex-wrap: wrap;
                  padding: 0.5rem 0; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details .kt-todo__icon {
                    margin: 0.4rem 0.5rem 0.4rem 0;
                    font-size: 0.7rem;
                    color: #5d78ff; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details .kt-todo__desc {
                    color: #74788d;
                    font-weight: 400;
                    font-size: 1rem; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details .btn {
                    padding: 8px 1rem; }
                    .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details .btn:last-child {
                      margin-left: 0.5rem; }
                .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__actions {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__datetime {
                    color: #a2a5b9;
                    font-weight: 500;
                    font-size: 1rem;
                    margin-right: 1.5rem; }
            .kt-todo  .kt-todo__body {
              padding-bottom: 25px;
              -webkit-box-flex: 1;
              -ms-flex-positive: 1;
              flex-grow: 1; }
              .kt-todo  .kt-todo__body .kt-todo__title {
                padding-top: 1rem; }
                .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__text {
                  color: #48465b;
                  font-size: 1.5rem;
                  font-weight: 600;
                  margin-top: 1rem;
                  display: block; }
                .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  padding: 0.8rem 0 2rem 0; }
                  .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels .kt-todo__label {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center; }
                    .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels .kt-todo__label .kt-todo__dot {
                      padding-right: 0.5rem;
                      font-size: 1.2rem; }
                    .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels .kt-todo__label .kt-todo__text {
                      color: #a2a5b9;
                      font-weight: 500;
                      font-size: 1rem;
                      margin: 0; }
                    .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels .kt-todo__label:last-child {
                      padding-left: 1.5rem; }
                .kt-todo  .kt-todo__body .kt-todo__title:hover {
                  color: #5d78ff;
                  -webkit-transition: all 0.3s ease;
                  transition: all 0.3s ease; }
              .kt-todo  .kt-todo__body .kt-todo__desc {
                padding-bottom: 10px;
                display: block;
                color: #a2a5b9;
                font-weight: 500; }
              .kt-todo  .kt-todo__body .kt-todo__files {
                padding-top: 10px; }
                .kt-todo  .kt-todo__body .kt-todo__files .kt-todo__file {
                  display: block;
                  padding-top: 0.7rem; }
                  .kt-todo  .kt-todo__body .kt-todo__files .kt-todo__file i {
                    padding-right: 0.5rem; }
                  .kt-todo  .kt-todo__body .kt-todo__files .kt-todo__file a {
                    color: #74788d;
                    font-weight: 500; }
                    .kt-todo  .kt-todo__body .kt-todo__files .kt-todo__file a:hover {
                      color: #5d78ff;
                      -webkit-transition: all 0.3s ease;
                      transition: all 0.3s ease; }
              .kt-todo  .kt-todo__body .kt-todo__comments {
                padding-bottom: 20px; }
                .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment {
                  padding-top: 3rem; }
                  .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment:last-child {
                    padding-top: 2rem; }
                  .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center; }
                    .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box .kt-media {
                      margin-right: 1rem; }
                    .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box .kt-todo__username {
                      -webkit-box-flex: 1;
                      -ms-flex-positive: 1;
                      flex-grow: 1;
                      color: #595d6e;
                      font-weight: 500; }
                      .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box .kt-todo__username:hover {
                        color: #5d78ff;
                        -webkit-transition: all 0.3s ease;
                        transition: all 0.3s ease; }
                    .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box .kt-todo__date {
                      color: #a2a5b9;
                      font-weight: 500; }
                  .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__text {
                    padding-left: 3.3rem;
                    display: block;
                    color: #a2a5b9;
                    font-weight: 500; }
            .kt-todo  .kt-todo__foot .kt-todo__form {
              margin-top: 20px;
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-direction: column;
              flex-direction: column;
              border: 1px solid #ebedf2;
              border-radius: 4px; }
              .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__head {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                padding: 20px 15px 20px 25px;
                border-bottom: 1px solid #ebedf2; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__head .kt-todo__title {
                  margin-right: 10px;
                  font-size: 1.2rem;
                  font-weight: 500;
                  color: #595d6e; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__head .kt-todo__actions {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__head .kt-todo__actions .kt-todo__icon {
                    margin-left: 5px; }
              .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body {
                padding: 0 0 10px 0; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-container.ql-snow {
                  border: 0;
                  padding: 0;
                  border-radius: 0; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-container.ql-snow .ql-editor {
                    font-weight: 400;
                    font-size: 1rem;
                    color: #74788d;
                    padding: 15px 25px;
                    font-family: Poppins, Helvetica, sans-serif; }
                    .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-container.ql-snow .ql-editor.ql-blank:before {
                      left: 25px;
                      color: #a2a5b9;
                      font-weight: 400;
                      font-style: normal; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-toolbar.ql-snow {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  height: 50px;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  border-radius: 0;
                  border: 0;
                  border-top: 0;
                  border-bottom: 1px solid #ebedf2;
                  padding-left: 18px; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-toolbar.ql-snow .ql-picker-label, .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-toolbar.ql-snow .ql-picker-label:before {
                    font-weight: 400;
                    font-size: 1rem;
                    color: #74788d;
                    font-family: Poppins, Helvetica, sans-serif; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .kt-todo__attachments {
                  display: inline-block;
                  padding: 0 25px; }
                  @media (max-width: 1024px) {
                    .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .kt-todo__attachments {
                      width: 100%; } }
              .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                width: 100%;
                padding: 20px 15px 20px 25px; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__primary {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-flex: 1;
                  -ms-flex-positive: 1;
                  flex-grow: 1;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__primary .btn-group .btn:nth-child(1) {
                    padding-left: 20px;
                    padding-right: 20px; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__primary .btn-group .btn:nth-child(2) {
                    padding-left: 6px;
                    padding-right: 9px; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__primary .kt-todo__panel {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center;
                    margin-left: 1rem; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__secondary {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  margin: 0; }

        .kt-todo .kt-portlet__head {
          min-height: 80px !important;
          padding: 10px 25px; }

        @media (max-width: 1024px) {
          .kt-todo {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding: 0; }
            .kt-todo .kt-todo__aside {
              background: #fff;
              margin: 0; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link-title {
                padding-left: 1.2rem; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link {
                padding: 0.75rem 10px; }
            .kt-todo .kt-todo__list {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex; }
              .kt-todo .kt-todo__list.kt-todo__list--hidden {
                display: none; }
              .kt-todo .kt-todo__list .kt-portlet__head {
                min-height: 60px !important;
                padding: 10px 15px; }
              .kt-todo .kt-todo__list .kt-todo__head {
                padding: 0; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar {
                  padding: 10px 15px; }
              .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items {
                overflow: auto;
                margin-bottom: 15px; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item {
                  min-width: 500px;
                  padding: 10px 15px; }
              .kt-todo .kt-todo__list .kt-todo__foot {
                padding: 0 15px; }
            .kt-todo .kt-todo__view {
              display: none; }
              .kt-todo .kt-todo__view .kt-todo__head .kt-todo__toolbar .kt-todo__icon.kt-todo__icon--back {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex; }
              .kt-todo .kt-todo__view.kt-todo__view--shown {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex; }
            .kt-todo .kt-portlet__head {
              min-height: 60px !important;
              padding: 10px 15px; } }

        @media (max-width: 768px) {
          .kt-todo .kt-todo__head .kt-todo__nav .btn {
            margin-right: 0.2rem; } }

        .kt-todo__aside-close {
          display: none; }

        @media (max-width: 1024px) {
          .kt-todo__aside {
            z-index: 1001;
            position: fixed;
            -webkit-overflow-scrolling: touch;
            top: 0;
            bottom: 0;
            overflow-y: auto;
            -webkit-transform: translate3d(0, 0, 0);
            backface-visibility: hidden;
            -webkit-backface-visibility: hidden;
            width: 300px !important;
            -webkit-transition: left 0.3s ease, right 0.3s ease;
            transition: left 0.3s ease, right 0.3s ease;
            left: -320px; }
            .kt-todo__aside.kt-todo__aside--on {
              -webkit-transition: left 0.3s ease, right 0.3s ease;
              transition: left 0.3s ease, right 0.3s ease;
              left: 0; } }
          @media screen\\0  and (max-width: 1024px) {
            .kt-todo__aside {
              -webkit-transition: none !important;
              transition: none !important; } }

        @media (max-width: 1024px) {
          .kt-todo__aside--right .kt-todo__aside {
            right: -320px;
            left: auto; }
            .kt-todo__aside--right .kt-todo__aside.kt-todo__aside--on {
              -webkit-transition: left 0.3s ease, right 0.3s ease;
              transition: left 0.3s ease, right 0.3s ease;
              right: 0;
              left: auto; }
          .kt-todo__aside-close {
            width: 25px;
            height: 25px;
            top: 1px;
            z-index: 1002;
            -webkit-transition: left 0.3s ease, right 0.3s ease;
            transition: left 0.3s ease, right 0.3s ease;
            position: fixed;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 3px;
            cursor: pointer;
            outline: none !important;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            left: -25px; } }
          @media screen\\0  and (max-width: 1024px) {
            .kt-todo__aside-close {
              -webkit-transition: none !important;
              transition: none !important; } }

        @media (max-width: 1024px) {
            .kt-todo__aside-close > i {
              line-height: 0;
              font-size: 1.4rem; }
            .kt-todo__aside-close:hover {
              text-decoration: none; }
            .kt-todo__aside--right .kt-todo__aside-close {
              left: auto;
              right: -25px; }
            .kt-todo__aside--on .kt-todo__aside-close {
              -webkit-transition: left 0.3s ease, right 0.3s ease;
              transition: left 0.3s ease, right 0.3s ease;
              left: 274px; }
            .kt-todo__aside--on.kt-todo__aside--right .kt-todo__aside-close {
              left: auto;
              right: 274px; }
          .kt-todo__aside-overlay {
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            overflow: hidden;
            z-index: 1000;
            background: rgba(0, 0, 0, 0.1);
            -webkit-animation: kt-animate-fade-in .3s linear 1;
            animation: kt-animate-fade-in .3s linear 1; }
          .kt-todo__aside-overlay {
            background: rgba(0, 0, 0, 0.05); }
          .kt-todo__aside-close {
            background-color: #f7f8fa; }
            .kt-todo__aside-close > i {
              color: #74788d; }
            .kt-todo__aside-close:hover {
              background-color: transparent; }
              .kt-todo__aside-close:hover > i {
                color: #5d78ff; } }

        @media (max-width: 350px) {
          .kt-todo__aside {
            width: 90% !important; } }
    </style>
";
        // line 1868
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 2801
        echo "-->
";
        // line 2802
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('styles'        );
        // line 2803
        echo "    <style>
        .table-bordered tr td:first-child {
            width: 200px;
            font-weight: bold;
        }
        .map_canvas {
          height: 350px;
        }
        .filter-option-inner br {
            display: none;
        }
        .select2 {
            width: 100% !important;
        }
\t\t.select2-selection {
\t\t\tmin-height: 36px !important;
\t\t}
        .pac-container {
            z-index: 9999;
        }
        @media (max-width: 576px) {
            .kt-widget.kt-widget--user-profile-3 .kt-widget__top .kt-widget__content .kt-widget__info .kt-widget__stats .kt-widget__item {
                width: 100%;
                margin: auto 0 !important;
            }
        }
        .notes_scroll,.notes_scroll .btn {
            cursor: pointer;
        }
    </style>
";
        // line 2802
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 2834
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('scripts'        );
        // line 2835
        echo "<script src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/admin/vendors/custom/geocomplete/jquery.geocomplete.js");
        echo "\" type=\"text/javascript\"></script>
<script src=\"https://maps.googleapis.com/maps/api/js?libraries=places&key=";
        // line 2836
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "google", [], "any", false, false, false, 2836), "map", [], "any", false, false, false, 2836), "html", null, true);
        echo "\"></script>
<script src=\"./admin/vendors/custom/gmaps/gmaps.js\" type=\"text/javascript\"></script>
<script type=\"text/javascript\">
\"use strict\";
";
        // line 2840
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "payment", [], "any", false, false, false, 2840), "enable", [], "any", false, false, false, 2840) == 1)) {
            // line 2841
            echo "    ";
            if (twig_in_filter("paystack", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "payment", [], "any", false, false, false, 2841), "gateways", [], "any", false, false, false, 2841))) {
                // line 2842
                echo "        function payWithPaystack(){
            ";
                // line 2843
                if (($context["paystack_amount"] ?? null)) {
                    // line 2844
                    echo "                var handler = PaystackPop.setup({
                    key: '";
                    // line 2845
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "payment", [], "any", false, false, false, 2845), "paystack_key", [], "any", false, false, false, 2845), "html", null, true);
                    echo "',
                    email: '";
                    // line 2846
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, false, 2846), "html", null, true);
                    echo "',
                    amount: ";
                    // line 2847
                    echo twig_escape_filter($this->env, (($context["paystack_amount"] ?? null) * 100), "html", null, true);
                    echo ",
                    //amount: \"";
                    // line 2848
                    echo twig_escape_filter($this->env, ($context["sender_fees"] ?? null), "html", null, true);
                    echo "\",
                    //currency: \"";
                    // line 2849
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "currency_code", [], "any", false, false, false, 2849), "html", null, true);
                    echo "\",
                    currency: \"GHS\",
                    ref: '";
                    // line 2851
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "U"), "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "number", [], "any", false, false, false, 2851), "html", null, true);
                    echo "_";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "id", [], "any", false, false, false, 2851), "html", null, true);
                    echo "', // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                    metadata: {
                        custom_fields: [
                            {
                                display_name: \"Tracking Number\",
                                variable_name: \"number\",
                                value: \"";
                    // line 2857
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "number", [], "any", false, false, false, 2857), "html", null, true);
                    echo "\"
                            }
                        ]
                    },
                    callback: function(response){

                         \$.request('onCheckpaystack', {
                              data: {reference: response.reference},
                              success: function(response, status, xhr, \$form) {

                                  swal.fire({
                                      title: '";
                    // line 2868
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Thank you!"]);
                    echo "',
                                      text: '";
                    // line 2869
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Your payment is successfully paid! You will be redirect rightnow, please wait"]);
                    echo "',
                                      type: 'success',
                                      buttonsStyling: false,
                                      confirmButtonText: '";
                    // line 2872
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["OK"]);
                    echo "',
                                      confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                                  });
                                  //window.location.reload();
                              }
                         });
                    },
                    onClose: function(){
                        swal.fire({
                            title: '";
                    // line 2881
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Closed!"]);
                    echo "',
                            text: '";
                    // line 2882
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["You did not pay the requested payment! :("]);
                    echo "',
                            type: 'warning',
                            buttonsStyling: false,
                            confirmButtonText: '";
                    // line 2885
                    echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["OK"]);
                    echo "',
                            confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                        });
                    }
                });
                handler.openIframe();
            ";
                }
                // line 2892
                echo "        }
    ";
            }
        }
        // line 2895
        echo "var KTDashboard = function () {

    // Order Statistics.
    // Based on Chartjs plugin - http://www.chartjs.org/
    var orderStatistics = function() {
        var container = KTUtil.getByID('kt_chart_order_statistics');

        if (!container) {
            return;
        }

        var MONTHS = ['1 Jan', '2 Jan', '3 Jan', '4 Jan', '5 Jan', '6 Jan', '7 Jan'];

        var color = Chart.helpers.color;
        var barChartData = {
            labels: ['1 Jan', '2 Jan', '3 Jan', '4 Jan', '5 Jan', '6 Jan', '7 Jan'],
            datasets : [
\t\t\t\t{
                    fill: true,
                    //borderWidth: 0,
                    backgroundColor: color(KTApp.getStateColor('brand')).alpha(0.6).rgbString(),
                    borderColor : color(KTApp.getStateColor('brand')).alpha(0).rgbString(),

                    pointHoverRadius: 4,
                    pointHoverBorderWidth: 12,
                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: KTApp.getStateColor('brand'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),

\t\t\t\t\tdata: [20, 30, 20, 40, 30, 60, 30]
\t\t\t\t},
\t\t\t\t{
                    fill: true,
                    //borderWidth: 0,
\t\t\t\t\tbackgroundColor : color(KTApp.getStateColor('brand')).alpha(0.2).rgbString(),
                    borderColor : color(KTApp.getStateColor('brand')).alpha(0).rgbString(),

                    pointHoverRadius: 4,
                    pointHoverBorderWidth: 12,
                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: KTApp.getStateColor('brand'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),

\t\t\t\t\tdata: [15, 40, 15, 30, 40, 30, 50]
\t\t\t\t}
            ]
        };

        var ctx = container.getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',
            data: barChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: false,
                scales: {
                    xAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        },
                        gridLines: false,
                        ticks: {
                            display: true,
                            beginAtZero: true,
                            fontColor: KTApp.getBaseColor('shape', 3),
                            fontSize: 13,
                            padding: 10
                        }
                    }],
                    yAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        },
                        gridLines: {
                            color: KTApp.getBaseColor('shape', 2),
                            drawBorder: false,
                            offsetGridLines: false,
                            drawTicks: false,
                            borderDash: [3, 4],
                            zeroLineWidth: 1,
                            zeroLineColor: KTApp.getBaseColor('shape', 2),
                            zeroLineBorderDash: [3, 4]
                        },
                        ticks: {
                            max: 70,
                            stepSize: 10,
                            display: true,
                            beginAtZero: true,
                            fontColor: KTApp.getBaseColor('shape', 3),
                            fontSize: 13,
                            padding: 10
                        }
                    }]
                },
                title: {
                    display: false
                },
                hover: {
                    mode: 'index'
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 10,
                    xPadding: 10,
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: KTApp.getStateColor('brand'),
                    titleFontColor: '#ffffff',
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                    }
                }
            }
        });
    };
    var latestTrendsMap = function() {
        if (\$('#kt_map_sender').length == 0) {
            return;
        }
        ";
        // line 3036
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 3036), "lat", [], "any", false, false, false, 3036)) {
            // line 3037
            echo "        try {
            var map = new GMaps({
                div: '#kt_map_sender',
                lat: ";
            // line 3040
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 3040), "lat", [], "any", false, false, false, 3040), "html", null, true);
            echo ",
                lng: ";
            // line 3041
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 3041), "lng", [], "any", false, false, false, 3041), "html", null, true);
            echo "
            });
            map.addMarker({
                lat: ";
            // line 3044
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 3044), "lat", [], "any", false, false, false, 3044), "html", null, true);
            echo ",
                lng: ";
            // line 3045
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 3045), "lng", [], "any", false, false, false, 3045), "html", null, true);
            echo ",
                title: '";
            // line 3046
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 3046), "name", [], "any", false, false, false, 3046), "html", null, true);
            echo "',
                infoWindow: {
                    content: '<p>";
            // line 3048
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Go to"]);
            echo " <a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 3048), "url", [], "any", false, false, false, 3048), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "sender_address", [], "any", false, false, false, 3048), "name", [], "any", false, false, false, 3048), "html", null, true);
            echo "</a></p>'
                }
            });
        } catch (e) {
            console.log(e);
        }

        ";
        }
        // line 3056
        echo "        if (\$('#kt_map_receiver').length == 0) {
            return;
        }
        ";
        // line 3059
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 3059), "lat", [], "any", false, false, false, 3059)) {
            // line 3060
            echo "            try {
                var map = new GMaps({
                    div: '#kt_map_receiver',
                    lat: ";
            // line 3063
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 3063), "lat", [], "any", false, false, false, 3063), "html", null, true);
            echo ",
                    lng: ";
            // line 3064
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 3064), "lng", [], "any", false, false, false, 3064), "html", null, true);
            echo "
                });
                map.addMarker({
                    lat: ";
            // line 3067
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 3067), "lat", [], "any", false, false, false, 3067), "html", null, true);
            echo ",
                    lng: ";
            // line 3068
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 3068), "lng", [], "any", false, false, false, 3068), "html", null, true);
            echo ",
                    title: '";
            // line 3069
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 3069), "name", [], "any", false, false, false, 3069), "html", null, true);
            echo "',
                    infoWindow: {
                        content: '<p>";
            // line 3071
            echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Go to"]);
            echo " <a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 3071), "url", [], "any", false, false, false, 3071), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["order"] ?? null), "receiver_address", [], "any", false, false, false, 3071), "name", [], "any", false, false, false, 3071), "html", null, true);
            echo "</a></p>'
                    }
                });
            } catch (e) {
                console.log(e);
            }
        ";
        }
        // line 3078
        echo "    }
    return {
        // public functions
        init: function () {
            orderStatistics();
            latestTrendsMap();
        },
    };
}();

KTUtil.ready(function () {
    KTDashboard.init();

    \$('.notes_scroll').on('click', function(e){
        \$([document.documentElement, document.body]).animate({
            scrollTop: \$(\"#notes_continer\").offset().top-60
        }, 2000);
    });
    \$('#assign_employee').on('click', '.btn-primary', function(e){
        var parent = \$('#assign_employee');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#assign_employee').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '";
        // line 3113
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
            });
        }
    });
    \$('#assign_manifest').on('click', '.btn-primary', function(e){
        var parent = \$('#assign_manifest');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#assign_manifest').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '";
        // line 3134
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
            });
        }
    });
    \$('#transfer_office').on('click', '.btn-primary', function(e){
        var parent = \$('#assign_employee');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#transfer_office').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '";
        // line 3155
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
            });
        }
    });
    \$('#refuse').on('click', '.btn-primary', function(e){
        var parent = \$('#refuse');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#refuse').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '";
        // line 3176
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
            });
        }
    });
    \$('#discards').on('click', '.btn-primary', function(e){
        var parent = \$('#discards');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#discards').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '";
        // line 3197
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
            });
        }
    });
    \$('#return_discards').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '";
        // line 3207
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
        });
        \$.request('onDiscards', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"";
        // line 3212
        echo url("dashboard/shipments");
        echo "/delivered\";
            }
        });
    });
    \$('#delivered_driver').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '";
        // line 3222
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
        });
        \$.request('onDeliver', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"";
        // line 3227
        echo url("dashboard/shipments");
        echo "/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 3227), "id", [], "any", false, false, false, 3227), "html", null, true);
        echo "/view\";
            }
        });
    });
    \$('#deliveried').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '";
        // line 3237
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
        });
        \$.request('onDelivery', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"";
        // line 3242
        echo url("dashboard/shipments");
        echo "/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 3242), "id", [], "any", false, false, false, 3242), "html", null, true);
        echo "/view\";
            }
        });
    });
    \$('#stock').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '";
        // line 3252
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
        });
        \$.request('onStock', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"";
        // line 3257
        echo url("dashboard/shipments");
        echo "/stock\";
            }
        });
    });
    \$('#received').on('click', '.btn-primary', function(e){
        var parent = \$('#received');
        var validation = 1;
        \$.validator.setDefaults({
            ignore: \":hidden\",
       });
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#received').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '";
        // line 3281
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
            });
        }
    });


    \$('#received').find('input,select').each(function(){
        \$(this).on('change', function(){
            \$('#received .btn-info').removeClass('kt-hidden');
            \$('#received .btn-primary').addClass('kt-hidden');
        });
    });

    \$('#received .btn-info').removeClass('kt-hidden');
    \$('#received .btn-primary').addClass('kt-hidden');


    \$('#received').on('click', '.save', function(e){
        var parent = \$('#received');
        var validation = 1;
        \$.validator.setDefaults({
            ignore: \":hidden\",
       });
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){

            var receiver_address_id = \$('#received #receiver_address_id').val();
            var package_fee = \$('#received #package_fee').val();
            var return_courier_fee = \$('#received #return_courier_fee').val();
            var return_defray = \$('#received .return_defray:checked').val();
            var return_package_fee = \$('#received .return_package_fee:checked').val();

             \$.request('onConfirmfees', {
                  data: {return_courier_fee: return_courier_fee, package_fee: package_fee, receiver_address_id: receiver_address_id, return_defray: return_defray, return_package_fee: return_package_fee},
                  success: function(response, status, xhr, \$form) {

                      swal.fire({
                          buttonsStyling: false,

                          html: \"<strong>";
        // line 3327
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The total cost of courier fees is"]);
        echo ":</strong> \"+response.delivery_cost+\"<br /><strong>";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The total requested from sender is"]);
        echo ":</strong> \"+response.sender_fees+\"<br /><strong>";
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["The total requested from receiver is"]);
        echo ":</strong> \"+response.receiver_fees,
                          type: \"warning\",

                          confirmButtonText: \"";
        // line 3330
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Yes, confirm!"]);
        echo "\",
                          confirmButtonClass: \"btn btn-sm btn-bold btn-success\",

                          showCancelButton: true,
                          cancelButtonText: '";
        // line 3334
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["No, change something"]);
        echo "',
                          cancelButtonClass: \"btn btn-sm btn-bold btn-danger\"
                      }).then(function (result) {
                          if (result.value) {
                              \$('#received .btn-info').addClass('kt-hidden');
                              \$('#received .btn-primary').removeClass('kt-hidden');
                          }
                      });
                  }
             });
        }
    });
    \$('#postpone').on('click', '.btn-primary', function(e){
        var parent = \$('#postpone');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            var css_class = \$(this).attr('class');
            css_class = css_class.replace('form-control ','');
            if(css_class != 'date'){
                if(\$(this).valid() == false){
                    validation = 0;
                }
            }
        });
        if(validation){
            \$('#postpone').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '";
        // line 3367
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
            });
        }
    });
    \$('#accept').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '";
        // line 3377
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
        });

        \$.request('onAccept', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"";
        // line 3383
        echo url("dashboard/shipments");
        echo "/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "param", [], "any", false, false, false, 3383), "id", [], "any", false, false, false, 3383), "html", null, true);
        echo "/view\";
            }
        });
    });

    function formatRepo(repo) {
        if (repo.loading) return repo.text;
        var markup = \"<div class='select2-result-repository clearfix'>\" +
                        \"<div class='select2-result-repository__meta'>\" +
                        \"<div class='select2-result-repository__title'>\" + repo.text + \"</div>\";
                        if (repo.mobile) {
                            markup += \"<div class='select2-result-repository__description'>\" + repo.mobile + \"</div>\";
                        }
                    markup += \"</div></div>\";
        return markup;
    }

    function formatRepoSelection(repo) {
        return repo.text || repo.id;
    }


    \$('body').on('change', '#discard_reason', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        \$('.discard_reason_receiver').addClass('kt-hidden');
        if(selected != '' && selected != 2){
            \$('.discard_reason_receiver').removeClass('kt-hidden');
        }
    });
    \$('body').on('change', '#receiver_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        if(selected == 'new'){
            \$('select.receiver_address_id').html('').val('').selectpicker('refresh');
            \$('#addnewreceiver').removeClass('kt-hidden');
        }else if(selected != ''){
            \$('#addnewreceiver').addClass('kt-hidden');
            \$.request('onClientaddresses', {
                 data: {id: selected},
                 success: function(response, status, xhr, \$form) {
                     \$('select.receiver_address_id').html(response.html).selectpicker('refresh');
                     \$('select.receiver_address_id').val(response.default).selectpicker('refresh');
                     if(response.default == 'new'){
                         \$('#addnewreceiveraddress').removeClass('kt-hidden');
                     }else if(selected != ''){
                         \$('#addnewreceiveraddress').addClass('kt-hidden');
                     }



                     var selected = response.default;
                     var type = \$('.type:checked').val();
                     var sender_address_id = \$('#sender_address_id').val();
                     var packaging_id = \$('#packaging_id').val();
                     var return_defray = \$('.return_defray:checked').val();
                     var return_package_fee = \$('.return_package_fee:checked').val();
                     if(selected != '' && selected != 'new'){
                         \$.request('onChangefees', {
                              data: {sender_address_id: sender_address_id, packaging_id: packaging_id, type: type, receiver_address_id: selected, return_defray: return_defray, return_package_fee: return_package_fee},
                              success: function(response, status, xhr, \$form) {
                                   \$('#delivery_cost').val(response.delivery_cost);
                                   \$('#return_courier_fee').val(response.return_courier_fee);
                              }
                         });
                    }
                 }
            });
        }
    });
    \$('body').on('click', '#addnewreceiver .save', function(e){
        e.preventDefault();
        var parent = \$('#addnewreceiver');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });


        if(validation){
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: '";
        // line 3470
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Processing, please wait"]);
        echo "...'
            });
            \$.request('onNewclient', {
                 data: {name: parent.find('.name').val(), mobile: parent.find('.mobile').val(), email: parent.find('.email').val(), gender: parent.find('.gender:checked').val(), street_addr: parent.find('.street_addr').val(), lat: parent.find('.lat').val(), lng: parent.find('.lng').val(), url: parent.find('.url').val(), area_id: parent.find('.area_id').find(\"option:selected\").val(), city_id: parent.find('.city_id').find(\"option:selected\").val(), postal_code: parent.find('.postal_code').val(), state_id: parent.find('.state_id').find(\"option:selected\").val(), country_id: parent.find('.country_id').find(\"option:selected\").val(), connect: parent.find('.connect:checked').val(), username: parent.find('.username').val(), password: parent.find('.password').val()},
                 error: function(response, status, xhr, \$form) {
                     swal.fire({
                         title: '";
        // line 3476
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Error!"]);
        echo "',
                         text: response.responseText,
                         type: 'error',
                         buttonsStyling: false,
                         confirmButtonText: '";
        // line 3480
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["OK"]);
        echo "',
                         confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                     });
                     KTApp.unblockPage();
                 },
                 success: function(response, status, xhr, \$form) {
                     var newOption = new Option(response.name, response.id, false, true);
                     \$('#receiver_id').append(newOption).trigger('change');
                     \$('select.receiver_address_id').html('<option value=\"'+response.address_id+'\">'+response.address_name+'</option>').selectpicker('refresh');
                     \$('select.receiver_address_id').val(response.address_id).selectpicker('refresh');


                      var selected = response.address_id;
                      var type = \$('.type:checked').val();
                      var sender_address_id = \$('#sender_address_id').val();
                      var packaging_id = \$('#packaging_id').val();
                      var return_defray = \$('.return_defray:checked').val();
                      var return_package_fee = \$('.return_package_fee:checked').val();
                      if(selected != '' && selected != 'new'){
                          \$.request('onChangefees', {
                               data: {sender_address_id: sender_address_id, packaging_id: packaging_id, type: type, receiver_address_id: selected, return_defray: return_defray, return_package_fee: return_package_fee},
                               success: function(response, status, xhr, \$form) {
                                    \$('#delivery_cost').val(response.delivery_cost);
                                    \$('#return_courier_fee').val(response.return_courier_fee);
                               }
                          });
                     }

                     KTApp.unblockPage();
                     parent.find('input,select').each(function(){
                         \$(this).val('');
                         \$(this).selectpicker('refresh');
                     });
                     \$('#addnewreceiver').addClass('kt-hidden');
                 }
            });
        }
    });
    \$('body').on('click', '#addnewreceiver .cancel', function(e){
        e.preventDefault();
        var newOption = new Option('', '', false, true);
        \$('#receiver_id').append(newOption).trigger('change');
        \$('#addnewreceiver').addClass('kt-hidden');
    });

    \$('body').on('changed.bs.select', '.receiver_address_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        if(selected == 'new'){
            \$('#addnewreceiveraddress').removeClass('kt-hidden');
        }else if(selected != ''){
            \$('#addnewreceiveraddress').addClass('kt-hidden');
        }
    });
    \$('body').on('click', '#addnewreceiveraddress .save', function(e){
        e.preventDefault();
        var parent = \$('#addnewreceiveraddress');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });

        if(validation){
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: '";
        // line 3551
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Processing, please wait"]);
        echo "...'
            });
            \$.request('onNewclientaddress', {
                 data: {client_id: \$('#receiver_id').val(), street_addr: parent.find('.street_addr').val(), lat: parent.find('.lat').val(), lng: parent.find('.lng').val(), url: parent.find('.url').val(), area_id: parent.find('.area_id').find(\"option:selected\").val(), city_id: parent.find('.city_id').find(\"option:selected\").val(), postal_code: parent.find('.postal_code').val(), state_id: parent.find('.state_id').find(\"option:selected\").val(), country_id: parent.find('.country_id').find(\"option:selected\").val()},
                 error: function(response, status, xhr, \$form) {
                     swal.fire({
                         title: '";
        // line 3557
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Error!"]);
        echo "',
                         text: response.responseText,
                         type: 'error',
                         buttonsStyling: false,
                         confirmButtonText: '";
        // line 3561
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["OK"]);
        echo "',
                         confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                     });
                     KTApp.unblockPage();
                 },
                 success: function(response, status, xhr, \$form) {
                     \$('select.receiver_address_id').html(response.html).selectpicker('refresh');
                     \$('select.receiver_address_id').val(response.default).selectpicker('refresh');




                       var selected = response.default;
                       var type = \$('.type:checked').val();
                       var sender_address_id = \$('#sender_address_id').val();
                       var packaging_id = \$('#packaging_id').val();
                       var return_defray = \$('.return_defray:checked').val();
                       var return_package_fee = \$('.return_package_fee:checked').val();
                       if(selected != '' && selected != 'new'){
                           \$.request('onChangefees', {
                                data: {sender_address_id: sender_address_id, packaging_id: packaging_id, type: type, receiver_address_id: selected, return_defray: return_defray, return_package_fee: return_package_fee},
                                success: function(response, status, xhr, \$form) {
                                     \$('#delivery_cost').val(response.delivery_cost);
                                     \$('#return_courier_fee').val(response.return_courier_fee);
                                }
                           });
                      }



                     KTApp.unblockPage();
                     parent.find('input,select').each(function(){
                         \$(this).val('');
                         \$(this).selectpicker('refresh');
                     });
                     \$('#addnewreceiveraddress').addClass('kt-hidden');
                 }
            });
        }
    });
    \$('body').on('click', '#addnewreceiveraddress .cancel', function(e){
        e.preventDefault();
        \$('select.receiver_address_id').val('').selectpicker('refresh');
        \$('#addnewreceiveraddress').addClass('kt-hidden');
    });


    \$('body').on('changed.bs.select', '.country_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        var parent = \$(e.currentTarget).parent().parent().parent().parent();
        if(selected != ''){
            \$.request('onListstates', {
                 data: {id: selected},
                 success: function(response, status, xhr, \$form) {
                      parent.find('select.state_id').selectpicker({title: '";
        // line 3615
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Select"]);
        echo "'}).selectpicker('refresh');
                      parent.find('select.state_id').html(response.html).selectpicker('refresh');
                 }
            });
       }
    });
    \$('body').on('changed.bs.select', '.state_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        var parent = \$(e.currentTarget).parent().parent().parent().parent();
        if(selected != ''){
            \$.request('onListcities', {
                 data: {id: selected},
                 success: function(response, status, xhr, \$form) {
                      parent.find('select.city_id').selectpicker({title: '";
        // line 3628
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Select"]);
        echo "'}).selectpicker('refresh');
                      parent.find('select.city_id').html(response.html).selectpicker('refresh');
                 }
            });
       }
    });
    \$('body').on('changed.bs.select', '.city_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        var parent = \$(e.currentTarget).parent().parent().parent().parent();
        if(selected != ''){
            \$.request('onListareas', {
                 data: {id: selected},
                 success: function(response, status, xhr, \$form) {
                      parent.find('select.area_id').selectpicker({title: '";
        // line 3641
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Select"]);
        echo "'}).selectpicker('refresh');
                      parent.find('select.area_id').html(response.html).selectpicker('refresh');
                 }
            });
       }
    });

    \$('body').on('change', '#receiver_address_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
         var type = \$('.type:checked').val();
         var sender_address_id = \$('#sender_address_id').val();
         var packaging_id = \$('#packaging_id').val();
         var return_defray = \$('.return_defray:checked').val();
         var return_package_fee = \$('.return_package_fee:checked').val();
         if(selected != '' && selected != 'new'){
             \$.request('onChangefees', {
                  data: {sender_address_id: sender_address_id, type: type, packaging_id:packaging_id, receiver_address_id: selected, return_defray: return_defray, return_package_fee: return_package_fee},
                  success: function(response, status, xhr, \$form) {
                       \$('#delivery_cost').val(response.delivery_cost);
                       \$('#return_courier_fee').val(response.return_courier_fee);
                  }
             });
        }
    });


    \$(\".clients\").select2({
        ";
        // line 3668
        if ((($context["currentLang"] ?? null) == "ar")) {
            // line 3669
            echo "            dir: \"rtl\",
        ";
        }
        // line 3671
        echo "        language: {
            errorLoading: function () {
                return \"";
        // line 3673
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["There is an error while searching"]);
        echo "...\";
            },
            inputTooLong: function (args) {
                return \"";
        // line 3676
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["You must enter less characters"]);
        echo "...\";
            },
            inputTooShort: function (args) {
                return \"";
        // line 3679
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["You must enter more characters"]);
        echo "...\";
            },
            loadingMore: function () {
                return \"";
        // line 3682
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Loading More"]);
        echo "...\";
            },
            maximumSelected: function (args) {
                return \"";
        // line 3685
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Maximum element has been selected"]);
        echo "...\";
            },
            noResults: function () {
                return \"";
        // line 3688
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["No result found"]);
        echo "...\";
            },
            searching: function () {
                return \"";
        // line 3691
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Searching"]);
        echo "...\";
            }
        },
        placeholder: \"";
        // line 3694
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Search for client"]);
        echo "\",
        allowClear: true,
        ajax: {
            transport: function(params, success, failure) {
                var \$request = \$.request('onGetclients', {
                    data: params.data,
                })
                \$request.done(success)
                \$request.fail(failure)

                return \$request
            },
            dataType: 'json',
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    \$('.bootstrap-touchspin-vertical-btn').TouchSpin({
        buttondown_class: 'btn btn-secondary',
        buttonup_class: 'btn btn-secondary',
        verticalbuttons: true,
        verticalup: '<i class=\"la la-plus\"></i>',
        verticaldown: '<i class=\"la la-minus\"></i>'
    });
    \$('.address').each(function(){
        var address = \$(this);
        address.geocomplete({
            map: \".map_canvas.map_\"+address.attr('rel'),
            mapOptions: {
                zoom: 18
            },
            markerOptions: {
                draggable: true
            },
            details: \".location-\"+\$(this).attr('rel'),
            detailsAttribute: 'data-'+\$(this).attr('rel'),
            autoselect: true,
        });
        address.bind(\"geocode:dragged\", function(event, latLng){
            \$(\"input[data-\"+address.attr('rel')+\"=lat]\").val(latLng.lat());
            \$(\"input[data-\"+address.attr('rel')+\"=lng]\").val(latLng.lng());
        });

    });
    \$('body').on('click', '.connect', function(){
        if(\$(this).is(\":checked\")){
            \$('#connect_'+\$(this).val()).removeClass('kt-hidden');
        }else{
            \$('#connect_'+\$(this).val()).addClass('kt-hidden');
        }
    });
    \$('body').on('click', '.return_defray', function(){
        if(\$(this).val() == 1){
            \$('.package_fee').removeClass('kt-hidden');


             var sender_address_id = \$('#sender_address_id').val();
             var receiver_address_id = \$('#receiver_address_id').val();
             var packaging_id = \$('#packaging_id').val();
             var type = \$('.type:checked').val();
             var return_defray = \$('.return_defray:checked').val();
             var return_package_fee = \$('.return_package_fee:checked').val();
             \$.request('onChangefees', {
                  data: {sender_address_id: sender_address_id, type: type, packaging_id:packaging_id, receiver_address_id: receiver_address_id, return_defray: return_defray, return_package_fee: return_package_fee},
                  success: function(response, status, xhr, \$form) {
                       \$('#delivery_cost').val(response.delivery_cost);
                       \$('#return_courier_fee').val(response.return_courier_fee);
                  }
             });
        }else{
            \$('.package_fee').addClass('kt-hidden');
        }
    });
    \$(\".fees\").inputmask('999";
        // line 3777
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "thousand_separator", [], "any", false, false, false, 3777), "html", null, true);
        echo "999";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "thousand_separator", [], "any", false, false, false, 3777), "html", null, true);
        echo "999";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "decimal_point", [], "any", false, false, false, 3777), "html", null, true);
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range("1", twig_get_attribute($this->env, $this->source, ($context["primary_currency"] ?? null), "initbiz_money_fraction_digits", [], "any", false, false, false, 3777)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo "9";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "', {
        numericInput: true
    });

    \$('#delete').on('click', function(e){
        e.preventDefault();

        swal.fire({
            buttonsStyling: false,

            text: \"";
        // line 3787
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Are you sure to delete"]);
        echo "\",
            type: \"error\",

            confirmButtonText: \"";
        // line 3790
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Yes, delete!"]);
        echo "\",
            confirmButtonClass: \"btn btn-sm btn-bold btn-danger\",

            showCancelButton: true,
            cancelButtonText: '";
        // line 3794
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["No, cancel"]);
        echo "',
            cancelButtonClass: \"btn btn-sm btn-bold btn-brand\"
        }).then(function (result) {
            if (result.value) {
                KTApp.blockPage({
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'success',
                    message: '";
        // line 3802
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Please wait"]);
        echo "...'
                });
                \$.request('onDelete', {
                    success: function(data) {
                        KTApp.unblockPage();
                        swal.fire({
                            title: '";
        // line 3808
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Deleted!"]);
        echo "',
                            text: '";
        // line 3809
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Your selected records have been deleted! :("]);
        echo "',
                            type: 'success',
                            buttonsStyling: false,
                            confirmButtonText: '";
        // line 3812
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["OK"]);
        echo "',
                            confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                        });
                        window.location.href = \"";
        // line 3815
        echo url("dashboard/shipments");
        echo "\";
                    }
                });
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            } else if (result.dismiss === 'cancel') {
                swal.fire({
                    title: '";
        // line 3822
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["Cancelled"]);
        echo "',
                    text: '";
        // line 3823
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["You selected records have not been deleted! :)"]);
        echo "',
                    type: 'error',
                    buttonsStyling: false,
                    confirmButtonText: '";
        // line 3826
        echo call_user_func_array($this->env->getFilter('__')->getCallable(), ["OK"]);
        echo "',
                    confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                });
            }
        });
    });
    if(\$('.kt-widget__action ul.kt-nav li').length == 0){
        \$('.kt-widget__action').hide();
    }
});
</script>
";
        // line 2834
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
    }

    public function getTemplateName()
    {
        return "C:\\laragon\\www\\cargo/themes/spotlayer/pages/dashboard/shipment.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  6336 => 2834,  6322 => 3826,  6316 => 3823,  6312 => 3822,  6302 => 3815,  6296 => 3812,  6290 => 3809,  6286 => 3808,  6277 => 3802,  6266 => 3794,  6259 => 3790,  6253 => 3787,  6228 => 3777,  6142 => 3694,  6136 => 3691,  6130 => 3688,  6124 => 3685,  6118 => 3682,  6112 => 3679,  6106 => 3676,  6100 => 3673,  6096 => 3671,  6092 => 3669,  6090 => 3668,  6060 => 3641,  6044 => 3628,  6028 => 3615,  5971 => 3561,  5964 => 3557,  5955 => 3551,  5881 => 3480,  5874 => 3476,  5865 => 3470,  5773 => 3383,  5764 => 3377,  5751 => 3367,  5715 => 3334,  5708 => 3330,  5698 => 3327,  5649 => 3281,  5622 => 3257,  5614 => 3252,  5599 => 3242,  5591 => 3237,  5576 => 3227,  5568 => 3222,  5555 => 3212,  5547 => 3207,  5534 => 3197,  5510 => 3176,  5486 => 3155,  5462 => 3134,  5438 => 3113,  5401 => 3078,  5387 => 3071,  5382 => 3069,  5378 => 3068,  5374 => 3067,  5368 => 3064,  5364 => 3063,  5359 => 3060,  5357 => 3059,  5352 => 3056,  5337 => 3048,  5332 => 3046,  5328 => 3045,  5324 => 3044,  5318 => 3041,  5314 => 3040,  5309 => 3037,  5307 => 3036,  5164 => 2895,  5159 => 2892,  5149 => 2885,  5143 => 2882,  5139 => 2881,  5127 => 2872,  5121 => 2869,  5117 => 2868,  5103 => 2857,  5090 => 2851,  5085 => 2849,  5081 => 2848,  5077 => 2847,  5073 => 2846,  5069 => 2845,  5066 => 2844,  5064 => 2843,  5061 => 2842,  5058 => 2841,  5056 => 2840,  5049 => 2836,  5044 => 2835,  5042 => 2834,  5040 => 2802,  5008 => 2803,  5006 => 2802,  5003 => 2801,  5001 => 1868,  4068 => 1869,  4066 => 1868,  4058 => 1863,  4053 => 1861,  4049 => 1860,  4045 => 1859,  4029 => 1850,  4018 => 1846,  4012 => 1843,  4006 => 1842,  4002 => 1840,  3995 => 1836,  3991 => 1834,  3989 => 1833,  3980 => 1832,  3973 => 1828,  3969 => 1826,  3967 => 1825,  3962 => 1823,  3956 => 1822,  3952 => 1820,  3945 => 1816,  3941 => 1814,  3939 => 1813,  3934 => 1812,  3927 => 1808,  3923 => 1806,  3921 => 1805,  3916 => 1803,  3910 => 1802,  3898 => 1797,  3887 => 1793,  3881 => 1790,  3876 => 1787,  3869 => 1783,  3865 => 1781,  3863 => 1780,  3858 => 1779,  3851 => 1775,  3847 => 1773,  3845 => 1772,  3840 => 1770,  3834 => 1769,  3824 => 1762,  3820 => 1761,  3809 => 1753,  3804 => 1751,  3797 => 1746,  3780 => 1744,  3776 => 1743,  3771 => 1741,  3767 => 1740,  3762 => 1737,  3751 => 1735,  3747 => 1734,  3742 => 1732,  3738 => 1731,  3733 => 1728,  3716 => 1726,  3712 => 1725,  3707 => 1723,  3703 => 1722,  3694 => 1716,  3689 => 1713,  3672 => 1711,  3668 => 1710,  3662 => 1707,  3652 => 1700,  3641 => 1692,  3627 => 1681,  3623 => 1680,  3606 => 1666,  3592 => 1655,  3578 => 1644,  3563 => 1634,  3553 => 1627,  3548 => 1625,  3541 => 1620,  3530 => 1618,  3526 => 1617,  3521 => 1615,  3517 => 1614,  3512 => 1611,  3501 => 1609,  3497 => 1608,  3492 => 1606,  3488 => 1605,  3483 => 1602,  3466 => 1600,  3462 => 1599,  3457 => 1597,  3453 => 1596,  3444 => 1590,  3439 => 1587,  3422 => 1585,  3418 => 1584,  3412 => 1581,  3402 => 1574,  3390 => 1565,  3383 => 1561,  3377 => 1558,  3370 => 1554,  3363 => 1550,  3353 => 1543,  3344 => 1536,  3336 => 1534,  3334 => 1533,  3329 => 1531,  3323 => 1530,  3318 => 1527,  3312 => 1525,  3309 => 1524,  3301 => 1522,  3299 => 1521,  3291 => 1518,  3283 => 1513,  3275 => 1508,  3264 => 1500,  3259 => 1498,  3255 => 1497,  3249 => 1493,  3235 => 1487,  3223 => 1482,  3218 => 1481,  3214 => 1480,  3199 => 1472,  3188 => 1468,  3181 => 1464,  3168 => 1454,  3155 => 1444,  3151 => 1443,  3147 => 1442,  3141 => 1439,  3134 => 1435,  3126 => 1430,  3115 => 1422,  3110 => 1420,  3106 => 1419,  3097 => 1413,  3090 => 1409,  3082 => 1404,  3071 => 1396,  3066 => 1394,  3062 => 1393,  3053 => 1387,  3039 => 1376,  3034 => 1374,  3027 => 1370,  3020 => 1366,  3009 => 1358,  3004 => 1356,  3000 => 1355,  2993 => 1350,  2982 => 1348,  2978 => 1347,  2972 => 1344,  2965 => 1340,  2958 => 1336,  2947 => 1328,  2942 => 1326,  2938 => 1325,  2931 => 1320,  2918 => 1318,  2914 => 1317,  2908 => 1314,  2901 => 1310,  2894 => 1306,  2883 => 1298,  2878 => 1296,  2874 => 1295,  2867 => 1290,  2856 => 1288,  2852 => 1287,  2846 => 1284,  2842 => 1282,  2830 => 1273,  2825 => 1270,  2823 => 1269,  2818 => 1267,  2811 => 1263,  2799 => 1253,  2784 => 1241,  2779 => 1238,  2777 => 1237,  2774 => 1236,  2762 => 1230,  2756 => 1226,  2748 => 1222,  2741 => 1218,  2735 => 1216,  2727 => 1212,  2725 => 1211,  2721 => 1210,  2712 => 1204,  2706 => 1201,  2692 => 1189,  2687 => 1188,  2678 => 1182,  2671 => 1178,  2657 => 1166,  2652 => 1165,  2645 => 1161,  2639 => 1158,  2635 => 1157,  2628 => 1153,  2615 => 1143,  2607 => 1138,  2600 => 1134,  2588 => 1124,  2576 => 1118,  2561 => 1105,  2551 => 1100,  2541 => 1097,  2537 => 1095,  2531 => 1093,  2528 => 1092,  2520 => 1090,  2517 => 1089,  2515 => 1088,  2507 => 1082,  2502 => 1081,  2494 => 1076,  2485 => 1069,  2482 => 1068,  2473 => 1061,  2461 => 1051,  2458 => 1050,  2454 => 1048,  2447 => 1044,  2442 => 1042,  2439 => 1041,  2436 => 1040,  2429 => 1036,  2424 => 1034,  2421 => 1033,  2419 => 1032,  2416 => 1031,  2413 => 1030,  2409 => 1028,  2404 => 1025,  2398 => 1023,  2392 => 1021,  2390 => 1020,  2385 => 1018,  2382 => 1017,  2379 => 1016,  2374 => 1013,  2368 => 1011,  2362 => 1009,  2360 => 1008,  2355 => 1006,  2352 => 1005,  2350 => 1004,  2347 => 1003,  2345 => 1002,  2340 => 999,  2334 => 997,  2328 => 995,  2326 => 994,  2321 => 992,  2314 => 987,  2308 => 985,  2302 => 983,  2300 => 982,  2295 => 980,  2291 => 978,  2287 => 976,  2281 => 973,  2277 => 972,  2274 => 971,  2271 => 970,  2265 => 967,  2261 => 966,  2258 => 965,  2256 => 964,  2253 => 963,  2251 => 962,  2247 => 960,  2241 => 958,  2235 => 956,  2233 => 955,  2229 => 954,  2224 => 951,  2220 => 949,  2217 => 948,  2211 => 946,  2205 => 944,  2203 => 943,  2198 => 941,  2186 => 932,  2178 => 926,  2175 => 925,  2173 => 924,  2165 => 918,  2153 => 908,  2150 => 907,  2146 => 905,  2139 => 901,  2134 => 899,  2131 => 898,  2128 => 897,  2121 => 893,  2116 => 891,  2113 => 890,  2111 => 889,  2108 => 888,  2105 => 887,  2101 => 885,  2096 => 882,  2090 => 880,  2084 => 878,  2082 => 877,  2077 => 875,  2074 => 874,  2071 => 873,  2066 => 870,  2060 => 868,  2054 => 866,  2052 => 865,  2047 => 863,  2044 => 862,  2042 => 861,  2039 => 860,  2037 => 859,  2032 => 856,  2026 => 854,  2020 => 852,  2018 => 851,  2013 => 849,  2006 => 844,  2000 => 842,  1994 => 840,  1992 => 839,  1987 => 837,  1983 => 835,  1979 => 833,  1973 => 830,  1969 => 829,  1966 => 828,  1963 => 827,  1957 => 824,  1953 => 823,  1950 => 822,  1948 => 821,  1945 => 820,  1943 => 819,  1939 => 817,  1933 => 815,  1927 => 813,  1925 => 812,  1921 => 811,  1916 => 808,  1912 => 806,  1909 => 805,  1903 => 803,  1897 => 801,  1895 => 800,  1890 => 798,  1878 => 789,  1865 => 783,  1855 => 775,  1838 => 770,  1832 => 769,  1824 => 767,  1822 => 766,  1817 => 764,  1811 => 761,  1807 => 760,  1803 => 758,  1797 => 754,  1791 => 752,  1789 => 751,  1785 => 749,  1779 => 747,  1777 => 746,  1763 => 745,  1760 => 744,  1754 => 742,  1748 => 740,  1746 => 739,  1738 => 733,  1732 => 732,  1730 => 731,  1726 => 729,  1720 => 727,  1718 => 726,  1704 => 725,  1701 => 724,  1695 => 722,  1689 => 720,  1687 => 719,  1683 => 717,  1680 => 716,  1676 => 714,  1673 => 713,  1668 => 710,  1662 => 708,  1660 => 707,  1655 => 706,  1653 => 705,  1649 => 704,  1644 => 702,  1641 => 701,  1638 => 700,  1622 => 697,  1618 => 696,  1613 => 693,  1607 => 691,  1605 => 690,  1600 => 689,  1598 => 688,  1594 => 687,  1589 => 685,  1586 => 684,  1583 => 683,  1566 => 679,  1561 => 677,  1558 => 676,  1541 => 672,  1536 => 670,  1531 => 667,  1525 => 665,  1523 => 664,  1519 => 663,  1514 => 661,  1511 => 660,  1509 => 659,  1506 => 658,  1503 => 657,  1499 => 655,  1493 => 652,  1489 => 651,  1486 => 650,  1483 => 649,  1477 => 646,  1473 => 645,  1470 => 644,  1467 => 643,  1461 => 640,  1457 => 639,  1454 => 638,  1452 => 637,  1449 => 636,  1446 => 635,  1442 => 633,  1439 => 632,  1436 => 631,  1424 => 622,  1419 => 619,  1407 => 614,  1399 => 613,  1393 => 610,  1387 => 606,  1384 => 605,  1381 => 604,  1379 => 603,  1376 => 602,  1372 => 600,  1370 => 599,  1367 => 598,  1364 => 597,  1356 => 592,  1351 => 590,  1344 => 586,  1339 => 584,  1336 => 583,  1333 => 582,  1330 => 581,  1328 => 580,  1324 => 578,  1320 => 576,  1314 => 573,  1310 => 572,  1307 => 571,  1304 => 570,  1298 => 567,  1294 => 566,  1291 => 565,  1289 => 564,  1286 => 563,  1283 => 562,  1279 => 560,  1273 => 557,  1269 => 556,  1266 => 555,  1263 => 554,  1257 => 551,  1253 => 550,  1250 => 549,  1248 => 548,  1245 => 547,  1242 => 546,  1238 => 544,  1232 => 541,  1228 => 540,  1225 => 539,  1222 => 538,  1216 => 535,  1212 => 534,  1209 => 533,  1207 => 532,  1204 => 531,  1201 => 530,  1199 => 529,  1196 => 528,  1190 => 524,  1185 => 521,  1174 => 518,  1171 => 517,  1153 => 515,  1151 => 514,  1144 => 513,  1138 => 511,  1136 => 510,  1131 => 508,  1128 => 507,  1124 => 506,  1117 => 502,  1113 => 501,  1109 => 500,  1104 => 497,  1099 => 494,  1088 => 491,  1081 => 489,  1074 => 488,  1068 => 486,  1066 => 485,  1061 => 483,  1058 => 482,  1054 => 481,  1047 => 477,  1043 => 476,  1039 => 475,  1034 => 472,  1032 => 471,  1027 => 468,  1025 => 467,  1015 => 460,  1001 => 448,  993 => 443,  988 => 441,  979 => 435,  974 => 433,  966 => 428,  962 => 427,  956 => 424,  952 => 423,  948 => 421,  941 => 417,  937 => 416,  933 => 414,  930 => 413,  928 => 412,  925 => 411,  921 => 409,  915 => 406,  911 => 405,  908 => 404,  905 => 403,  899 => 400,  895 => 399,  892 => 398,  890 => 397,  887 => 396,  884 => 395,  880 => 393,  876 => 391,  870 => 389,  868 => 388,  864 => 387,  860 => 386,  857 => 385,  854 => 384,  850 => 382,  844 => 380,  842 => 379,  838 => 378,  834 => 377,  831 => 376,  829 => 375,  826 => 374,  823 => 373,  816 => 369,  812 => 368,  806 => 365,  802 => 364,  794 => 359,  790 => 358,  780 => 355,  776 => 354,  772 => 352,  770 => 351,  764 => 348,  760 => 347,  754 => 344,  750 => 343,  742 => 338,  738 => 337,  732 => 334,  728 => 333,  724 => 331,  722 => 330,  716 => 327,  712 => 326,  707 => 323,  684 => 322,  678 => 321,  673 => 319,  668 => 316,  662 => 313,  658 => 312,  655 => 311,  653 => 310,  649 => 308,  643 => 306,  637 => 304,  635 => 303,  630 => 301,  618 => 292,  594 => 271,  586 => 268,  580 => 265,  576 => 263,  573 => 262,  568 => 259,  564 => 257,  558 => 255,  552 => 253,  550 => 252,  547 => 251,  544 => 250,  535 => 249,  533 => 248,  527 => 245,  523 => 243,  514 => 239,  508 => 236,  504 => 234,  501 => 233,  499 => 232,  491 => 229,  485 => 226,  481 => 224,  479 => 223,  475 => 221,  469 => 219,  463 => 217,  461 => 216,  455 => 213,  447 => 208,  441 => 205,  430 => 197,  421 => 190,  418 => 189,  411 => 185,  406 => 183,  403 => 182,  400 => 181,  393 => 177,  388 => 175,  385 => 174,  382 => 173,  375 => 169,  370 => 166,  367 => 165,  360 => 161,  355 => 158,  352 => 157,  350 => 156,  346 => 154,  343 => 153,  340 => 152,  337 => 151,  330 => 147,  325 => 145,  322 => 144,  319 => 143,  312 => 139,  307 => 136,  304 => 135,  297 => 131,  292 => 128,  289 => 127,  282 => 123,  277 => 120,  274 => 119,  271 => 118,  264 => 114,  259 => 111,  256 => 110,  252 => 108,  246 => 105,  242 => 103,  236 => 100,  232 => 98,  230 => 97,  227 => 96,  224 => 95,  221 => 94,  218 => 93,  215 => 92,  208 => 88,  203 => 85,  200 => 84,  193 => 80,  188 => 77,  185 => 76,  182 => 75,  179 => 74,  176 => 73,  169 => 69,  164 => 66,  157 => 62,  152 => 59,  149 => 58,  146 => 57,  143 => 56,  141 => 55,  135 => 52,  126 => 46,  121 => 43,  118 => 42,  116 => 41,  112 => 39,  109 => 38,  106 => 37,  99 => 33,  94 => 30,  91 => 29,  84 => 25,  79 => 23,  76 => 22,  73 => 21,  70 => 20,  67 => 19,  64 => 18,  62 => 17,  54 => 12,  46 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"kt-portlet \">
    <div class=\"kt-portlet__body\">
        <div class=\"kt-widget kt-widget--user-profile-3\">
            <div class=\"kt-widget__top\">
                <div class=\"kt-widget__content\">
                    <div class=\"kt-widget__head\">
                        <a href=\"javascript:;\" class=\"kt-widget__username\">
                            {{settings.tracking.prefix_order}}{{order.number}}
                        </a>
                        <div class=\"kt-widget__action\">
                \t\t\t<a href=\"javascript:;\" class=\"btn btn-label-brand btn-bold btn-sm dropdown-toggle\" data-toggle=\"dropdown\">
                \t\t\t\t{{'Order Actions'|__}}
                \t\t\t</a>
                \t\t\t<div class=\"dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md\">
                                <!--begin::Nav-->
                                <ul class=\"kt-nav\">
                                    {% if order.requested != 4 and order.requested != 8 %}
                                        {% if order.requested == 0 or order.requested == 1 or data.requested == 100 %}
                                            {% if order.requested == 1 and user.role_id == 5 %}
                                            {% else %}
                                                {% if user.hasUserPermission('order', 'u') %}
                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"{{'dashboard/order-edit'|page({ id: order.id })}}\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-edit-1\"></i>
                                                            <span class=\"kt-nav__link-text\">{{'Edit'|__}}</span>
                                                        </a>
                                                    </li>
                                                {% endif %}
                                                {% if user.hasUserPermission('order', 'd') %}
                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"javascript:;\" id=\"delete\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-cancel\"></i>
                                                            <span class=\"kt-nav__link-text\">{{'Delete'|__}}</span>
                                                        </a>
                                                    </li>
                                                {% endif %}
                                            {% endif %}
                                        {% endif %}


                                        {% if user.role_id != 5 %}
                                            {% if order.requested == 0 %}
                                                <li class=\"kt-nav__item\">
                                                    <a href=\"javascript:;\" id=\"accept\" class=\"kt-nav__link\">
                                                        <i class=\"kt-nav__link-icon flaticon2-check-mark\"></i>
                                                        <span class=\"kt-nav__link-text\">{{'Accept'|__}}</span>
                                                    </a>
                                                </li>
                                                <li class=\"kt-nav__item\">
                                                    <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#refuse\" class=\"kt-nav__link\">
                                                        <i class=\"kt-nav__link-icon flaticon-signs-2\"></i>
                                                        <span class=\"kt-nav__link-text\">{{'Refuse'|__}}</span>
                                                    </a>
                                                </li>
                                            {% elseif order.requested != 2 %}
                                                {% if order.requested == 1 or order.requested == 5 or order.requested == 7 or order.requested == 10 %}
                                                    {% if order.manifest_id is empty %}
                                                        {% if order.assigned_id %}
                                                            <li class=\"kt-nav__item\">
                                                                <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#assign_employee\" class=\"kt-nav__link\">
                                                                    <i class=\"kt-nav__link-icon flaticon-users\"></i>
                                                                    <span class=\"kt-nav__link-text\">{{'Change Assigned Employee'|__}}</span>
                                                                </a>
                                                            </li>
                                                        {% else %}
                                                            <li class=\"kt-nav__item\">
                                                                <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#assign_employee\" class=\"kt-nav__link\">
                                                                    <i class=\"kt-nav__link-icon flaticon-users\"></i>
                                                                    <span class=\"kt-nav__link-text\">{{'Assign Employee'|__}}</span>
                                                                </a>
                                                            </li>
                                                        {% endif %}
                                                    {% endif %}
                                                    {% if order.assigned_id is empty %}
                                                        {% if user.hasUserPermission('manifest', 'c') %}
                                                            {% if order.manifest_id is empty %}
                                                                <li class=\"kt-nav__item\">
                                                                    <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#assign_manifest\" class=\"kt-nav__link\">
                                                                        <i class=\"kt-nav__link-icon flaticon-truck\"></i>
                                                                        <span class=\"kt-nav__link-text\">{{'Assign Manifest'|__}}</span>
                                                                    </a>
                                                                </li>
                                                            {% else %}
                                                                {% if order.manifest.status == 1 %}
                                                                    <li class=\"kt-nav__item\">
                                                                        <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#assign_manifest\" class=\"kt-nav__link\">
                                                                            <i class=\"kt-nav__link-icon flaticon-truck\"></i>
                                                                            <span class=\"kt-nav__link-text\">{{'Change Manifest'|__}}</span>
                                                                        </a>
                                                                    </li>
                                                                {% endif %}
                                                            {% endif %}
                                                        {% endif %}
                                                    {% endif %}
                                                    {% if order.assigned_id is not empty or order.manifest_id is not empty %}
                                                        <li class=\"kt-nav__item\">
                                                            {% if order.type == 1 %}
                                                                <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#received\" class=\"kt-nav__link\">
                                                                    <i class=\"kt-nav__link-icon flaticon2-check-mark\"></i>
                                                                    <span class=\"kt-nav__link-text\">{{'Received'|__}}</span>
                                                                </a>
                                                            {% else %}
                                                                <a href=\"javascript:;\" id=\"delivered_driver\" class=\"kt-nav__link\">
                                                                    <i class=\"kt-nav__link-icon flaticon2-check-mark\"></i>
                                                                    <span class=\"kt-nav__link-text\">{{'Delivered to driver'|__}}</span>
                                                                </a>
                                                            {% endif %}
                                                        </li>
                                                    {% endif %}
                                                    {% if order.requested != 10 %}
                                                        <li class=\"kt-nav__item\">
                                                            <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#transfer_office\" class=\"kt-nav__link\">
                                                                <i class=\"kt-nav__link-icon flaticon-logout\"></i>
                                                                <span class=\"kt-nav__link-text\">{{'Transfer To Branch'|__}}</span>
                                                            </a>
                                                        </li>
                                                    {% endif %}
                                                {% endif %}
                                                {% if order.requested != 7 and order.requested != 10 and order.requested != 11  and order.requested != 12 %}
                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"javascript:;\" id=\"stock\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-user-ok\"></i>
                                                            <span class=\"kt-nav__link-text\">{{'Save in stock'|__}}</span>
                                                        </a>
                                                    </li>
                                                {% endif %}
                                                {% if order.requested != 7 and order.requested != 9 and order.requested != 10 and order.requested != 11 and order.requested != 12 %}
                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#postpone\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-refresh\"></i>
                                                            <span class=\"kt-nav__link-text\">{{'Postpone'|__}}</span>
                                                        </a>
                                                    </li>
                                                {% endif %}
                                                {% if order.requested == 3 or order.requested == 9 %}
                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#discards\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-refresh\"></i>
                                                            <span class=\"kt-nav__link-text\">{{'Transfer as discards'|__}}</span>
                                                        </a>
                                                    </li>
                                                {% endif %}
                                                {% if order.requested == 3 or order.requested == 5 or order.requested == 6 or order.requested == 7 or order.requested == 9 %}
                                                    <li class=\"kt-nav__item\">
                                                        <a href=\"{{'dashboard/order-deliver'|page({id:this.param.id})}}\" id=\"deliveried_bak\" class=\"kt-nav__link\">
                                                            <i class=\"kt-nav__link-icon flaticon-user-ok\"></i>
                                                            <span class=\"kt-nav__link-text\">{{'Delieveried'|__}}</span>
                                                        </a>
                                                    </li>
                                                {% endif %}
                                            {% endif %}
                                        {% endif %}
                                    {% endif %}


                                    {% if user.role_id != 5 %}
                                        {% if order.return_defray == 1 and order.requested == 4 %}
                                            <li class=\"kt-nav__item\">
                                                <a href=\"javascript:;\" id=\"return_discards\" class=\"kt-nav__link\">
                                                    <i class=\"kt-nav__link-icon flaticon-refresh\"></i>
                                                    <span class=\"kt-nav__link-text\">{{'Return discards'|__}}</span>
                                                </a>
                                            </li>
                                        {% endif %}
                                        {% if order.requested == 11 %}
                                            <li class=\"kt-nav__item\">
                                                <a href=\"javascript:;\" id=\"return_discards\" class=\"kt-nav__link\">
                                                    <i class=\"kt-nav__link-icon flaticon2-check-mark\"></i>
                                                    <span class=\"kt-nav__link-text\">{{'Deliver returned discards'|__}}</span>
                                                </a>
                                            </li>
                                        {% endif %}
                                        {% if order.type == 2 and order.requested != 0 %}
                                            <li class=\"kt-nav__item\">
                                                <a href=\"{{'dashboard/order-print'|page({ id: order.id, type: 'shipment' })}}\" target=\"_blank\" class=\"kt-nav__link\">
                                                    <i class=\"kt-nav__link-icon flaticon2-print\"></i>
                                                    <span class=\"kt-nav__link-text\">{{'Print Shipment'|__}}</span>
                                                </a>
                                            </li>
                                        {% endif %}
                                        {% if order.requested != 0 and order.requested != 10  and order.requested != 11 and order.requested != 12 %}
                                            <li class=\"kt-nav__item\">
                                                <a href=\"{{'dashboard/order-print'|page({ id: order.id, type: 'label' })}}\" target=\"_blank\" class=\"kt-nav__link\">
                                                    <i class=\"kt-nav__link-icon flaticon2-print\"></i>
                                                    <span class=\"kt-nav__link-text\">{{'Print Label'|__}}</span>
                                                </a>
                                            </li>
                                        {% endif %}
                                    {% endif %}
                                </ul>
                                <!--end::Nav-->
                            </div>
                        </div>
                    </div>

                    <div class=\"kt-widget__subhead\">
                        {{ barcodeHTML({data: order.barcode, type: 'CODABAR'}) }}
                    </div>

                    <div class=\"kt-widget__info\">

                        <div class=\"kt-widget__stats d-flex align-items-center flex-fill\">
                            <div class=\"kt-widget__item\">
                                <span class=\"kt-widget__date\">
                                    {{'Notes'|__}}
                                </span>
                                <div class=\"kt-widget__label notes_scroll\">
                                    <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">{{order.notes.count}}</span>
                                </div>
                            </div>
                            <div class=\"kt-widget__item\">
                                <span class=\"kt-widget__date\">
                                    {{'Package Receive Date'|__}}
                                </span>
                                <div class=\"kt-widget__label\">
                                    {% if addShipmentForm == \"add_form_normal\" %}
                                    <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">{{order.created_at|date(settings.dateformat)}}</span>
                                    {% else %}
                                    <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">{{order.ship_date|date(settings.dateformat)}}</span>
                                    {% endif %}
                                </div>
                            </div>
                            {% if addShipmentForm != \"add_form_simple\" %}
                            <div class=\"kt-widget__item\">
                                <span class=\"kt-widget__date\">
                                    {{'Received Date'|__}}
                                </span>
                                <div class=\"kt-widget__label\">
                                    <span class=\"btn btn-label-{{progress_status}} btn-sm btn-bold btn-upper\">{{order.releasedNote_received|date(settings.dateformat)}}</span>
                                </div>
                            </div>
                            {% elseif addShipmentForm != \"add_form_simple\" %}
                                {% if order.receive_date %}
                                    <div class=\"kt-widget__item\">
                                        <span class=\"kt-widget__date\">
                                            {{'Received Date'|__}}
                                        </span>
                                        <div class=\"kt-widget__label\">
                                            <span class=\"btn btn-label-{{progress_status}} btn-sm btn-bold btn-upper\">{{order.receive_date|date(settings.dateformat)}}</span>
                                        </div>
                                    </div>
                                {% else %}
                                    <div class=\"kt-widget__item\">
                                        <span class=\"kt-widget__date\">
                                            {{'Package Expected Delievery Date'|__}}
                                        </span>
                                        <div class=\"kt-widget__label\">
                                            {% if order.delivery_date %}
                                                <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">{{order.delivery_date|date(settings.dateformat)}}</span>{% if order.postponed == 1 and order.requested in [1,3,5,6,7,10,11] %} | <span class=\"btn btn-label-info btn-sm btn-bold btn-upper\">{{'POSTPONED'|__}}</span>{% endif %}
                                            {% else %}
                                                <span class=\"btn btn-label-brand btn-sm btn-bold btn-upper\">
                                                {%if order.deliverytime %}
                                                    {{order.ship_date|date_modify(\"+\"~order.deliverytime.count~\" hours\")|date(settings.dateformat)}}
                                                {% else %}
                                                    {{ null }}
                                                {% endif %}
                                                </span>
                                            {% endif %}
                                        </div>
                                    </div>
                                {% endif %}
                            {% endif %}

                            <div class=\"kt-widget__item flex-fill\">
                                <span class=\"kt-widget__subtitel\">{{'Progress'|__}}</span>
                                <div class=\"kt-widget__progress d-flex  align-items-center\">
                                    <div class=\"progress\" style=\"height: 5px;width: 100%;\">
                                        <div class=\"progress-bar kt-bg-{{progress_status}}\" role=\"progressbar\" style=\"width: {{progress}}%;\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                                    </div>
                                    <span class=\"kt-widget__stat\">
                                        {{progress}}%
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end:: Portlet-->


<div class=\"row\">
    <div class=\"col-xl-6\">
        <!--begin:: Widgets/Order Statistics-->
        <div class=\"kt-portlet kt-portlet--height-fluid\">
        \t<div class=\"kt-portlet__head\">
        \t\t<div class=\"kt-portlet__head-label\">
        \t\t\t<h3 class=\"kt-portlet__head-title\">
        \t\t\t\t{{'Order Details'|__}}
        \t\t\t</h3>
        \t\t</div>
        \t</div>
        \t<div class=\"kt-portlet__body kt-portlet__body--fluid\">
        \t\t<div class=\"kt-widget12\">
        \t\t\t<div class=\"kt-widget12__content\">
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Shipment Type'|__}}</span>
                                <span class=\"kt-widget12__value\">
                                    {% if order.type == 1 %}
                                        {{'Receive'|__}}
                                    {% else %}
                                        {{'Send'|__}}
                                    {% endif %}
                                </span>
                            </div>
                            {% if addShipmentForm == \"add_form_advance\" %}
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Package Type'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.packaging.name}}</span>
                            </div>
                            {% endif %}
                        </div>
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Status'|__}}</span>
                                <span class=\"kt-widget12__value\">
                                    <span class=\"btn btn-label-{{order.status.color}} btn-sm btn-bold btn-upper\">{{order.status.name}}</span>
                                    {% if progress_status == 'danger' %} | <span class=\"btn btn-label-{{progress_status}} btn-sm btn-bold btn-upper\">{{'Delayed'|__}}</span>{% endif %}{% if order.requested == 10 or order.requested == 11 %} | <span class=\"btn btn-label-warning btn-sm btn-bold btn-upper\">{{'RETURNING DISCARDS'|__}}</span>{% endif %}{% if order.postponed == 1 %} | <span class=\"btn btn-label-info btn-sm btn-bold btn-upper\">{{'POSTPONED'|__}}</span>{% endif %}{% if order.requested == 12 and row.status.equal != 12 %} | <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">{{'Supplied'|__}}</span>{% endif %}
                                </span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Channel'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.channel|__}}</span>
                            </div>
                        </div>
                        {% if addShipmentForm == \"add_form_normal\" %}
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Label'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.label.name}}</span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Ground Handler'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.handler.name}}</span>
                            </div>
                        </div>
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Breakdown'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.breakdown.name}}</span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Tranfer to Jost'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.transfer_jost? 'Yes' : 'No'}}</span>
                            </div>
                        </div>
                        {% elseif addShipmentForm == \"add_form_advance\" %}
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Payment Type'|__}}</span>
                                <span class=\"kt-widget12__value\">{% if order.payment_type == 1 %}{{'Postpaid'|__}}{% elseif order.payment_type == 2 %}{{'Prepaid'|__}}{% endif %}</span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Currency'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.currency.name}}</span>
                            </div>
                        </div>
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Office'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.office.name}}</span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Delivery Time'|__}}</span>
                                <span class=\"kt-widget12__value\">{{order.deliverytime.name}}</span>
                            </div>
                        </div>
                        {% endif %}
                        {% if order.assigned_id or order.manifest_id %}
                            <div class=\"kt-widget12__item\">
                                {% if order.manifest_id %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Manifest'|__}}</span>
                                        <span class=\"kt-widget12__value\">#{{order.currency.id}}</span>
                                        {% if order.manifest.driver %}
                                            <span class=\"kt-widget12__value kt-font-sm\">{{order.manifest.driver.name}}</span>
                                        {% endif %}
                                    </div>
                                {% endif %}
                                {% if order.assigned_id %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Assigned To'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{order.responsiable.name}}</span>
                                        {% if order.responsiable.mobile %}
                                            <span class=\"kt-widget12__value kt-font-sm\">{{order.responsiable.mobile}}</span>
                                        {% endif %}
                                    </div>
                                {% endif %}
                            </div>
                        {% endif %}
                        {% if order.courier_id or order.mode_id %}
                            <div class=\"kt-widget12__item\">
                                {% if order.courier_id %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Courier Company'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{order.courier.name}}</span>
                                    </div>
                                {% endif %}
                                {% if order.mode_id %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Shipping Mode'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{order.mode.name}}</span>
                                    </div>
                                {% endif %}
                            </div>
                        {% endif %}

                        {% if order.requested in [4,5,6,10,11,12] %}
                            {% if order.delivered_by != order.delivered_responsiable %}
                                <div class=\"kt-widget12__item\">
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Delivered Responsiabality'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{order.delivered_responsiabality.name}}</span>
                                    </div>
                                </div>
                            {% endif %}
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Delivered By'|__}}</span>
                                    <span class=\"kt-widget12__value\">{{order.delivered_employee.name}}</span>
                                </div>
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Received By'|__}}</span>
                                    <span class=\"kt-widget12__value\">{{order.received_by}}</span>
                                </div>
                            </div>
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Receiver ID Copy'|__}}</span>
                                    <span class=\"kt-widget12__value\">
                                        <img src=\"{{order.id_copy.path}}\" style=\"max-height:350px;\" alt=\"\" />
                                    </span>
                                </div>
                            </div>
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Receiver Signature'|__}}</span>
                                    <span class=\"kt-widget12__value\">
                                        <img src=\"{{order.esign}}\" style=\"max-height:350px;\" alt=\"\" />
                                    </span>
                                </div>
                            </div>
                        {% endif %}
        \t\t\t</div>
        \t\t</div>
        \t</div>
        </div>
        <!--end:: Widgets/Order Statistics-->
    </div>
    <div class=\"col-xl-6\">
        <!--begin:: Widgets/Order Statistics-->
        <div class=\"kt-portlet kt-portlet--height-fluid\">
        \t<div class=\"kt-portlet__head\">
        \t\t<div class=\"kt-portlet__head-label\">
        \t\t\t<h3 class=\"kt-portlet__head-title\">
        \t\t\t\t{{'Payment&Items Details'|__}}
        \t\t\t</h3>
        \t\t</div>
        \t</div>
        \t<div class=\"kt-portlet__body kt-portlet__body--fluid\">
        \t\t<div class=\"kt-widget12\">
        \t\t\t<div class=\"kt-widget12__content\">
                        {% if order.items is not empty %}
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                            \t\t<div class=\"table-responsive\">
                                        {% if addShipmentForm == \"add_form_normal\" %}
                                        <table class=\"table table-dark\">
                                            <thead>
                                                <tr>
                                                    <th>{{\"AirwayBill\"|__}}</th>
                                                    <th>{{\"Info\"|__}}</th>
                                                    <th>{{\"Weight\"|__}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {% for item in order.items %}
                                                    <tr>
                                                        <th scope=\"row\">{{order.airWayBill}}</th>
                                                        <td>
                                                            {% if item.description %}
                                                                {{item.description}}<br />
                                                            {% endif %}
                                                            <b>{{'Number of boxes'|__}}:</b> {{item.num_boxes}}<br />
                                                            <b>{{'Number of boxes'|__}}:</b> {{item.num_pallets}}<br />
                                                        </td>
                                                        <td>{{item.weight}} {{'Kg'|__}}</td>
                                                    </tr>
                                                {% endfor %}
                                            </tbody>
                                        </table>
                                        {% else %}
                                        <table class=\"table table-dark\">
                    \t\t\t\t\t  \t<thead>
                    \t\t\t\t\t    \t<tr>
                    \t\t\t\t\t      \t\t<th>{{\"Category\"|__}}</th>
                    \t\t\t\t\t      \t\t<th>{{\"Info\"|__}}</th>
                    \t\t\t\t\t      \t\t<th>{{\"Weight\"|__}}</th>
                    \t\t\t\t\t    \t</tr>
                    \t\t\t\t\t  \t</thead>
                    \t\t\t\t\t  \t<tbody>
                                                {% for item in order.items %}
                        \t\t\t\t\t    \t<tr>
                        \t\t\t\t\t\t      \t<th scope=\"row\">{{item.category.name}}</th>
                        \t\t\t\t\t\t      \t<td>
                                                            {% if item.description %}
                                                                {{item.description}}<br />
                                                            {% endif %}
                                                            <b>{{'Quantity'|__}}:</b> {{item.quantity}}<br />
                                                            {% if addShipmentForm != \"add_form_simple\" %}
                                                            <b>{{'Dimensions'|__}}:</b> {{item.length}} {{'cm'|__}} x {{item.width}} {{'cm'|__}} x {{item.height}} {{'cm'|__}}
                                                            {% endif %}
                                                        </td>
                        \t\t\t\t\t\t      \t<td>{{item.weight}} {{'Kg'|__}}</td>
                        \t\t\t\t\t    \t</tr>
                                                {% endfor %}
                    \t\t\t\t\t  \t</tbody>
                    \t\t\t\t\t</table>
                                        {% endif %}
                                    </div>
                                </div>
                            </div>
                        {% endif %}
                        
                        {% if addShipmentForm == \"add_form_normal\" %}
                        {% if order.custom_clearance != 0 or order.fiscal_representaion != 0 %}
                            <div class=\"kt-widget12__item\">
                                {% if order.custom_clearance != 0 %}
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Custom Clearance'|__}}</span>
                                    <span class=\"kt-widget12__value\">{{order.custom_clearance|currency}}</span>
                                </div>
                                {% endif %}
                                {% if order.fiscal_representation != 0 %}
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Fisacl Representaion'|__}}</span>
                                    <span class=\"kt-widget12__value\">{{order.fiscal_representation|currency}}</span>
                                </div>
                                {% endif %}
                            </div>
                        {% endif %}
                        {% if order.payment_term != 0 or order.price_kg != 0 %}
                            <div class=\"kt-widget12__item\">
                                {% if order.payment_term != 0 %}
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Invoice Payment Term'|__}}</span>
                                    <span class=\"kt-widget12__value\">{{order.payment_term|currency}}</span>
                                </div>
                                {% endif %}
                                {% if order.price_kg != 0 %}
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Handling Price per Kg '|__}}</span>
                                    <span class=\"kt-widget12__value\">{{order.price_kg|currency}}</span>
                                </div>
                                {% endif %}
                            </div>
                        {% endif %}
                        {% if order.storage_fee == 1 %}
                            <div class=\"kt-widget12__item\">
                                {% if order.cost_24 != 0 %}
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Storage Costs after 24 Hours'|__}}</span>
                                    <span class=\"kt-widget12__value\">{{order.cost_24|currency}}</span>
                                </div>
                                {% endif %}
                                {% if order.cost_48 != 0 %}
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Storage Costs after 48 Hours '|__}}</span>
                                    <span class=\"kt-widget12__value\">{{order.cost_48|currency}}</span>
                                </div>
                                {% endif %}
                            </div>
                        {% endif %}
                       
                        <div class=\"kt-widget12__item\">
                            {% set sub_total = order.price_kg * order.details.weight %}
                            {% set store_fee = order.payment_term * order.cost_24%}
                            {% set total = order.custom_clearance+order.fiscal_representation + sub_total + store_fee %}
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Sub Total'|__}}</span>
                                <span class=\"kt-widget12__value\">
                                        {{sub_total|currency}}
                                </span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Total'|__}}</span>
                                <span class=\"kt-widget12__value\">
                                        {{total|currency}}
                                </span>
                            </div>
                        </div>
                        {% else %}
                            {% if order.payments.where('item_id', this.param.id).where('for_id', order.receiver_id).first.status == 3 and order.receiver_id == user.id %}

                            {% elseif order.payments.where('item_id', this.param.id).where('for_id', order.sender_id).first.status == 3 and order.sender_id == user.id %}

                            {% else %}

                                {% if settings.payment.enable == 1 and order.receiver_id and order.requested != 0 and order.requested != 2 %}
                                    {% if 'paystack' in settings.payment.gateways %}
                                        {% if user.id in [order.sender_id,order.receiver_id]%}
                                            <div class=\"kt-widget12__item\">
                                                <div class=\"kt-widget12__info\">
                                                    <form class=\"kt-widget12__desc\">
                                                        <script src=\"https://js.paystack.co/v1/inline.js\"></script>
                                                        <button type=\"button\" class=\"btn btn-wide btn-success btn-bold btn-upper btn-elevate btn-elevate-air\" onclick=\"payWithPaystack()\"> <i class=\"fa fa-dollar-sign\"></i> {{'PAY ONLINE'|__}} </button>
                                                    </form>
                                                    <span class=\"kt-widget12__value\">
                                                        <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"{{'Paystack only uses GHS currency so that is why we change the money for it'|__}}\" data-original-title=\"{{'Paystack only uses GHS currency so that is why we change the money for it'|__}}\" data-skin=\"dark\">{{\"The amount will be changed for Ghanaian cedi for using the paystack payment gateway, the changed price will equal the same as you should pay\"|__}}</span>
                                                        <span class=\"btn btn-label-warning btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"{{'If you are sender or receiver you will have to pay the amount if its prepaid or postpaid just if you are the resposnsiable of this payment'|__}}\" data-original-title=\"{{'If you are sender or receiver you will have to pay the amount if its prepaid or postpaid just if you are the resposnsiable of this payment'|__}}\" data-skin=\"dark\">{{\"The amount you will pay online is what is requested from you only\"|__}}</span>
                                                    </span>
                                                </div>
                                            </div>
                                        {% else %}
                                            <div class=\"alert alert-info\" role=\"alert\">
                                                <div class=\"alert-icon\"><i class=\"flaticon-info\"></i></div>
                                                <div class=\"alert-text\">
                                                    {{'Sender and receiver can see the online payment option if they have to pay something they can pay it online'|__}}.
                                                </div>
                                                <div class=\"alert-close\">
                                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                                        <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        {% endif %}
                                    {% endif %}
                                {% endif %}

                            {% endif %}
                            {% if order.tax != 0 or order.insurance != 0 or order.customs_value != 0 %}
                                <div class=\"kt-widget12__item\">
                                    {% if order.tax != 0 %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Tax'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{order.tax}}%</span>
                                    </div>
                                    {% endif %}
                                    {% if order.insurance != 0 %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Insurance'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{order.insurance}}%</span>
                                    </div>
                                    {% endif %}
                                    {% if order.customs_value != 0 %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Customs Value'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{order.customs_value}}</span>
                                    </div>
                                    {% endif %}
                                </div>
                            {% endif %}
                            {% if user.role_id in [1,2,3,6] or user.is_superuser %}
                                <div class=\"kt-widget12__item\">
                                    {% if order.type == 1 and order.receiver_id is null %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'Pickup Cost'|__}}</span>
                                            <span class=\"kt-widget12__value\">
                                                {{order.courier_fee|currency}}
                                                {% if order.payments.where('payment_type', 'courier_fee').first.status == 3 %}
                                                    <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">{{'Paid'|__}}</span>
                                                {% endif %}
                                            </span>
                                        </div>
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'Shipping Cost'|__}}</span>
                                            <span class=\"kt-widget12__value\">
                                                {{settings.fees.delivery_cost|currency}} <!--{% if order.requested in [100,0,1] %}<span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-original-title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-skin=\"dark\">{{\"Not confirmed yet\"|__}}</span>{% endif %}-->
                                            </span>
                                        </div>
                                    {% else %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'Shipping Cost'|__}}</span>
                                            <span class=\"kt-widget12__value\">
                                                {{order.courier_fee|currency}} <!--{% if order.requested in [100,0,1] %}<span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-original-title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-skin=\"dark\">{{\"Not confirmed yet\"|__}}</span>{% endif %}-->
                                            </span>
                                        </div>
                                    {% endif %}
                                    {% if order.return_defray == 1 %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'Package Cost'|__}}</span>
                                            <span class=\"kt-widget12__value\">
                                                {{order.package_fee|currency}}
                                                {% if order.payments.where('payment_type', 'package_fee').first.status == 2 %}
                                                    <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\">{{'Canceled'|__}}</span>
                                                {% elseif order.requested == 12 %}
                                                    <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">{{'Paid'|__}}</span>
                                                {% endif %}
                                            </span>
                                        </div>
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'Return Courier Cost'|__}}</span>
                                            <span class=\"kt-widget12__value\">{{order.return_courier_fee|currency}}</span> <!--{% if order.requested in [100,0,1] %}<span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-original-title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-skin=\"dark\">{{\"Not confirmed yet\"|__}}</span>{% endif %}-->
                                        </div>
                                    {% else %}
                                        {% if order.payments.where('payment_type', 'return_package_fee').first %}
                                            <div class=\"kt-widget12__info\">
                                                <span class=\"kt-widget12__desc\">{{'Return Courier Cost'|__}}</span>
                                                <span class=\"kt-widget12__value\">
                                                    {{order.return_courier_fee|currency}}
                                                    {% if order.payments.where('payment_type', 'return_package_fee').first.status == 2 %}
                                                        <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\">{{'Canceled'|__}}</span>
                                                    {% elseif order.payments.where('payment_type', 'return_package_fee').first.status == 3 %}
                                                        <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">{{'Paid'|__}}</span>
                                                    {% endif %}
                                                </span>
                                            </div>
                                        {% endif %}
                                    {% endif %}
                                </div>
                            {% endif %}
                            {% if addShipmentForm != \"add_form_simple\" %}
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    {% if user.id == order.receiver_id %}
                                        <span class=\"kt-widget12__desc\">{{'Total Requested From You'|__}}</span>
                                    {% else %}
                                        <span class=\"kt-widget12__desc\">{{'Total Requested From The Receiver'|__}}</span>
                                    {% endif %}
                                    <span class=\"kt-widget12__value\">
                                        {{receiver_fees|currency}}<!--{% if order.requested in [100,0,1] %} <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-original-title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-skin=\"dark\">{{\"Not confirmed yet\"|__}}</span>{% endif %}-->
                                        {% if order.payments.where('item_id', this.param.id).where('for_id', order.receiver_id).first.status == 3 %}
                                            <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">{{'Paid'|__}}</span>
                                        {% endif %}
                                    </span>
                                    <!--
                                        {% if order.type == 1 and order.receiver_id is null %}
                                            <span class=\"muted-text kt-font-danger\">{{'The correct fees will be applied after selecting the receiver address'|__}}</span>
                                        {% endif %}</span>
                                    -->
                                </div>
                            </div>
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    {% if user.id == order.sender_id %}
                                        <span class=\"kt-widget12__desc\">{{'Total Requested From You'|__}}</span>
                                    {% else %}
                                        <span class=\"kt-widget12__desc\">{{'Total Requested From The Sender'|__}}</span>
                                    {% endif %}
                                    <span class=\"kt-widget12__value\">
                                        {{sender_fees|currency}}<!--{% if order.requested in [100,0,1] %} <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-original-title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-skin=\"dark\">{{\"Not confirmed yet\"|__}}</span>{% endif %}-->
                                        {% if order.payments.where('item_id', this.param.id).where('for_id', order.sender_id).first.status == 3 %}
                                            <span class=\"btn btn-label-success btn-sm btn-bold btn-upper\">{{'Paid'|__}}</span>
                                        {% endif %}
                                    </span>
                                    <!--
                                        {% if order.type == 1 and order.receiver_id is null %}
                                            <span class=\"muted-text kt-font-danger\">{{'The correct fees will be applied after selecting the receiver address'|__}}</span>
                                        {% endif %}
                                    -->
                                </div>
                            </div>
                            {% endif %}
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Total received'|__}}</span>
                                    <span class=\"kt-widget12__value\">{{total_received|currency}}</span>
                                </div>
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Total remaining'|__}}</span>
                                    <span class=\"kt-widget12__value\">
                                        {% if order.return_defray == 1 and order.return_package_fee == 2 and order.requested in [4,5,6,8,9,10,11,12]%}
                                            {{total_remaining|currency}} <span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\">{{\"The requested amount of the sender was deducted from the return\"|__}}</span>
                                        {% else %}
                                            {{total_remaining|currency}}
                                        {% endif %} <!--{% if order.requested in [100,0,1] %}<span class=\"btn btn-label-danger btn-sm btn-bold btn-upper\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-original-title=\"{{'The actual cost will be calculated when we receive the package'|__}}\" data-skin=\"dark\">{{\"Not confirmed yet\"|__}}</span>{% endif %}-->
                                    </span>
                                </div>
                            </div>
                        {% endif %}
                    </div>
                </div>
        \t</div>
        </div>
        <!--end:: Widgets/Order Statistics-->
    </div>
</div>
<div class=\"row\">
    <div class=\"col-xl-{% if order.type == 1 and order.receiver_id == null %}12{% else %}6{% endif %}\">
        <!--begin:: Widgets/Order Statistics-->
        <div class=\"kt-portlet kt-portlet--height-fluid\">
        \t<div class=\"kt-portlet__head\">
        \t\t<div class=\"kt-portlet__head-label\">
        \t\t\t<h3 class=\"kt-portlet__head-title\">
        \t\t\t\t{{'Sender Details'|__}}
        \t\t\t</h3>
        \t\t</div>
        \t</div>
        \t<div class=\"kt-portlet__body kt-portlet__body--fluid\">
        \t\t<div class=\"kt-widget12\">
        \t\t\t<div class=\"kt-widget12__content\">
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Name'|__}}</span>
                                <span class=\"kt-widget12__value\">
                                    {% if order.sender_name %}
                                        {{order.sender_name|capitalize}}
                                    {% else%}
                                        {{order.sender.name|capitalize}}
                                    {% endif %}
                                    {% if order.sender.userverify_dateverified %}
                                        <i class=\"flaticon2-correct kt-font-info\"></i>
                                    {% endif %}
                                </span>
                            </div>
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Mobile'|__}}</span>
                                {% if order.sender_mobile %}
                                <span class=\"kt-widget12__value\">{{order.sender_mobile}}</span>
                                {% else%}
                                <span class=\"kt-widget12__value\">{{order.sender.mobile}}</span>
                                {% endif %}
                            </div>
                        </div>
                        {% if order.sender.email or order.sender.gender %}
                            <div class=\"kt-widget12__item\">
                                {% if order.sender.email %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Email'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{ order.sender.email }}</span>
                                    </div>
                                {% endif %}
                                {% if order.sender.gender %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Gender'|__}}</span>
                                        <span class=\"kt-widget12__value\">{{order.sender.gender}}</span>
                                    </div>
                                {% endif %}
                            </div>
                        {% endif %}
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Sender Address'|__}}</span>
                                <span class=\"kt-widget12__value\">
                                    {% if order.sender_addr %}
                                        {{order.sender_addr|capitalize}}
                                    {% else%}
                                        {{order.sender_address.name|capitalize}}
                                    {% endif %}
                                </span>
                            </div>
                        </div>
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                                <span class=\"kt-widget12__desc\">{{'Street'|__}}</span>
                                <span class=\"kt-widget12__value\">
                                    {% if order.sender_addr %}
                                        {{order.sender_addr|capitalize}}
                                    {% else%}
                                        {{order.sender_address.street}}
                                    {% endif%}
                                </span>
                            </div>
                        </div>
                        {% if order.sender_address.area or order.sender_address.thecity %}
                            <div class=\"kt-widget12__item\">
                                {% if order.sender_address.area %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'County'|__}}</span>
                                        <span class=\"kt-widget12__value\">
                                            {% if order.sender_area_id %}
                                                {{order.sender_area_id|capitalize}}
                                            {% else%}
                                                {{order.sender_address.area.name}}
                                            {% endif %}
                                        </span>
                                    </div>
                                {% endif %}
                                {% if order.sender_address.thecity %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'City'|__}}</span>
                                        <span class=\"kt-widget12__value\">
                                            {% if order.sender_city_id %}
                                                {{order.sender_city_id|capitalize}}
                                            {% else%}
                                                {{order.sender_address.thecity.name}}
                                            {% endif %}
                                        </span>
                                    </div>
                                {% endif %}
                            </div>
                        {% endif %}
                        {% if order.sender_address.thestate or order.sender_address.thecountry %}
                            <div class=\"kt-widget12__item\">
                                {% if order.sender_address.thestate %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'State'|__}}</span>
                                        <span class=\"kt-widget12__value\">
                                            {{order.sender_address.thestate.name}}
                                        </span>
                                    </div>
                                {% endif %}
                                {% if order.sender_address.thecountry %}
                                    <div class=\"kt-widget12__info\">
                                        <span class=\"kt-widget12__desc\">{{'Country'|__}}</span>
                                        <span class=\"kt-widget12__value\">
                                            {{order.sender_address.thecountry.name}}
                                        </span>
                                    </div>
                                {% endif %}
                            </div>
                        {% endif %}
                        {% if addShipmentForm == \"add_form_advance\" %}
                        <div class=\"kt-widget12__item\">
                            <div class=\"kt-widget12__info\">
                        \t\t<div class=\"kt-widget15\">
                        \t\t\t<div class=\"kt-widget15__map\">
                        \t\t\t\t<div id=\"kt_map_sender\" style=\"height:320px;\"></div>
                        \t\t\t</div>
                                </div>
                            </div>
                        </div>
                        {% endif %}
        \t\t\t</div>
        \t\t</div>
        \t</div>
        </div>
        <!--end:: Widgets/Order Statistics-->
    </div>
    {% if addShipmentForm != \"add_form_normal\" %}
    {% if order.type == 2 or order.receiver_id != null %}
        <div class=\"col-xl-6\">
            <!--begin:: Widgets/Order Statistics-->
            <div class=\"kt-portlet kt-portlet--height-fluid\">
            \t<div class=\"kt-portlet__head\">
            \t\t<div class=\"kt-portlet__head-label\">
            \t\t\t<h3 class=\"kt-portlet__head-title\">
            \t\t\t\t{{'Receiver Details'|__}}
            \t\t\t</h3>
            \t\t</div>
            \t</div>
            \t<div class=\"kt-portlet__body kt-portlet__body--fluid\">
            \t\t<div class=\"kt-widget12\">
            \t\t\t<div class=\"kt-widget12__content\">
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Name'|__}}</span>
                                    <span class=\"kt-widget12__value\">
                                        {% if order.receiver_name %}
                                            {{order.receiver_name|capitalize}}
                                        {% else%}
                                            {{order.receiver.name|capitalize}}
                                        {% endif %}
                                        {% if order.receiver.userverify_dateverified %}
                                            <i class=\"flaticon2-correct kt-font-info\"></i>
                                        {% endif %}
                                    </span>
                                </div>
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Mobile'|__}}</span>
                                    {% if order.receiver_mobile %}
                                    <span class=\"kt-widget12__value\">{{order.receiver_mobile|capitalize}}</span>
                                    {% else%}
                                    <span class=\"kt-widget12__value\">{{order.receiver.mobile}}</span>
                                    {% endif %}
                                </div>
                            </div>
                            {% if order.receiver.email or order.receiver.gender %}
                                <div class=\"kt-widget12__item\">
                                    {% if order.receiver.email %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'Email'|__}}</span>
                                            <span class=\"kt-widget12__value\">{{ order.receiver.email }}</span>
                                        </div>
                                    {% endif %}
                                    {% if order.receiver.gender %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'Gender'|__}}</span>
                                            <span class=\"kt-widget12__value\">{{order.receiver.gender}}</span>
                                        </div>
                                    {% endif %}
                                </div>
                            {% endif %}
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Receiver Address'|__}}</span>
                                    <span class=\"kt-widget12__value\">
                                        {% if order.receiver_addr %}
                                            {{order.receiver_addr|capitalize}}
                                        {% else%}
                                            {{order.receiver_address.name|capitalize}}
                                        {% endif %}
                                    </span>
                                </div>
                            </div>
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                                    <span class=\"kt-widget12__desc\">{{'Street'|__}}</span>
                                    <span class=\"kt-widget12__value\">
                                        {% if order.receiver_addr %}
                                            {{order.receiver_addr|capitalize}}
                                        {% else%}
                                            {{order.receiver_address.street}}
                                        {% endif %}
                                    </span>
                                </div>
                            </div>
                            {% if order.receiver_address.area or order.receiver_address.thecity %}
                                <div class=\"kt-widget12__item\">
                                    {% if order.receiver_address.area %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'County'|__}}</span>
                                            <span class=\"kt-widget12__value\">
                                                {% if order.receiver_area_id %}
                                                    {{order.receiver_area_id|capitalize}}
                                                {% else%}
                                                    {{order.receiver_address.area.name}}
                                                {% endif %}
                                            </span>
                                        </div>
                                    {% endif %}
                                    {% if order.receiver_address.thecity %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'City'|__}}</span>
                                            <span class=\"kt-widget12__value\">
                                                {% if order.receiver_city_id %}
                                                    {{order.receiver_city_id|capitalize}}
                                                {% else%}
                                                    {{order.receiver_address.thecity.name}}
                                                {% endif %}
                                            </span>
                                        </div>
                                    {% endif %}
                                </div>
                            {% endif %}
                            {% if order.receiver_address.thestate or order.receiver_address.thecountry %}
                                <div class=\"kt-widget12__item\">
                                    {% if order.receiver_address.thestate %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'State'|__}}</span>
                                            <span class=\"kt-widget12__value\">
                                                {{order.receiver_address.thestate.name}}
                                            </span>
                                        </div>
                                    {% endif %}
                                    {% if order.receiver_address.thecountry %}
                                        <div class=\"kt-widget12__info\">
                                            <span class=\"kt-widget12__desc\">{{'Country'|__}}</span>
                                            <span class=\"kt-widget12__value\">
                                                {{order.receiver_address.thecountry.name}}
                                            </span>
                                        </div>
                                    {% endif %}
                                </div>
                            {% endif %}
                            {% if addShipmentForm != \"add_form_simple\" %}
                            <div class=\"kt-widget12__item\">
                                <div class=\"kt-widget12__info\">
                            \t\t<div class=\"kt-widget15\">
                            \t\t\t<div class=\"kt-widget15__map\">
                            \t\t\t\t<div id=\"kt_map_receiver\" style=\"height:320px;\"></div>
                            \t\t\t</div>
                                    </div>
                                </div>
                            </div>
                            {% endif %}
            \t\t\t</div>
            \t\t</div>
            \t</div>
            </div>
            <!--end:: Widgets/Order Statistics-->
        </div>
    {% endif %}
    {% endif %}
</div>
<div class=\"row\">
    <div class=\"col-xl-6\">
        <!--begin:: Widgets/Last Updates-->
        <div class=\"kt-portlet kt-portlet--height-fluid\">
            <div class=\"kt-portlet__head\">
                <div class=\"kt-portlet__head-label\">
                    <h3 class=\"kt-portlet__head-title\">{{'Latest Updates'|__}}</h3>
                </div>
            </div>
            <div class=\"kt-portlet__body\">
                <div class=\"kt-scroll\" data-scroll=\"true\" style=\"height: 300px\">
                    {% for activity in order.activities %}
                        <!--begin::widget 12-->
                        <div class=\"kt-widget4\">
                            <div class=\"kt-widget4__item\">
                                <span class=\"kt-widget4__icon\"><i class=\"flaticon-pie-chart-1 kt-font-info\"></i></span>
                                <div class=\"kt-widget4__info\">
        \t\t\t\t\t\t\t<a href=\"javascript:;\" class=\"kt-widget4__username\">
                                        {% if activity.other %}
                                            {% set text = 'activity_'~activity.description %}
                                            {{text|__}}: {{activity.other}}
                                        {% else %}
                                            {% set text = 'activity_'~activity.description %}
            \t\t\t\t\t\t\t\t{{text|__}}
                                        {% endif %}
        \t\t\t\t\t\t\t</a>
        \t\t\t\t\t\t\t<p class=\"kt-widget4__text\">
        \t\t\t\t\t\t\t\t{{activity.created_at|date(settings.dateformat)}} {{activity.created_at|date('h:i')}} {{activity.created_at|date('a')|__}}
        \t\t\t\t\t\t\t</p>
        \t\t\t\t\t\t</div>
                                <span class=\"kt-widget4__number kt-font-info\">{{activity.user.name}}</span>
                            </div>
                        </div>
                        <!--end::Widget 12-->
                    {% else %}
                        <div class=\"kt-notification\">
                            <div class=\"kt-notification__item\">
                                <div class=\"kt-notification__item-icon\">
                                    <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"24px\" height=\"24px\" viewBox=\"0 0 24 24\" version=\"1.1\" class=\"kt-svg-icon kt-svg-icon--brand\">
                                        <g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">
                                            <rect x=\"0\" y=\"0\" width=\"24\" height=\"24\"/>
                                            <path d=\"M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z\" fill=\"#000000\"/>
                                            <rect fill=\"#000000\" opacity=\"0.3\" transform=\"translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) \" x=\"16.3255682\" y=\"2.94551858\" width=\"3\" height=\"18\" rx=\"1\"/>
                                        </g>
                                    </svg>
                                </div>
                                <div class=\"kt-notification__item-details\">
                                    <div class=\"kt-notification__item-title\">
                                        {{'No activities found'|__}}.
                                    </div>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        </div>
        <!--end:: Widgets/Last Updates-->
    </div>
    <div class=\"col-xl-6\" id=\"notes_continer\">

        <div class=\"kt-portlet kt-portlet--height-fluid\">
            <div class=\"kt-portlet__head\">
                <div class=\"kt-portlet__head-label\">
                    <h3 class=\"kt-portlet__head-title\">{{'Notes'|__}}</h3>
                </div>
            </div>
            <div class=\"kt-portlet__body\">
                {{ form_ajax('onNote', {success:\"flashRequest(data.type,data.message);\$('#note_message').val('');\", class:'kt_form notes' }) }}
                    <div class=\"form-group form-group-last kt-hide\">
                        <div class=\"alert alert-danger kt_form_msg_notes\" role=\"alert\">
                            <div class=\"alert-icon\"><i class=\"flaticon-warning\"></i></div>
                            <div class=\"alert-text\">
                                {{'Oh snap! Change a few things up and try submitting again'|__}}.
                            </div>
                            <div class=\"alert-close\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                    <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <textarea class=\"form-control\" id=\"note_message\" rows=\"3\" name=\"note\" placeholder=\"{{'Write your notes'|__}}\" required></textarea>
                    </div>
                    <div class=\"row\">
                        <div class=\"col\">
                            <button type=\"submit\" class=\"btn btn-label-brand btn-bold\">{{'Add note'|__}}</button>
                            <button type=\"reset\" class=\"btn btn-clean btn-bold\">{{'Cancel'|__}}</button>
                        </div>
                    </div>
                {{ form_close() }}

                <div class=\"kt-separator kt-separator--space-lg kt-separator--border-dashed\"></div>
                <div id=\"notes\">
                    {% partial 'notes' item=order %}
                </div>
            </div>
        </div>
    </div>
    <!--
        <div class=\"col-xl-6 kt-todo\">
            <div class=\"kt-grid__item kt-grid__item--fluid  kt-portlet kt-portlet--height-fluid kt-todo__list\" id=\"kt_todo_list\">
                <div class=\"kt-portlet__body kt-portlet__body--fit-x\">
                    <div class=\"kt-todo__head\">
                        <div class=\"kt-todo__toolbar\">
                            <div class=\"kt-todo__info\">
                                <span class=\"kt-todo__name\">
                                    {{'Order Tasks'|__}}
                                </span>
                            </div>
                            <div class=\"kt-todo__details\">
                                <a href=\"javascript:;\" class=\"btn btn-label-success btn-upper btn-sm btn-bold\">{{'New Task'|__}}</a>
                            </div>
                        </div>
                    </div>

                    <div class=\"kt-todo__body\">
                        {% for task in tasks %}
                            <div class=\"kt-todo__items\" data-type=\"task\">
                                <div class=\"kt-todo__item kt-todo__item--unread\" data-id=\"1\" data-type=\"task\">
                                    <div class=\"kt-todo__info\">
                                        <div class=\"kt-todo__actions\">
                                            <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--tick kt-checkbox--brand\">
                                                <input type=\"checkbox\">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class=\"kt-todo__details\" data-toggle=\"view\">
                                        <div class=\"kt-todo__message\">
                                            <span class=\"kt-todo__subject\">{{task.title}}</span>
                                        </div>
                                        <div class=\"kt-todo__labels\">
                                            <span class=\"kt-todo__label kt-badge kt-badge--unified-brand kt-badge--bold kt-badge--inline\">{{task.comment}}</span>
                                        </div>
                                    </div>
                                    <div class=\"kt-todo__datetime\" data-toggle=\"view\">
                                        8:30 PM
                                    </div>
                                    <div class=\"kt-todo__sender\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"{{task.user.name}}\">
                                        {% if task.user.avatar %}
                                             <span class=\"kt-media kt-media--sm kt-media--danger kt-hidden\" style=\"background-image: url('{{task.user.avatar.thumb(100, 100,'crop')}}')\">
                                                 <span></span>
                                             </span>
                                        {% else %}
                                            <div class=\"kt-todo__sender\" data-toggle=\"kt-tooltip\" data-placement=\"top\" title=\"\" data-original-title=\"{{task.user.name}}\">
                                                <span class=\"kt-media kt-media--sm kt-media--brand\">
                                                    <span>{{task.user.name|capitalize|slice(0,2)}}</span>
                                                </span>
                                            </div>
                                       {% endif %}
                                    </div>
                                </div>
                            </div>
                        {% else %}
                            <div class=\"kt-todo__items\" data-type=\"task\">
                                <div class=\"kt-todo__item kt-todo__item--unread\" data-type=\"task\">
                                    <div class=\"kt-todo__details\" data-toggle=\"view\">
                                        <div class=\"kt-todo__message\">
                                            <span class=\"kt-todo__subject\">{{'No tasks found'|__}}.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                    {% if tasks %}
                        <div class=\"kt-todo__foot\">
                            <div class=\"kt-todo__toolbar\">
                                <div class=\"kt-todo__controls\">
                                     {{ tasks.render|raw }}
                                     <button class=\"kt-todo__icon\" data-toggle=\"kt-tooltip\" title=\"Previose page\">
                                         <i class=\"flaticon2-left-arrow\"></i>
                                     </button>

                                     <button class=\"kt-todo__icon\" data-toggle=\"kt-tooltip\" title=\"Next page\">
                                         <i class=\"flaticon2-right-arrow\"></i>
                                     </button>
                                </div>
                            </div>
                        </div>
                    {% endif %}
                </div>
            </div>
        </div>
    -->
</div>

<div class=\"modal fade\" id=\"assign_employee\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >{{'Assign Employee'|__}}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>
            {{ form_ajax('onAssign', { success: 'created successfully!', flash: true, class: 'kt_form' }) }}
                <div class=\"modal-body\">
                    {% if no_employees %}
                        <div class=\"alert alert-warning\" role=\"alert\">
                            <div class=\"alert-icon\"><i class=\"flaticon-warning\"></i></div>
                            <div class=\"alert-text\">
                                {{'No employees cover that shipment area, so here are all the employees to choose from'|__}}.
                            </div>
                            <div class=\"alert-close\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                    <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>
                                </button>
                            </div>
                        </div>
                    {% endif %}
                    <div class=\"row\">
                        <div class=\"form-group col-lg-12\">
                            <label>{{'To who?'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control \" name=\"assigned_id\" data-live-search=\"true\">
                                <option data-hidden=\"true\"></option>
                                {% for employee in employees %}
                                    <option value=\"{{employee.id}}\">{{employee.name}}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{'Close'|__}}</button>
                    <button type=\"submit\" class=\"btn btn-primary\">{{'Assign'|__}}</button>
                </div>
            {{ form_close() }}
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"assign_manifest\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >{{'Assign Manifest'|__}}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>
            {{ form_ajax('onManifest', { success: 'created successfully!', flash: true, class: 'kt_form' }) }}
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"form-group col-lg-12\">
                            <label>{{'To which manifest?'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control \" name=\"manifest_id\" data-live-search=\"true\">
                                <option data-hidden=\"true\"></option>
                                {% for manifest in manifestes %}
                                    <option value=\"{{manifest.id}}\">#{{manifest.id}} {{manifest.driver.name}}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{'Close'|__}}</button>
                    <button type=\"submit\" class=\"btn btn-primary\">{{'Assign'|__}}</button>
                </div>
            {{ form_close() }}
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"transfer_office\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >{{'Transfer to office'|__}}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>
            {{ form_ajax('onTransfer', { success: 'created successfully!', flash: true, class: 'kt_form' }) }}
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"form-group col-lg-12\">
                            <label>{{'To which office?'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control \" name=\"office_id\" data-live-search=\"true\">
                                <option data-hidden=\"true\"></option>
                                {% for office in offices %}
                                    <option value=\"{{office.id}}\">{{office.name}}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{'Close'|__}}</button>
                    <button type=\"submit\" class=\"btn btn-primary\">{{'Transfer'|__}}</button>
                </div>
            {{ form_close() }}
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"postpone\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >{{'Postpone'|__}}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>
            {{ form_ajax('onPostpone', { success: 'created successfully!', flash: true, class: 'kt_form' }) }}
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"form-group col-lg-12\">
                        <label>{{'To Date'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                        <div class=\"input-group date\">
                            <input type=\"text\" class=\"form-control date\" readonly=\"\" name=\"ship_date\" value=\"{{now|date_modify(\"+1 day\")|date(settings.dateformat)}}\" required>
                            <div class=\"input-group-append\">
                                <span class=\"input-group-text\">
                                    <i class=\"la la-calendar\"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"form-group col-lg-12\">
                        <label>{{'Reason'|__}}</label>
                        <textarea class=\"form-control\" name=\"message\"></textarea>
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{'Close'|__}}</button>
                <button type=\"submit\" class=\"btn btn-primary\">{{'Postpone'|__}}</button>
            </div>
            {{ form_close() }}
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"refuse\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >{{'Refuse'|__}}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>

            {{ form_ajax('onRefuse', { success: 'created successfully!', flash: true, class: 'kt_form' }) }}
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"form-group col-lg-12\">
                        <label>{{'Reason'|__}}</label>
                        <textarea class=\"form-control\" name=\"message\"></textarea>
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{'Close'|__}}</button>
                <button type=\"submit\" class=\"btn btn-primary\">{{'Refuse'|__}}</button>
            </div>
            {{ form_close() }}
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"discards\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >{{'Transfer as discards'|__}}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>

            {{ form_ajax('onDiscards', { success: 'created successfully!', flash: true, class: 'kt_form' }) }}
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"form-group col-lg-12\">
                        <label>{{'Reason'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                        <select class=\"form-control \" id=\"discard_reason\" name=\"message\" data-live-search=\"true\" required>
                            <option data-hidden=\"true\"></option>
                            <option value=\"1\">{{'Receiver request'|__}}</option>
                            <option value=\"2\">{{'Receiver evade'|__}}</option>
                            <option value=\"3\">{{'Sender request'|__}}</option>
                        </select>
                    </div>
                </div>
                <div class=\"discard_reason_receiver kt-hidden\">
                    <div class=\"row\">
                        <div class=\"form-group form-group-last kt-hide\">
                            <div class=\"alert alert-danger kt_form_msg\" role=\"alert\">
                                <div class=\"alert-icon\"><i class=\"flaticon-warning\"></i></div>
                                <div class=\"alert-text\">
                                    {{'Oh snap! Change a few things up and try submitting again'|__}}.
                                </div>
                                <div class=\"alert-close\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                        <span aria-hidden=\"true\"><i class=\"la la-close\"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class=\"form-group col-lg-12\">
                            <label>{{'Return package fees responsiable'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"col-lg-9 col-xl-6\">
                                <div class=\"kt-radio-inline\">
                                    <label class=\"kt-radio\">
                                        <input type=\"radio\" name=\"return_package_fee\" class=\"return_package_fee\" value=\"1\" {% if order.return_package_fee == 1 %}checked{% endif %} required> {{'Receiver'|__}}
                                        <span></span>
                                    </label>
                                    <label class=\"kt-radio\">
                                        <input type=\"radio\" name=\"return_package_fee\" class=\"return_package_fee\" value=\"2\" {% if order.return_package_fee == 2 %}checked{% endif %} required> {{'Sender'|__}}
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-group row\">
                        {% for payment in order.payments.where('for_id', order.receiver_id) %}
                            <label class=\"col-lg-9 col-form-label kt-align-left\" for=\"payment_{{payment.id}}\">
                                {% if payment.payment_type %}{{payment.payment_type|__}}: {% endif %}{{'Is it paid ?'|__}}
                            </label>
                            <div class=\"col-lg-3\">
                                <div class=\"kt-checkbox-single\">
                                    <label class=\"kt-checkbox\">
                                        <input type=\"checkbox\" name=\"payments[]\" id=\"payment_{{payment.id}}\" value=\"{{payment.id}}\">
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{'Close'|__}}</button>
                <button type=\"submit\" class=\"btn btn-primary\">{{'Discard'|__}}</button>
            </div>
            {{ form_close() }}
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"received\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5 class=\"modal-title\" >{{'Receiver information'|__}}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                </button>
            </div>

            {{ form_ajax('onDeliver', { success: 'created successfully!', flash: true, class: 'kt_form', novalidate: true }) }}
                <div class=\"modal-body\">
                    <div class=\"row\">

                        <div class=\"form-group col-lg-12\">
                            <label>{{'Receiver'|__}}/{{'Client'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control clients col-lg-12\" name=\"receiver_id\" id=\"receiver_id\" required>
                                <option data-hidden=\"true\"></option>
                                {% if order.receiver %}
                                    <option value=\"{{order.receiver.id}}\" selected>{{order.receiver.name}}</option>
                                {% endif %}
                                {% if user.hasUserPermission('client', 'c') %}
                                    <option value=\"new\" data-icon=\"flaticon2-add\">{{'Add New'|__}}</option>
                                {% endif %}
                            </select>
                        </div>
                        <div class=\"form-group col-lg-12\">
                            <label>{{'Receiver Address'|__}}/{{'Client Address'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <select class=\"form-control receiver_address_id\" name=\"receiver_address_id\" id=\"receiver_address_id\" data-live-search=\"true\" title=\"{{'Please select receiver first'|__}}\" required>
                                <option data-hidden=\"true\"></option>
                                {% if order.receiver %}
                                    <option value=\"{{order.receiver_address.id}}\" selected>{{order.receiver_address.name}}</option>
                                {% endif %}
                            </select>
                        </div>
                        <div class=\"col-lg-12 kt-hidden\" id=\"addnewreceiver\">
                            <div class=\"kt-portlet kt-portlet--bordered kt-portlet--head--noborder kt-margin-b-0\">
                                <div class=\"kt-portlet__head\">
                                    <div class=\"kt-portlet__head-label\">
                                        <h3 class=\"kt-portlet__head-title\">
                                            {{'Add a new client'|__}}
                                        </h3>
                                    </div>
                                </div>
                                <div class=\"kt-portlet__body\">
                                    <div class=\"row\">
                                        <div class=\"form-group col-lg-12\">
                                            <label>{{'Client Name'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                            <input type=\"text\" class=\"form-control name\" name=\"receiver[name]\" required />
                                        </div>
                                        <div class=\"form-group col-lg-6\">
                                            <label>{{'Mobile'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                            <input type=\"text\" class=\"form-control mobile\" name=\"receiver[mobile]\" required />
                                        </div>
                                        <div class=\"form-group col-lg-6\">
                                            <label>{{'Gender'|__}}</label>
                                            <div class=\"kt-radio-inline\">
                                                <label class=\"kt-radio\">
                                                    <input type=\"radio\" name=\"receiver[gender]\" class=\"gender\" value=\"male\"> {{'Male'|__}}
                                                    <span></span>
                                                </label>
                                                <label class=\"kt-radio\">
                                                    <input type=\"radio\" name=\"receiver[gender]\" class=\"gender\" value=\"female\"> {{'Female'|__}}
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"location-receiver\">
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-12\">
                                                <label>{{'Address'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                <input type=\"text\" class=\"form-control address street_addr\" data-receiver=\"route\" name=\"receiver[street_address]\"  rel=\"receiver\" required/>
                                                <input type=\"hidden\" class=\"form-control lat\" data-receiver=\"lat\" name=\"receiver[lat]\" />
                                                <input type=\"hidden\" class=\"form-control lng\" data-receiver=\"lng\" name=\"receiver[lng]\" />
                                                <input type=\"hidden\" class=\"form-control url\" data-receiver=\"url\" name=\"receiver[url]\" />
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>{{'Country'|__}}</label>
                                                <select class=\"form-control country_id\" data-receiver=\"country\" data-live-search=\"true\" name=\"receiver[country]\">
                                                    <option data-hidden=\"true\"></option>
                                                    {% for country in countries %}
                                                        <option value=\"{{country.id}}\" {% if currentLang != 'en' %}data-subtext=\"{{country.lang(currentLang).name}}\"{% endif %}>{{country.lang('en').name}}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>{{'Postal Code'|__}}</label>
                                                <input class=\"form-control postal_code\" type=\"text\" data-receiver=\"postal_code\" name=\"receiver[postal_code]\" >
                                            </div>
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-6\">
                                                <label>{{'State / Region'|__}}</label>
                                                <select class=\"form-control state_id\" data-receiver=\"administrative_area_level_1\" title=\"{{'Please select country first'|__}}\" name=\"receiver[state]\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    {% for state in states %}
                                                        <option value=\"{{state.id}}\" {% if currentLang != 'en' %}data-subtext=\"{{state.lang(currentLang).name}}\"{% endif %}>{{state.lang('en').name}}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>{{'City'|__}}</label>
                                                <select class=\"form-control city_id\" data-receiver=\"locality\" name=\"receiver[city]\" title=\"{{'Please select state first'|__}}\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    {% for city in cities %}
                                                        <option value=\"{{city.id}}\">{{city.name}}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-12\">
                                                <label>{{'County'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                <select class=\"form-control area_id\" data-receiver=\"sublocality\" name=\"receiver[county]\" title=\"{{'Please select city first'|__}}\" data-live-search=\"true\" required>
                                                    <option data-hidden=\"true\"></option>
                                                    {% for county in areas %}
                                                        <option value=\"{{county.id}}\">{{county.name}}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-12\">
                                                <label>{{'Google Map'|__}}</label>
                                                <div class=\"col-sm-12 map_canvas map_receiver\"></div>
                                                <span class=\"form-text text-muted\">{{'Change the pin to select the right location'|__}}</span>
                                            </div>
                                        </div>
                                        <div class=\"form-group row\">
                                            <div class=\"col-lg-12\">
                                                <div class=\"kt-checkbox-single\">
                                                    <label class=\"kt-checkbox\">
                                                        <input type=\"checkbox\" name=\"connect\" class=\"connect\" value=\"receiver\"> {{'Connect client'|__}}: {{'create an account for the client'|__}}
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=\"row align-items-center kt-hidden\" id=\"connect_receiver\">
                                            <div class=\"col-md-12\">
                                                <div class=\"form-group kt-form__group--inline\">
                                                    <div class=\"kt-form__label\">
                                                        <label class=\"col-form-label\">{{'Email'|__}}:&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                    </div>
                                                    <div class=\"kt-form__control\">
                                                        <input type=\"text\" class=\"form-control email\" name=\"receiver[email]\" required/>
                                                    </div>
                                                </div>
                                                <div class=\"d-md-none kt-margin-b-10\"></div>
                                            </div>
                                            <div class=\"col-md-12\">
                                                <div class=\"form-group kt-form__group--inline\">
                                                    <div class=\"kt-form__label\">
                                                        <label class=\"col-form-label\">{{'Username'|__}}:&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                    </div>
                                                    <div class=\"kt-form__control\">
                                                        <input type=\"text\" class=\"form-control username\" name=\"receiver[username]\" required>
                                                    </div>
                                                </div>
                                                <div class=\"d-md-none kt-margin-b-10\"></div>
                                            </div>
                                            <div class=\"col-md-12\">
                                                <div class=\"form-group kt-form__group--inline\">
                                                    <div class=\"kt-form__label\">
                                                        <label class=\"col-form-label\">{{'Password'|__}}:&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                    </div>
                                                    <div class=\"kt-form__control\">
                                                        <input type=\"password\" class=\"form-control password\" name=\"receiver[password]\" required>
                                                    </div>
                                                </div>
                                                <div class=\"d-md-none kt-margin-b-10\"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"kt-portlet__foot\">
                                    <div class=\"row align-items-center\">
                                        <div class=\"col-lg-12\">
                                            <button type=\"button\" class=\"btn btn-success save\">{{'Save'|__}}</button>
                                            <button type=\"button\" class=\"btn btn-secondary cancel\">{{'Cancel'|__}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"col-lg-12 kt-hidden\" id=\"addnewreceiveraddress\">
                            <div class=\"kt-portlet kt-portlet--bordered kt-portlet--head--noborder kt-margin-b-0\">
                                <div class=\"kt-portlet__head\">
                                    <div class=\"kt-portlet__head-label\">
                                        <h3 class=\"kt-portlet__head-title\">
                                            {{'Add a new client address'|__}}
                                        </h3>
                                    </div>
                                </div>
                                <div class=\"kt-portlet__body\">
                                    <div class=\"location-receiveraddress\">
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-12\">
                                                <label>{{'Address'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                                                <input type=\"text\" class=\"form-control address street_addr\" data-receiveraddress=\"route\" name=\"receiveraddress[street_address]\"  rel=\"receiveraddress\" required/>
                                                <input type=\"hidden\" class=\"form-control lat\" data-receiveraddress=\"lat\" name=\"receiveraddress[lat]\" />
                                                <input type=\"hidden\" class=\"form-control lng\" data-receiveraddress=\"lng\" name=\"receiveraddress[lng]\" />
                                                <input type=\"hidden\" class=\"form-control url\" data-receiveraddress=\"url\" name=\"receiveraddress[url]\" />
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>{{'Country'|__}}</label>
                                                <select class=\"form-control country_id\" data-receiveraddress=\"country\" data-live-search=\"true\" name=\"receiveraddress[country]\">
                                                    <option data-hidden=\"true\"></option>
                                                    {% for country in countries %}
                                                        <option value=\"{{country.id}}\" {% if currentLang != 'en' %}data-subtext=\"{{country.lang(currentLang).name}}\"{% endif %}>{{country.lang('en').name}}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>{{'Postal Code'|__}}</label>
                                                <input class=\"form-control postal_code\" type=\"text\" data-sendreceiveraddresser=\"postal_code\" name=\"receiveraddress[postal_code]\" >
                                            </div>
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-6\">
                                                <label>{{'State / Region'|__}}</label>
                                                <select class=\"form-control state_id\" data-receiveraddress=\"administrative_area_level_1\" title=\"{{'Please select country first'|__}}\" name=\"receiveraddress[state]\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    {% for state in states %}
                                                        <option value=\"{{state.id}}\" {% if currentLang != 'en' %}data-subtext=\"{{state.lang(currentLang).name}}\"{% endif %}>{{state.lang('en').name}}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-6\">
                                                <label>{{'City'|__}}</label>
                                                <select class=\"form-control city_id\" data-receiveraddress=\"locality\" name=\"receiveraddress[city]\" title=\"{{'Please select state first'|__}}\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    {% for city in cities %}
                                                        <option value=\"{{city.id}}\">{{city.name}}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                            <div class=\"form-group col-lg-12\">
                                                <label>{{'County'|__}}</label>
                                                <select class=\"form-control area_id\" data-receiveraddress=\"sublocality\" name=\"receiveraddress[county]\" title=\"{{'Please select city first'|__}}\" data-live-search=\"true\">
                                                    <option data-hidden=\"true\"></option>
                                                    {% for county in areas %}
                                                        <option value=\"{{county.id}}\" {% if currentLang != 'en' %}data-subtext=\"{{county.lang(currentLang).name}}\"{% endif %}>{{county.lang('en').name}}</option>
                                                    {% endfor %}
                                                </select>
                                            </div>
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-lg-12\">
                                                <label>{{'Google Map'|__}}</label>
                                                <div class=\"col-sm-12 map_canvas map_receiveraddress\"></div>
                                                <span class=\"form-text text-muted\">{{'Change the pin to select the right location'|__}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"kt-portlet__foot\">
                                    <div class=\"row align-items-center\">
                                        <div class=\"col-lg-12\">
                                            <button type=\"button\" class=\"btn btn-success save\">{{'Save'|__}}</button>
                                            <button type=\"button\" class=\"btn btn-secondary cancel\">{{'Cancel'|__}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit\"></div>
                        <div class=\"form-group col-lg-12 {% if order.return_defray != 1 %}kt-hidden{% endif %} package_fee\">
                            <label>{{'Courier Cost'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"input-group\">
                                {% if primary_currency.place_symbol_before == 1 %}
                                    <div class=\"input-group-prepend\">
                                        <span class=\"input-group-text\">
                                            {{primary_currency.currency_symbol}}
                                        </span>
                                    </div>
                                {% endif %}
                                    <input type=\"text\" class=\"form-control decimal\" data-type='currency' id=\"courier_fee\" name=\"courier_fee\" value=\"{{order.courier_fee}}\" required />
                                {% if primary_currency.place_symbol_before == 0 %}
                                    <div class=\"input-group-append\">
                                        <span class=\"input-group-text\">
                                            {{primary_currency.currency_symbol}}
                                        </span>
                                    </div>
                                {% endif %}
                            </div>
                        </div>
                        <div class=\"form-group col-lg-12\">
                            <label>{{'Return package cost'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"kt-radio-inline\">
                                <label class=\"kt-radio\">
                                    <input type=\"radio\" name=\"return_defray\" class=\"return_defray\" value=\"1\" {% if order.return_defray == 1 %}checked{% endif %} required> {{'Yes'|__}}
                                    <span></span>
                                </label>
                                <label class=\"kt-radio\">
                                    <input type=\"radio\" name=\"return_defray\" class=\"return_defray\" value=\"2\" {% if order.return_defray == 2 %}checked{% endif %}  required> {{'No'|__}}
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class=\"form-group col-lg-12 {% if order.return_defray != 1 %}kt-hidden{% endif %} package_fee\">
                            <label>{{'Package Cost'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"input-group\">
                                {% if primary_currency.place_symbol_before == 1 %}
                                    <div class=\"input-group-prepend\">
                                        <span class=\"input-group-text\">
                                            {{primary_currency.currency_symbol}}
                                        </span>
                                    </div>
                                {% endif %}
                                    <input type=\"text\" class=\"form-control decimal\" data-type='currency' id=\"package_fee\" name=\"package_fee\" value=\"{{order.package_fee}}\" required />
                                {% if primary_currency.place_symbol_before == 0 %}
                                    <div class=\"input-group-append\">
                                        <span class=\"input-group-text\">
                                            {{primary_currency.currency_symbol}}
                                        </span>
                                    </div>
                                {% endif %}
                            </div>
                        </div>
                        <div class=\"form-group col-lg-12 {% if order.return_defray != 1 %}kt-hidden{% endif %} package_fee\">
                            <label>{{'Return Shipment Cost'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"input-group\">
                                {% if primary_currency.place_symbol_before == 1 %}
                                    <div class=\"input-group-prepend\">
                                        <span class=\"input-group-text\">
                                            {{primary_currency.currency_symbol}}
                                        </span>
                                    </div>
                                {% endif %}
                                    <input type=\"text\" class=\"form-control decimal\" data-type='currency' name=\"return_courier_fee\" id=\"return_courier_fee\" value=\"{% if order.package_fee %}{{order.return_courier_fee}}{% else %}{{settings.fees.delivery_cost_back_receiver}}{% endif %}\" required />
                                {% if primary_currency.place_symbol_before == 0 %}
                                    <div class=\"input-group-append\">
                                        <span class=\"input-group-text\">
                                            {{primary_currency.currency_symbol}}
                                        </span>
                                    </div>
                                {% endif %}
                            </div>
                        </div>
                        <div class=\"form-group col-lg-12 {% if order.return_defray != 1 %}kt-hidden{% endif %} package_fee\">
                            <label>{{'Return package fees responsiable'|__}}&nbsp;<span class=\"kt-badge kt-badge--danger kt-badge--dot\"></span></label>
                            <div class=\"kt-radio-inline\">
                                <label class=\"kt-radio\">
                                    <input type=\"radio\" name=\"return_package_fee\" class=\"return_package_fee\" value=\"1\" {% if order.return_package_fee != 2 %}checked{% endif %} required> {{'Receiver'|__}}
                                    <span></span>
                                </label>
                                <label class=\"kt-radio\">
                                    <input type=\"radio\" name=\"return_package_fee\" class=\"return_package_fee\" value=\"2\" required {% if order.return_package_fee == 2 %}checked{% endif %}> {{'Sender'|__}}
                                    <span></span>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{{'Close'|__}}</button>
                    <button type=\"button\" class=\"btn btn-info save\">{{'Confirm'|__}}</button>
                    <button type=\"submit\" class=\"btn btn-primary kt-hidden\">{{'Deliver'|__}}</button>
                </div>
            {{ form_close() }}
        </div>
    </div>
</div>
<!--
{% put styles %}
    <style>
        .kt-todo .kt-todo__content {
          width: 100%; }

        .kt-todo .kt-todo__aside {
          padding: 20px;
          width: 275px; }
          @media (min-width: 1025px) {
            .kt-todo .kt-todo__aside {
              margin-right: 25px; } }
          .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item {
            margin-bottom: 0.5rem; }
            .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link-title {
              padding-left: 1.9rem;
              font-weight: 600;
              color: #48465b;
              font-size: 1.1rem;
              padding-left: 1.9rem;
              font-weight: 600;
              color: #48465b;
              font-size: 1.1rem; }
            .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link {
              padding: 0.6rem 20px;
              border-radius: 4px; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link i {
                color: #8e96b8; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease;
                fill: #8e96b8; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link:hover g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-icon {
                text-align: center;
                margin: 0 0.7rem 0 -0.23rem; }
                .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-icon.kt-nav__link-icon--size {
                  font-size: 0.9rem;
                  color: #93a2dd; }
                .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-icon svg {
                  width: 20px;
                  height: 20px; }
                .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-icon g [fill] {
                  fill: #93a2dd; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link .kt-nav__link-text {
                font-weight: 500;
                color: #595d6e; }
            .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item:last-child {
              margin-bottom: 0; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item:last-child .kt-nav__link .kt-nav__link-icon {
                font-size: 0.9rem; }
            .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link {
              background-color: #f7f8fa;
              border-radius: 4px; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link i, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link i, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link i {
                color: #5d78ff; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link g [fill], .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link g [fill], .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease;
                fill: #5d78ff; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link:hover g [fill], .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link:hover g [fill], .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link:hover g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.hover .kt-nav__link .kt-nav__link-text, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--selected .kt-nav__link .kt-nav__link-text, .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item.kt-nav__item--active .kt-nav__link .kt-nav__link-text {
                color: #5d78ff; }

        .kt-todo .kt-todo__header {
          padding: 13px 25px;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -ms-flex-wrap: wrap;
          flex-wrap: wrap;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          width: 100%; }
          .kt-todo .kt-todo__header .kt-todo__title {
            margin: 0;
            font-size: 1.4rem;
            padding-right: 2rem;
            font-weight: 600;
            color: #595d6e; }
          .kt-todo .kt-todo__header .kt-todo__nav {
            padding: 0;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            margin-top: 0.2rem; }
            .kt-todo .kt-todo__header .kt-todo__nav .btn {
              margin-right: 1rem; }
            .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link {
              padding: 0.5rem 1.2rem;
              font-weight: 500;
              color: #74788d;
              border-radius: 4px; }
              .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link:not(:first-child):not(:last-child) {
                margin: 0 0.2rem; }
              .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link:hover, .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link.kt-todo__link--selected, .kt-todo .kt-todo__header .kt-todo__nav .kt-todo__link.kt-todo__link--active {
                background-color: #f7f8fa;
                color: #5d78ff;
                border-radius: 4px; }
          .kt-todo .kt-todo__header .kt-todo__users {
            line-height: 0;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center; }
            .kt-todo .kt-todo__header .kt-todo__users .kt-media {
              position: relative;
              z-index: 0; }
              .kt-todo .kt-todo__header .kt-todo__users .kt-media:not(:first-child):not(:last-child) {
                margin-left: -0.1rem; }
          @media (max-width: 1024px) {
            .kt-todo .kt-todo__header {
              padding: 10px 15px; } }

        .kt-todo .kt-todo__icon {
          border: 0;
          background: none;
          outline: none !important;
          -webkit-box-shadow: none;
          box-shadow: none;
          outline: none !important;
          display: -webkit-box;
          display: -ms-flexbox;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          align-items: center;
          height: 35px;
          width: 35px;
          background-color: #f7f8fa;
          -webkit-transition: all 0.3s ease;
          transition: all 0.3s ease;
          cursor: pointer;
          margin: 0;
          border-radius: 0;
          border-radius: 4px; }
          .kt-todo .kt-todo__icon i {
            font-size: 1.1rem; }
          .kt-todo .kt-todo__icon.kt-todo__icon--sm {
            height: 26px;
            width: 26px; }
            .kt-todo .kt-todo__icon.kt-todo__icon--sm i {
              font-size: 0.8rem; }
          .kt-todo .kt-todo__icon.kt-todo__icon--md {
            height: 30px;
            width: 30px; }
            .kt-todo .kt-todo__icon.kt-todo__icon--md i {
              font-size: 1rem; }
          .kt-todo .kt-todo__icon.kt-todo__icon--light {
            background-color: transparent; }
          .kt-todo .kt-todo__icon i {
            color: #8e96b8; }
          .kt-todo .kt-todo__icon g [fill] {
            -webkit-transition: fill 0.3s ease;
            transition: fill 0.3s ease;
            fill: #8e96b8; }
          .kt-todo .kt-todo__icon:hover g [fill] {
            -webkit-transition: fill 0.3s ease;
            transition: fill 0.3s ease; }
          .kt-todo .kt-todo__icon.kt-todo__icon--active, .kt-todo .kt-todo__icon:hover {
            -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
            background-color: #ebedf2; }
            .kt-todo .kt-todo__icon.kt-todo__icon--active.kt-todo__icon--light, .kt-todo .kt-todo__icon:hover.kt-todo__icon--light {
              background-color: transparent; }
            .kt-todo .kt-todo__icon.kt-todo__icon--active i, .kt-todo .kt-todo__icon:hover i {
              color: #5d78ff; }
            .kt-todo .kt-todo__icon.kt-todo__icon--active g [fill], .kt-todo .kt-todo__icon:hover g [fill] {
              -webkit-transition: fill 0.3s ease;
              transition: fill 0.3s ease;
              fill: #5d78ff; }
            .kt-todo .kt-todo__icon.kt-todo__icon--active:hover g [fill], .kt-todo .kt-todo__icon:hover:hover g [fill] {
              -webkit-transition: fill 0.3s ease;
              transition: fill 0.3s ease; }
          .kt-todo .kt-todo__icon.kt-todo__icon--back {
            background-color: transparent; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back i {
              color: #8e96b8;
              font-size: 1.5rem; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back g [fill] {
              -webkit-transition: fill 0.3s ease;
              transition: fill 0.3s ease;
              fill: #8e96b8; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back:hover g [fill] {
              -webkit-transition: fill 0.3s ease;
              transition: fill 0.3s ease; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back svg {
              height: 32px;
              width: 32px; }
            .kt-todo .kt-todo__icon.kt-todo__icon--back:hover {
              background-color: transparent; }
              .kt-todo .kt-todo__icon.kt-todo__icon--back:hover i {
                color: #5d78ff; }
              .kt-todo .kt-todo__icon.kt-todo__icon--back:hover g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease;
                fill: #5d78ff; }
              .kt-todo .kt-todo__icon.kt-todo__icon--back:hover:hover g [fill] {
                -webkit-transition: fill 0.3s ease;
                transition: fill 0.3s ease; }

        .kt-todo .kt-todo__list {
          display: -webkit-box !important;
          display: -ms-flexbox !important;
          display: flex !important;
          padding: 0; }
          .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar {
            position: relative;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding: 0 25px; }
            .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -webkit-box-align: center;
              -ms-flex-align: center;
              align-items: center;
              margin-right: 1rem; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__check {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__check .kt-checkbox {
                  margin: 0;
                  padding-left: 0; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__check .kt-todo__icon {
                  margin-left: 0; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__panel {
                display: none;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .btn {
                padding: 0.6rem 1rem; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions.kt-todo__actions--expanded .kt-todo__panel {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__icon {
                margin-right: 0.5rem; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__icon.kt-todo__icon--back {
                  margin-right: 2.5rem; }
            .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -webkit-box-align: center;
              -ms-flex-align: center;
              align-items: center;
              margin: 0.4rem 0; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls .kt-todo__icon {
                margin-left: 0.5rem; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls .kt-todo__sort {
                margin-left: 0.5rem; }
              .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls .kt-todo__pages .kt-todo__perpage {
                color: #74788d;
                font-size: 1rem;
                font-weight: 500;
                margin-right: 1rem;
                cursor: pointer;
                -webkit-transition: all 0.3s ease;
                transition: all 0.3s ease;
                padding: 0.5rem 0; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__controls .kt-todo__pages .kt-todo__perpage:hover {
                  -webkit-transition: all 0.3s ease;
                  transition: all 0.3s ease;
                  color: #5d78ff; }
            .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar .kt-todo__sep {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              height: 1rem;
              width: 1rem; }
          .kt-todo .kt-todo__list .kt-todo__body {
            padding: 20px 0 0 0; }
            .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items {
              padding: 0;
              margin-bottom: 15px; }
              .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: start;
                -ms-flex-align: start;
                align-items: flex-start;
                padding: 12px 25px;
                -webkit-transition: all 0.3s ease;
                transition: all 0.3s ease;
                cursor: pointer; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex; }
                  @media screen\\0 {
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info {
                      padding: 8px 0; } }
                  @media screen\\0 {
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info {
                      min-width: 210px; } }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    margin: 0.3rem 10px 0 0;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center; }
                    @media screen\\0 {
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions {
                        min-width: 70px; } }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-checkbox {
                      margin: 0;
                      padding: 0;
                      margin-right: 10px; }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-todo__icon {
                      height: 22px;
                      width: 22px; }
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-todo__icon i {
                        font-size: 1rem;
                        color: #ebedf2; }
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-todo__icon:hover i {
                        color: rgba(255, 184, 34, 0.5) !important; }
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__actions .kt-todo__icon.kt-todo__icon--on i {
                        color: #ffb822 !important; }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center;
                    margin-right: 30px;
                    width: 175px; }
                    @media screen\\0 {
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender {
                        min-width: 175px; } }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-media {
                      margin-right: 10px;
                      min-width: 30px !important; }
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-media span {
                        min-width: 30px !important; }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-todo__author {
                      font-size: 1rem;
                      color: #595d6e;
                      font-weight: 500; }
                    @media (max-width: 1400px) {
                      .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender {
                        display: block;
                        width: 70px;
                        margin-right: 10px; }
                        .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-todo__author {
                          display: block; }
                        .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__info .kt-todo__sender .kt-media {
                          margin-bottom: 5px; } }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details {
                  -webkit-box-flex: 1;
                  -ms-flex-positive: 1;
                  flex-grow: 1;
                  margin-top: 5px; }
                  @media screen\\0 {
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details {
                      width: 0;
                      height: 0; } }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details .kt-todo__message .kt-todo__subject {
                    font-size: 1rem;
                    color: #595d6e;
                    font-weight: 500; }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details .kt-todo__message .kt-todo__summary {
                    font-size: 1rem;
                    color: #a2a5b9;
                    font-weight: 400;
                    text-overflow: ellipsis; }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details .kt-todo__labels {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    margin-top: 5px; }
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__details .kt-todo__labels .kt-todo__label {
                      margin-right: 5px; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__datetime {
                  font-size: 1rem;
                  color: #a2a5b9;
                  font-weight: 400;
                  margin-right: 1rem;
                  margin-top: 5px;
                  width: 85px;
                  text-align: right; }
                  @media (max-width: 1400px) {
                    .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item .kt-todo__datetime {
                      width: 70px; } }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item:hover, .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--selected {
                  -webkit-transition: all 0.3s ease;
                  transition: all 0.3s ease;
                  background-color: #f7f8fa; }
                  .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item:hover .kt-todo__info .kt-todo__actions .kt-todo__icon i, .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--selected .kt-todo__info .kt-todo__actions .kt-todo__icon i {
                    font-size: 1rem;
                    color: #e2e5ec; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--unread .kt-todo__sender .kt-todo__author {
                  color: #48465b;
                  font-weight: 600; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--unread .kt-todo__details .kt-todo__message .kt-todo__subject {
                  color: #595d6e;
                  font-weight: 600; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item.kt-todo__item--unread .kt-todo__datetime {
                  color: #48465b;
                  font-weight: 600; }
          .kt-todo .kt-todo__list .kt-todo__foot {
            margin-top: 10px;
            padding: 0 25px; }
            .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar {
              float: right; }
              .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                margin-left: 1rem; }
                .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__icon {
                  margin-left: 0.5rem; }
                .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__sort {
                  margin-left: 0.5rem; }
                .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__pages {
                  margin-left: 0.5rem; }
                  .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__pages .kt-todo__perpage {
                    color: #74788d;
                    font-size: 1rem;
                    font-weight: 500;
                    margin-right: 1rem;
                    cursor: pointer;
                    -webkit-transition: all 0.3s ease;
                    transition: all 0.3s ease;
                    padding: 0.5rem 0; }
                    .kt-todo .kt-todo__list .kt-todo__foot .kt-todo__toolbar .kt-todo__controls .kt-todo__pages .kt-todo__perpage:hover {
                      -webkit-transition: all 0.3s ease;
                      transition: all 0.3s ease;
                      color: #5d78ff; }

        .kt-todo .kt-todo__view {
          padding: 0;
          display: -webkit-box !important;
          display: -ms-flexbox !important;
          display: flex !important; }
          .kt-todo  {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 100%;
            padding-bottom: 25px; }
            @media (max-width: 1024px) {
              .kt-todo  {
                padding-bottom: 15px; } }
            .kt-todo  .kt-todo__head {
              width: 100%; }
              .kt-todo  .kt-todo__head .kt-todo__toolbar {
                cursor: pointer;
                padding-top: 0.9rem;
                width: 100%;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap; }
                .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__icon.kt-todo__icon--back {
                  display: none; }
                .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__info {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  -webkit-box-flex: 1;
                  -ms-flex-positive: 1;
                  flex-grow: 1; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__info .kt-media {
                    margin-right: 0.7rem; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__info .kt-todo__name {
                    color: #48465b;
                    font-weight: 600;
                    font-size: 1.1rem;
                    padding-right: 0.5rem; }
                    .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__info .kt-todo__name:hover {
                      color: #5d78ff;
                      -webkit-transition: all 0.3s ease;
                      transition: all 0.3s ease; }
                .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  -ms-flex-wrap: wrap;
                  flex-wrap: wrap;
                  padding: 0.5rem 0; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details .kt-todo__icon {
                    margin: 0.4rem 0.5rem 0.4rem 0;
                    font-size: 0.7rem;
                    color: #5d78ff; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details .kt-todo__desc {
                    color: #74788d;
                    font-weight: 400;
                    font-size: 1rem; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details .btn {
                    padding: 8px 1rem; }
                    .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__details .btn:last-child {
                      margin-left: 0.5rem; }
                .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__actions {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center; }
                  .kt-todo  .kt-todo__head .kt-todo__toolbar .kt-todo__actions .kt-todo__datetime {
                    color: #a2a5b9;
                    font-weight: 500;
                    font-size: 1rem;
                    margin-right: 1.5rem; }
            .kt-todo  .kt-todo__body {
              padding-bottom: 25px;
              -webkit-box-flex: 1;
              -ms-flex-positive: 1;
              flex-grow: 1; }
              .kt-todo  .kt-todo__body .kt-todo__title {
                padding-top: 1rem; }
                .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__text {
                  color: #48465b;
                  font-size: 1.5rem;
                  font-weight: 600;
                  margin-top: 1rem;
                  display: block; }
                .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  padding: 0.8rem 0 2rem 0; }
                  .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels .kt-todo__label {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center; }
                    .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels .kt-todo__label .kt-todo__dot {
                      padding-right: 0.5rem;
                      font-size: 1.2rem; }
                    .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels .kt-todo__label .kt-todo__text {
                      color: #a2a5b9;
                      font-weight: 500;
                      font-size: 1rem;
                      margin: 0; }
                    .kt-todo  .kt-todo__body .kt-todo__title .kt-todo__labels .kt-todo__label:last-child {
                      padding-left: 1.5rem; }
                .kt-todo  .kt-todo__body .kt-todo__title:hover {
                  color: #5d78ff;
                  -webkit-transition: all 0.3s ease;
                  transition: all 0.3s ease; }
              .kt-todo  .kt-todo__body .kt-todo__desc {
                padding-bottom: 10px;
                display: block;
                color: #a2a5b9;
                font-weight: 500; }
              .kt-todo  .kt-todo__body .kt-todo__files {
                padding-top: 10px; }
                .kt-todo  .kt-todo__body .kt-todo__files .kt-todo__file {
                  display: block;
                  padding-top: 0.7rem; }
                  .kt-todo  .kt-todo__body .kt-todo__files .kt-todo__file i {
                    padding-right: 0.5rem; }
                  .kt-todo  .kt-todo__body .kt-todo__files .kt-todo__file a {
                    color: #74788d;
                    font-weight: 500; }
                    .kt-todo  .kt-todo__body .kt-todo__files .kt-todo__file a:hover {
                      color: #5d78ff;
                      -webkit-transition: all 0.3s ease;
                      transition: all 0.3s ease; }
              .kt-todo  .kt-todo__body .kt-todo__comments {
                padding-bottom: 20px; }
                .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment {
                  padding-top: 3rem; }
                  .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment:last-child {
                    padding-top: 2rem; }
                  .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center; }
                    .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box .kt-media {
                      margin-right: 1rem; }
                    .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box .kt-todo__username {
                      -webkit-box-flex: 1;
                      -ms-flex-positive: 1;
                      flex-grow: 1;
                      color: #595d6e;
                      font-weight: 500; }
                      .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box .kt-todo__username:hover {
                        color: #5d78ff;
                        -webkit-transition: all 0.3s ease;
                        transition: all 0.3s ease; }
                    .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__box .kt-todo__date {
                      color: #a2a5b9;
                      font-weight: 500; }
                  .kt-todo  .kt-todo__body .kt-todo__comments .kt-todo__comment .kt-todo__text {
                    padding-left: 3.3rem;
                    display: block;
                    color: #a2a5b9;
                    font-weight: 500; }
            .kt-todo  .kt-todo__foot .kt-todo__form {
              margin-top: 20px;
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-direction: column;
              flex-direction: column;
              border: 1px solid #ebedf2;
              border-radius: 4px; }
              .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__head {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                padding: 20px 15px 20px 25px;
                border-bottom: 1px solid #ebedf2; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__head .kt-todo__title {
                  margin-right: 10px;
                  font-size: 1.2rem;
                  font-weight: 500;
                  color: #595d6e; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__head .kt-todo__actions {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__head .kt-todo__actions .kt-todo__icon {
                    margin-left: 5px; }
              .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body {
                padding: 0 0 10px 0; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-container.ql-snow {
                  border: 0;
                  padding: 0;
                  border-radius: 0; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-container.ql-snow .ql-editor {
                    font-weight: 400;
                    font-size: 1rem;
                    color: #74788d;
                    padding: 15px 25px;
                    font-family: Poppins, Helvetica, sans-serif; }
                    .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-container.ql-snow .ql-editor.ql-blank:before {
                      left: 25px;
                      color: #a2a5b9;
                      font-weight: 400;
                      font-style: normal; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-toolbar.ql-snow {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  height: 50px;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  border-radius: 0;
                  border: 0;
                  border-top: 0;
                  border-bottom: 1px solid #ebedf2;
                  padding-left: 18px; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-toolbar.ql-snow .ql-picker-label, .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .ql-toolbar.ql-snow .ql-picker-label:before {
                    font-weight: 400;
                    font-size: 1rem;
                    color: #74788d;
                    font-family: Poppins, Helvetica, sans-serif; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .kt-todo__attachments {
                  display: inline-block;
                  padding: 0 25px; }
                  @media (max-width: 1024px) {
                    .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__body .kt-todo__attachments {
                      width: 100%; } }
              .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                width: 100%;
                padding: 20px 15px 20px 25px; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__primary {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-flex: 1;
                  -ms-flex-positive: 1;
                  flex-grow: 1;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__primary .btn-group .btn:nth-child(1) {
                    padding-left: 20px;
                    padding-right: 20px; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__primary .btn-group .btn:nth-child(2) {
                    padding-left: 6px;
                    padding-right: 9px; }
                  .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__primary .kt-todo__panel {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-box-align: center;
                    -ms-flex-align: center;
                    align-items: center;
                    margin-left: 1rem; }
                .kt-todo  .kt-todo__foot .kt-todo__form .kt-todo__foot .kt-todo__secondary {
                  display: -webkit-box;
                  display: -ms-flexbox;
                  display: flex;
                  -webkit-box-align: center;
                  -ms-flex-align: center;
                  align-items: center;
                  margin: 0; }

        .kt-todo .kt-portlet__head {
          min-height: 80px !important;
          padding: 10px 25px; }

        @media (max-width: 1024px) {
          .kt-todo {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding: 0; }
            .kt-todo .kt-todo__aside {
              background: #fff;
              margin: 0; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link-title {
                padding-left: 1.2rem; }
              .kt-todo .kt-todo__aside .kt-todo__nav .kt-nav .kt-nav__item .kt-nav__link {
                padding: 0.75rem 10px; }
            .kt-todo .kt-todo__list {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex; }
              .kt-todo .kt-todo__list.kt-todo__list--hidden {
                display: none; }
              .kt-todo .kt-todo__list .kt-portlet__head {
                min-height: 60px !important;
                padding: 10px 15px; }
              .kt-todo .kt-todo__list .kt-todo__head {
                padding: 0; }
                .kt-todo .kt-todo__list .kt-todo__head .kt-todo__toolbar {
                  padding: 10px 15px; }
              .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items {
                overflow: auto;
                margin-bottom: 15px; }
                .kt-todo .kt-todo__list .kt-todo__body .kt-todo__items .kt-todo__item {
                  min-width: 500px;
                  padding: 10px 15px; }
              .kt-todo .kt-todo__list .kt-todo__foot {
                padding: 0 15px; }
            .kt-todo .kt-todo__view {
              display: none; }
              .kt-todo .kt-todo__view .kt-todo__head .kt-todo__toolbar .kt-todo__icon.kt-todo__icon--back {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex; }
              .kt-todo .kt-todo__view.kt-todo__view--shown {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex; }
            .kt-todo .kt-portlet__head {
              min-height: 60px !important;
              padding: 10px 15px; } }

        @media (max-width: 768px) {
          .kt-todo .kt-todo__head .kt-todo__nav .btn {
            margin-right: 0.2rem; } }

        .kt-todo__aside-close {
          display: none; }

        @media (max-width: 1024px) {
          .kt-todo__aside {
            z-index: 1001;
            position: fixed;
            -webkit-overflow-scrolling: touch;
            top: 0;
            bottom: 0;
            overflow-y: auto;
            -webkit-transform: translate3d(0, 0, 0);
            backface-visibility: hidden;
            -webkit-backface-visibility: hidden;
            width: 300px !important;
            -webkit-transition: left 0.3s ease, right 0.3s ease;
            transition: left 0.3s ease, right 0.3s ease;
            left: -320px; }
            .kt-todo__aside.kt-todo__aside--on {
              -webkit-transition: left 0.3s ease, right 0.3s ease;
              transition: left 0.3s ease, right 0.3s ease;
              left: 0; } }
          @media screen\\0  and (max-width: 1024px) {
            .kt-todo__aside {
              -webkit-transition: none !important;
              transition: none !important; } }

        @media (max-width: 1024px) {
          .kt-todo__aside--right .kt-todo__aside {
            right: -320px;
            left: auto; }
            .kt-todo__aside--right .kt-todo__aside.kt-todo__aside--on {
              -webkit-transition: left 0.3s ease, right 0.3s ease;
              transition: left 0.3s ease, right 0.3s ease;
              right: 0;
              left: auto; }
          .kt-todo__aside-close {
            width: 25px;
            height: 25px;
            top: 1px;
            z-index: 1002;
            -webkit-transition: left 0.3s ease, right 0.3s ease;
            transition: left 0.3s ease, right 0.3s ease;
            position: fixed;
            border: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 3px;
            cursor: pointer;
            outline: none !important;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            left: -25px; } }
          @media screen\\0  and (max-width: 1024px) {
            .kt-todo__aside-close {
              -webkit-transition: none !important;
              transition: none !important; } }

        @media (max-width: 1024px) {
            .kt-todo__aside-close > i {
              line-height: 0;
              font-size: 1.4rem; }
            .kt-todo__aside-close:hover {
              text-decoration: none; }
            .kt-todo__aside--right .kt-todo__aside-close {
              left: auto;
              right: -25px; }
            .kt-todo__aside--on .kt-todo__aside-close {
              -webkit-transition: left 0.3s ease, right 0.3s ease;
              transition: left 0.3s ease, right 0.3s ease;
              left: 274px; }
            .kt-todo__aside--on.kt-todo__aside--right .kt-todo__aside-close {
              left: auto;
              right: 274px; }
          .kt-todo__aside-overlay {
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            overflow: hidden;
            z-index: 1000;
            background: rgba(0, 0, 0, 0.1);
            -webkit-animation: kt-animate-fade-in .3s linear 1;
            animation: kt-animate-fade-in .3s linear 1; }
          .kt-todo__aside-overlay {
            background: rgba(0, 0, 0, 0.05); }
          .kt-todo__aside-close {
            background-color: #f7f8fa; }
            .kt-todo__aside-close > i {
              color: #74788d; }
            .kt-todo__aside-close:hover {
              background-color: transparent; }
              .kt-todo__aside-close:hover > i {
                color: #5d78ff; } }

        @media (max-width: 350px) {
          .kt-todo__aside {
            width: 90% !important; } }
    </style>
{% endput %}
-->
{% put styles %}
    <style>
        .table-bordered tr td:first-child {
            width: 200px;
            font-weight: bold;
        }
        .map_canvas {
          height: 350px;
        }
        .filter-option-inner br {
            display: none;
        }
        .select2 {
            width: 100% !important;
        }
\t\t.select2-selection {
\t\t\tmin-height: 36px !important;
\t\t}
        .pac-container {
            z-index: 9999;
        }
        @media (max-width: 576px) {
            .kt-widget.kt-widget--user-profile-3 .kt-widget__top .kt-widget__content .kt-widget__info .kt-widget__stats .kt-widget__item {
                width: 100%;
                margin: auto 0 !important;
            }
        }
        .notes_scroll,.notes_scroll .btn {
            cursor: pointer;
        }
    </style>
{% endput %}
{% put scripts %}
<script src=\"{{ 'assets/admin/vendors/custom/geocomplete/jquery.geocomplete.js'|theme }}\" type=\"text/javascript\"></script>
<script src=\"https://maps.googleapis.com/maps/api/js?libraries=places&key={{settings.google.map}}\"></script>
<script src=\"./admin/vendors/custom/gmaps/gmaps.js\" type=\"text/javascript\"></script>
<script type=\"text/javascript\">
\"use strict\";
{% if settings.payment.enable == 1 %}
    {% if 'paystack' in settings.payment.gateways %}
        function payWithPaystack(){
            {% if paystack_amount %}
                var handler = PaystackPop.setup({
                    key: '{{settings.payment.paystack_key}}',
                    email: '{{user.email}}',
                    amount: {{paystack_amount*100}},
                    //amount: \"{{sender_fees}}\",
                    //currency: \"{{primary_currency.currency_code}}\",
                    currency: \"GHS\",
                    ref: '{{ 'now'|date('U') }}_{{order.number}}_{{order.id}}', // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                    metadata: {
                        custom_fields: [
                            {
                                display_name: \"Tracking Number\",
                                variable_name: \"number\",
                                value: \"{{order.number}}\"
                            }
                        ]
                    },
                    callback: function(response){

                         \$.request('onCheckpaystack', {
                              data: {reference: response.reference},
                              success: function(response, status, xhr, \$form) {

                                  swal.fire({
                                      title: '{{\"Thank you!\"|__}}',
                                      text: '{{\"Your payment is successfully paid! You will be redirect rightnow, please wait\"|__}}',
                                      type: 'success',
                                      buttonsStyling: false,
                                      confirmButtonText: '{{\"OK\"|__}}',
                                      confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                                  });
                                  //window.location.reload();
                              }
                         });
                    },
                    onClose: function(){
                        swal.fire({
                            title: '{{\"Closed!\"|__}}',
                            text: '{{\"You did not pay the requested payment! :(\"|__}}',
                            type: 'warning',
                            buttonsStyling: false,
                            confirmButtonText: '{{\"OK\"|__}}',
                            confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                        });
                    }
                });
                handler.openIframe();
            {% endif %}
        }
    {% endif %}
{% endif %}
var KTDashboard = function () {

    // Order Statistics.
    // Based on Chartjs plugin - http://www.chartjs.org/
    var orderStatistics = function() {
        var container = KTUtil.getByID('kt_chart_order_statistics');

        if (!container) {
            return;
        }

        var MONTHS = ['1 Jan', '2 Jan', '3 Jan', '4 Jan', '5 Jan', '6 Jan', '7 Jan'];

        var color = Chart.helpers.color;
        var barChartData = {
            labels: ['1 Jan', '2 Jan', '3 Jan', '4 Jan', '5 Jan', '6 Jan', '7 Jan'],
            datasets : [
\t\t\t\t{
                    fill: true,
                    //borderWidth: 0,
                    backgroundColor: color(KTApp.getStateColor('brand')).alpha(0.6).rgbString(),
                    borderColor : color(KTApp.getStateColor('brand')).alpha(0).rgbString(),

                    pointHoverRadius: 4,
                    pointHoverBorderWidth: 12,
                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: KTApp.getStateColor('brand'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),

\t\t\t\t\tdata: [20, 30, 20, 40, 30, 60, 30]
\t\t\t\t},
\t\t\t\t{
                    fill: true,
                    //borderWidth: 0,
\t\t\t\t\tbackgroundColor : color(KTApp.getStateColor('brand')).alpha(0.2).rgbString(),
                    borderColor : color(KTApp.getStateColor('brand')).alpha(0).rgbString(),

                    pointHoverRadius: 4,
                    pointHoverBorderWidth: 12,
                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: KTApp.getStateColor('brand'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),

\t\t\t\t\tdata: [15, 40, 15, 30, 40, 30, 50]
\t\t\t\t}
            ]
        };

        var ctx = container.getContext('2d');
        var chart = new Chart(ctx, {
            type: 'line',
            data: barChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: false,
                scales: {
                    xAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Month'
                        },
                        gridLines: false,
                        ticks: {
                            display: true,
                            beginAtZero: true,
                            fontColor: KTApp.getBaseColor('shape', 3),
                            fontSize: 13,
                            padding: 10
                        }
                    }],
                    yAxes: [{
                        categoryPercentage: 0.35,
                        barPercentage: 0.70,
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Value'
                        },
                        gridLines: {
                            color: KTApp.getBaseColor('shape', 2),
                            drawBorder: false,
                            offsetGridLines: false,
                            drawTicks: false,
                            borderDash: [3, 4],
                            zeroLineWidth: 1,
                            zeroLineColor: KTApp.getBaseColor('shape', 2),
                            zeroLineBorderDash: [3, 4]
                        },
                        ticks: {
                            max: 70,
                            stepSize: 10,
                            display: true,
                            beginAtZero: true,
                            fontColor: KTApp.getBaseColor('shape', 3),
                            fontSize: 13,
                            padding: 10
                        }
                    }]
                },
                title: {
                    display: false
                },
                hover: {
                    mode: 'index'
                },
                tooltips: {
                    enabled: true,
                    intersect: false,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 10,
                    xPadding: 10,
                    caretPadding: 0,
                    displayColors: false,
                    backgroundColor: KTApp.getStateColor('brand'),
                    titleFontColor: '#ffffff',
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 5
                    }
                }
            }
        });
    };
    var latestTrendsMap = function() {
        if (\$('#kt_map_sender').length == 0) {
            return;
        }
        {% if order.sender_address.lat %}
        try {
            var map = new GMaps({
                div: '#kt_map_sender',
                lat: {{order.sender_address.lat}},
                lng: {{order.sender_address.lng}}
            });
            map.addMarker({
                lat: {{order.sender_address.lat}},
                lng: {{order.sender_address.lng}},
                title: '{{order.sender_address.name}}',
                infoWindow: {
                    content: '<p>{{\"Go to\"|__}} <a href=\"{{order.sender_address.url}}\" target=\"_blank\">{{order.sender_address.name}}</a></p>'
                }
            });
        } catch (e) {
            console.log(e);
        }

        {% endif %}
        if (\$('#kt_map_receiver').length == 0) {
            return;
        }
        {% if order.receiver_address.lat %}
            try {
                var map = new GMaps({
                    div: '#kt_map_receiver',
                    lat: {{order.receiver_address.lat}},
                    lng: {{order.receiver_address.lng}}
                });
                map.addMarker({
                    lat: {{order.receiver_address.lat}},
                    lng: {{order.receiver_address.lng}},
                    title: '{{order.receiver_address.name}}',
                    infoWindow: {
                        content: '<p>{{\"Go to\"|__}} <a href=\"{{order.receiver_address.url}}\" target=\"_blank\">{{order.receiver_address.name}}</a></p>'
                    }
                });
            } catch (e) {
                console.log(e);
            }
        {% endif %}
    }
    return {
        // public functions
        init: function () {
            orderStatistics();
            latestTrendsMap();
        },
    };
}();

KTUtil.ready(function () {
    KTDashboard.init();

    \$('.notes_scroll').on('click', function(e){
        \$([document.documentElement, document.body]).animate({
            scrollTop: \$(\"#notes_continer\").offset().top-60
        }, 2000);
    });
    \$('#assign_employee').on('click', '.btn-primary', function(e){
        var parent = \$('#assign_employee');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#assign_employee').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '{{\"Please wait\"|__}}...'
            });
        }
    });
    \$('#assign_manifest').on('click', '.btn-primary', function(e){
        var parent = \$('#assign_manifest');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#assign_manifest').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '{{\"Please wait\"|__}}...'
            });
        }
    });
    \$('#transfer_office').on('click', '.btn-primary', function(e){
        var parent = \$('#assign_employee');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#transfer_office').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '{{\"Please wait\"|__}}...'
            });
        }
    });
    \$('#refuse').on('click', '.btn-primary', function(e){
        var parent = \$('#refuse');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#refuse').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '{{\"Please wait\"|__}}...'
            });
        }
    });
    \$('#discards').on('click', '.btn-primary', function(e){
        var parent = \$('#discards');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#discards').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '{{\"Please wait\"|__}}...'
            });
        }
    });
    \$('#return_discards').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '{{\"Please wait\"|__}}...'
        });
        \$.request('onDiscards', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"{{url('dashboard/shipments')}}/delivered\";
            }
        });
    });
    \$('#delivered_driver').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '{{\"Please wait\"|__}}...'
        });
        \$.request('onDeliver', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"{{url('dashboard/shipments')}}/{{this.param.id}}/view\";
            }
        });
    });
    \$('#deliveried').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '{{\"Please wait\"|__}}...'
        });
        \$.request('onDelivery', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"{{url('dashboard/shipments')}}/{{this.param.id}}/view\";
            }
        });
    });
    \$('#stock').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '{{\"Please wait\"|__}}...'
        });
        \$.request('onStock', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"{{url('dashboard/shipments')}}/stock\";
            }
        });
    });
    \$('#received').on('click', '.btn-primary', function(e){
        var parent = \$('#received');
        var validation = 1;
        \$.validator.setDefaults({
            ignore: \":hidden\",
       });
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){
            \$('#received').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '{{\"Please wait\"|__}}...'
            });
        }
    });


    \$('#received').find('input,select').each(function(){
        \$(this).on('change', function(){
            \$('#received .btn-info').removeClass('kt-hidden');
            \$('#received .btn-primary').addClass('kt-hidden');
        });
    });

    \$('#received .btn-info').removeClass('kt-hidden');
    \$('#received .btn-primary').addClass('kt-hidden');


    \$('#received').on('click', '.save', function(e){
        var parent = \$('#received');
        var validation = 1;
        \$.validator.setDefaults({
            ignore: \":hidden\",
       });
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });
        if(validation){

            var receiver_address_id = \$('#received #receiver_address_id').val();
            var package_fee = \$('#received #package_fee').val();
            var return_courier_fee = \$('#received #return_courier_fee').val();
            var return_defray = \$('#received .return_defray:checked').val();
            var return_package_fee = \$('#received .return_package_fee:checked').val();

             \$.request('onConfirmfees', {
                  data: {return_courier_fee: return_courier_fee, package_fee: package_fee, receiver_address_id: receiver_address_id, return_defray: return_defray, return_package_fee: return_package_fee},
                  success: function(response, status, xhr, \$form) {

                      swal.fire({
                          buttonsStyling: false,

                          html: \"<strong>{{'The total cost of courier fees is'|__}}:</strong> \"+response.delivery_cost+\"<br /><strong>{{'The total requested from sender is'|__}}:</strong> \"+response.sender_fees+\"<br /><strong>{{'The total requested from receiver is'|__}}:</strong> \"+response.receiver_fees,
                          type: \"warning\",

                          confirmButtonText: \"{{'Yes, confirm!'|__}}\",
                          confirmButtonClass: \"btn btn-sm btn-bold btn-success\",

                          showCancelButton: true,
                          cancelButtonText: '{{\"No, change something\"|__}}',
                          cancelButtonClass: \"btn btn-sm btn-bold btn-danger\"
                      }).then(function (result) {
                          if (result.value) {
                              \$('#received .btn-info').addClass('kt-hidden');
                              \$('#received .btn-primary').removeClass('kt-hidden');
                          }
                      });
                  }
             });
        }
    });
    \$('#postpone').on('click', '.btn-primary', function(e){
        var parent = \$('#postpone');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            var css_class = \$(this).attr('class');
            css_class = css_class.replace('form-control ','');
            if(css_class != 'date'){
                if(\$(this).valid() == false){
                    validation = 0;
                }
            }
        });
        if(validation){
            \$('#postpone').modal('toggle');
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'success',
                message: '{{\"Please wait\"|__}}...'
            });
        }
    });
    \$('#accept').on('click', function(e){
        e.preventDefault();
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'success',
            message: '{{\"Please wait\"|__}}...'
        });

        \$.request('onAccept', {
            success: function(data) {
                KTApp.unblockPage();
                window.location.href = \"{{url('dashboard/shipments')}}/{{this.param.id}}/view\";
            }
        });
    });

    function formatRepo(repo) {
        if (repo.loading) return repo.text;
        var markup = \"<div class='select2-result-repository clearfix'>\" +
                        \"<div class='select2-result-repository__meta'>\" +
                        \"<div class='select2-result-repository__title'>\" + repo.text + \"</div>\";
                        if (repo.mobile) {
                            markup += \"<div class='select2-result-repository__description'>\" + repo.mobile + \"</div>\";
                        }
                    markup += \"</div></div>\";
        return markup;
    }

    function formatRepoSelection(repo) {
        return repo.text || repo.id;
    }


    \$('body').on('change', '#discard_reason', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        \$('.discard_reason_receiver').addClass('kt-hidden');
        if(selected != '' && selected != 2){
            \$('.discard_reason_receiver').removeClass('kt-hidden');
        }
    });
    \$('body').on('change', '#receiver_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        if(selected == 'new'){
            \$('select.receiver_address_id').html('').val('').selectpicker('refresh');
            \$('#addnewreceiver').removeClass('kt-hidden');
        }else if(selected != ''){
            \$('#addnewreceiver').addClass('kt-hidden');
            \$.request('onClientaddresses', {
                 data: {id: selected},
                 success: function(response, status, xhr, \$form) {
                     \$('select.receiver_address_id').html(response.html).selectpicker('refresh');
                     \$('select.receiver_address_id').val(response.default).selectpicker('refresh');
                     if(response.default == 'new'){
                         \$('#addnewreceiveraddress').removeClass('kt-hidden');
                     }else if(selected != ''){
                         \$('#addnewreceiveraddress').addClass('kt-hidden');
                     }



                     var selected = response.default;
                     var type = \$('.type:checked').val();
                     var sender_address_id = \$('#sender_address_id').val();
                     var packaging_id = \$('#packaging_id').val();
                     var return_defray = \$('.return_defray:checked').val();
                     var return_package_fee = \$('.return_package_fee:checked').val();
                     if(selected != '' && selected != 'new'){
                         \$.request('onChangefees', {
                              data: {sender_address_id: sender_address_id, packaging_id: packaging_id, type: type, receiver_address_id: selected, return_defray: return_defray, return_package_fee: return_package_fee},
                              success: function(response, status, xhr, \$form) {
                                   \$('#delivery_cost').val(response.delivery_cost);
                                   \$('#return_courier_fee').val(response.return_courier_fee);
                              }
                         });
                    }
                 }
            });
        }
    });
    \$('body').on('click', '#addnewreceiver .save', function(e){
        e.preventDefault();
        var parent = \$('#addnewreceiver');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });


        if(validation){
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: '{{\"Processing, please wait\"|__}}...'
            });
            \$.request('onNewclient', {
                 data: {name: parent.find('.name').val(), mobile: parent.find('.mobile').val(), email: parent.find('.email').val(), gender: parent.find('.gender:checked').val(), street_addr: parent.find('.street_addr').val(), lat: parent.find('.lat').val(), lng: parent.find('.lng').val(), url: parent.find('.url').val(), area_id: parent.find('.area_id').find(\"option:selected\").val(), city_id: parent.find('.city_id').find(\"option:selected\").val(), postal_code: parent.find('.postal_code').val(), state_id: parent.find('.state_id').find(\"option:selected\").val(), country_id: parent.find('.country_id').find(\"option:selected\").val(), connect: parent.find('.connect:checked').val(), username: parent.find('.username').val(), password: parent.find('.password').val()},
                 error: function(response, status, xhr, \$form) {
                     swal.fire({
                         title: '{{\"Error!\"|__}}',
                         text: response.responseText,
                         type: 'error',
                         buttonsStyling: false,
                         confirmButtonText: '{{\"OK\"|__}}',
                         confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                     });
                     KTApp.unblockPage();
                 },
                 success: function(response, status, xhr, \$form) {
                     var newOption = new Option(response.name, response.id, false, true);
                     \$('#receiver_id').append(newOption).trigger('change');
                     \$('select.receiver_address_id').html('<option value=\"'+response.address_id+'\">'+response.address_name+'</option>').selectpicker('refresh');
                     \$('select.receiver_address_id').val(response.address_id).selectpicker('refresh');


                      var selected = response.address_id;
                      var type = \$('.type:checked').val();
                      var sender_address_id = \$('#sender_address_id').val();
                      var packaging_id = \$('#packaging_id').val();
                      var return_defray = \$('.return_defray:checked').val();
                      var return_package_fee = \$('.return_package_fee:checked').val();
                      if(selected != '' && selected != 'new'){
                          \$.request('onChangefees', {
                               data: {sender_address_id: sender_address_id, packaging_id: packaging_id, type: type, receiver_address_id: selected, return_defray: return_defray, return_package_fee: return_package_fee},
                               success: function(response, status, xhr, \$form) {
                                    \$('#delivery_cost').val(response.delivery_cost);
                                    \$('#return_courier_fee').val(response.return_courier_fee);
                               }
                          });
                     }

                     KTApp.unblockPage();
                     parent.find('input,select').each(function(){
                         \$(this).val('');
                         \$(this).selectpicker('refresh');
                     });
                     \$('#addnewreceiver').addClass('kt-hidden');
                 }
            });
        }
    });
    \$('body').on('click', '#addnewreceiver .cancel', function(e){
        e.preventDefault();
        var newOption = new Option('', '', false, true);
        \$('#receiver_id').append(newOption).trigger('change');
        \$('#addnewreceiver').addClass('kt-hidden');
    });

    \$('body').on('changed.bs.select', '.receiver_address_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        if(selected == 'new'){
            \$('#addnewreceiveraddress').removeClass('kt-hidden');
        }else if(selected != ''){
            \$('#addnewreceiveraddress').addClass('kt-hidden');
        }
    });
    \$('body').on('click', '#addnewreceiveraddress .save', function(e){
        e.preventDefault();
        var parent = \$('#addnewreceiveraddress');
        var validation = 1;
        parent.find('input,select').each(function(){
            if(\$(this).is(':hidden')){
                return;
            }
            if(\$(this).valid() == false){
                validation = 0;
            }
        });

        if(validation){
            KTApp.blockPage({
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: '{{\"Processing, please wait\"|__}}...'
            });
            \$.request('onNewclientaddress', {
                 data: {client_id: \$('#receiver_id').val(), street_addr: parent.find('.street_addr').val(), lat: parent.find('.lat').val(), lng: parent.find('.lng').val(), url: parent.find('.url').val(), area_id: parent.find('.area_id').find(\"option:selected\").val(), city_id: parent.find('.city_id').find(\"option:selected\").val(), postal_code: parent.find('.postal_code').val(), state_id: parent.find('.state_id').find(\"option:selected\").val(), country_id: parent.find('.country_id').find(\"option:selected\").val()},
                 error: function(response, status, xhr, \$form) {
                     swal.fire({
                         title: '{{\"Error!\"|__}}',
                         text: response.responseText,
                         type: 'error',
                         buttonsStyling: false,
                         confirmButtonText: '{{\"OK\"|__}}',
                         confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                     });
                     KTApp.unblockPage();
                 },
                 success: function(response, status, xhr, \$form) {
                     \$('select.receiver_address_id').html(response.html).selectpicker('refresh');
                     \$('select.receiver_address_id').val(response.default).selectpicker('refresh');




                       var selected = response.default;
                       var type = \$('.type:checked').val();
                       var sender_address_id = \$('#sender_address_id').val();
                       var packaging_id = \$('#packaging_id').val();
                       var return_defray = \$('.return_defray:checked').val();
                       var return_package_fee = \$('.return_package_fee:checked').val();
                       if(selected != '' && selected != 'new'){
                           \$.request('onChangefees', {
                                data: {sender_address_id: sender_address_id, packaging_id: packaging_id, type: type, receiver_address_id: selected, return_defray: return_defray, return_package_fee: return_package_fee},
                                success: function(response, status, xhr, \$form) {
                                     \$('#delivery_cost').val(response.delivery_cost);
                                     \$('#return_courier_fee').val(response.return_courier_fee);
                                }
                           });
                      }



                     KTApp.unblockPage();
                     parent.find('input,select').each(function(){
                         \$(this).val('');
                         \$(this).selectpicker('refresh');
                     });
                     \$('#addnewreceiveraddress').addClass('kt-hidden');
                 }
            });
        }
    });
    \$('body').on('click', '#addnewreceiveraddress .cancel', function(e){
        e.preventDefault();
        \$('select.receiver_address_id').val('').selectpicker('refresh');
        \$('#addnewreceiveraddress').addClass('kt-hidden');
    });


    \$('body').on('changed.bs.select', '.country_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        var parent = \$(e.currentTarget).parent().parent().parent().parent();
        if(selected != ''){
            \$.request('onListstates', {
                 data: {id: selected},
                 success: function(response, status, xhr, \$form) {
                      parent.find('select.state_id').selectpicker({title: '{{'Select'|__}}'}).selectpicker('refresh');
                      parent.find('select.state_id').html(response.html).selectpicker('refresh');
                 }
            });
       }
    });
    \$('body').on('changed.bs.select', '.state_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        var parent = \$(e.currentTarget).parent().parent().parent().parent();
        if(selected != ''){
            \$.request('onListcities', {
                 data: {id: selected},
                 success: function(response, status, xhr, \$form) {
                      parent.find('select.city_id').selectpicker({title: '{{'Select'|__}}'}).selectpicker('refresh');
                      parent.find('select.city_id').html(response.html).selectpicker('refresh');
                 }
            });
       }
    });
    \$('body').on('changed.bs.select', '.city_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
        var parent = \$(e.currentTarget).parent().parent().parent().parent();
        if(selected != ''){
            \$.request('onListareas', {
                 data: {id: selected},
                 success: function(response, status, xhr, \$form) {
                      parent.find('select.area_id').selectpicker({title: '{{'Select'|__}}'}).selectpicker('refresh');
                      parent.find('select.area_id').html(response.html).selectpicker('refresh');
                 }
            });
       }
    });

    \$('body').on('change', '#receiver_address_id', function(e, clickedIndex, newValue, oldValue){
        var selected = \$(e.currentTarget).val();
         var type = \$('.type:checked').val();
         var sender_address_id = \$('#sender_address_id').val();
         var packaging_id = \$('#packaging_id').val();
         var return_defray = \$('.return_defray:checked').val();
         var return_package_fee = \$('.return_package_fee:checked').val();
         if(selected != '' && selected != 'new'){
             \$.request('onChangefees', {
                  data: {sender_address_id: sender_address_id, type: type, packaging_id:packaging_id, receiver_address_id: selected, return_defray: return_defray, return_package_fee: return_package_fee},
                  success: function(response, status, xhr, \$form) {
                       \$('#delivery_cost').val(response.delivery_cost);
                       \$('#return_courier_fee').val(response.return_courier_fee);
                  }
             });
        }
    });


    \$(\".clients\").select2({
        {% if currentLang == 'ar'%}
            dir: \"rtl\",
        {% endif %}
        language: {
            errorLoading: function () {
                return \"{{'There is an error while searching'|__}}...\";
            },
            inputTooLong: function (args) {
                return \"{{'You must enter less characters'|__}}...\";
            },
            inputTooShort: function (args) {
                return \"{{'You must enter more characters'|__}}...\";
            },
            loadingMore: function () {
                return \"{{'Loading More'|__}}...\";
            },
            maximumSelected: function (args) {
                return \"{{'Maximum element has been selected'|__}}...\";
            },
            noResults: function () {
                return \"{{'No result found'|__}}...\";
            },
            searching: function () {
                return \"{{'Searching'|__}}...\";
            }
        },
        placeholder: \"{{'Search for client'|__}}\",
        allowClear: true,
        ajax: {
            transport: function(params, success, failure) {
                var \$request = \$.request('onGetclients', {
                    data: params.data,
                })
                \$request.done(success)
                \$request.fail(failure)

                return \$request
            },
            dataType: 'json',
            processResults: function (response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        },
        minimumInputLength: 0,
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    \$('.bootstrap-touchspin-vertical-btn').TouchSpin({
        buttondown_class: 'btn btn-secondary',
        buttonup_class: 'btn btn-secondary',
        verticalbuttons: true,
        verticalup: '<i class=\"la la-plus\"></i>',
        verticaldown: '<i class=\"la la-minus\"></i>'
    });
    \$('.address').each(function(){
        var address = \$(this);
        address.geocomplete({
            map: \".map_canvas.map_\"+address.attr('rel'),
            mapOptions: {
                zoom: 18
            },
            markerOptions: {
                draggable: true
            },
            details: \".location-\"+\$(this).attr('rel'),
            detailsAttribute: 'data-'+\$(this).attr('rel'),
            autoselect: true,
        });
        address.bind(\"geocode:dragged\", function(event, latLng){
            \$(\"input[data-\"+address.attr('rel')+\"=lat]\").val(latLng.lat());
            \$(\"input[data-\"+address.attr('rel')+\"=lng]\").val(latLng.lng());
        });

    });
    \$('body').on('click', '.connect', function(){
        if(\$(this).is(\":checked\")){
            \$('#connect_'+\$(this).val()).removeClass('kt-hidden');
        }else{
            \$('#connect_'+\$(this).val()).addClass('kt-hidden');
        }
    });
    \$('body').on('click', '.return_defray', function(){
        if(\$(this).val() == 1){
            \$('.package_fee').removeClass('kt-hidden');


             var sender_address_id = \$('#sender_address_id').val();
             var receiver_address_id = \$('#receiver_address_id').val();
             var packaging_id = \$('#packaging_id').val();
             var type = \$('.type:checked').val();
             var return_defray = \$('.return_defray:checked').val();
             var return_package_fee = \$('.return_package_fee:checked').val();
             \$.request('onChangefees', {
                  data: {sender_address_id: sender_address_id, type: type, packaging_id:packaging_id, receiver_address_id: receiver_address_id, return_defray: return_defray, return_package_fee: return_package_fee},
                  success: function(response, status, xhr, \$form) {
                       \$('#delivery_cost').val(response.delivery_cost);
                       \$('#return_courier_fee').val(response.return_courier_fee);
                  }
             });
        }else{
            \$('.package_fee').addClass('kt-hidden');
        }
    });
    \$(\".fees\").inputmask('999{{primary_currency.thousand_separator}}999{{primary_currency.thousand_separator}}999{{primary_currency.decimal_point}}{% for i in '1'..primary_currency.initbiz_money_fraction_digits %}9{% endfor %}', {
        numericInput: true
    });

    \$('#delete').on('click', function(e){
        e.preventDefault();

        swal.fire({
            buttonsStyling: false,

            text: \"{{'Are you sure to delete'|__}}\",
            type: \"error\",

            confirmButtonText: \"{{'Yes, delete!'|__}}\",
            confirmButtonClass: \"btn btn-sm btn-bold btn-danger\",

            showCancelButton: true,
            cancelButtonText: '{{\"No, cancel\"|__}}',
            cancelButtonClass: \"btn btn-sm btn-bold btn-brand\"
        }).then(function (result) {
            if (result.value) {
                KTApp.blockPage({
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'success',
                    message: '{{\"Please wait\"|__}}...'
                });
                \$.request('onDelete', {
                    success: function(data) {
                        KTApp.unblockPage();
                        swal.fire({
                            title: '{{\"Deleted!\"|__}}',
                            text: '{{\"Your selected records have been deleted! :(\"|__}}',
                            type: 'success',
                            buttonsStyling: false,
                            confirmButtonText: '{{\"OK\"|__}}',
                            confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                        });
                        window.location.href = \"{{url('dashboard/shipments')}}\";
                    }
                });
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
            } else if (result.dismiss === 'cancel') {
                swal.fire({
                    title: '{{\"Cancelled\"|__}}',
                    text: '{{\"You selected records have not been deleted! :)\"|__}}',
                    type: 'error',
                    buttonsStyling: false,
                    confirmButtonText: '{{\"OK\"|__}}',
                    confirmButtonClass: \"btn btn-sm btn-bold btn-brand\",
                });
            }
        });
    });
    if(\$('.kt-widget__action ul.kt-nav li').length == 0){
        \$('.kt-widget__action').hide();
    }
});
</script>
{% endput %}", "C:\\laragon\\www\\cargo/themes/spotlayer/pages/dashboard/shipment.htm", "");
    }
}
